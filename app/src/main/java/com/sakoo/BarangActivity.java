package com.sakoo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class BarangActivity extends AppCompatActivity {

    private Button simpanBarang;

    private Bitmap bitmap, thumbnail;
    BarangService barangService;

    private ImageView imageView1, imageView2, imageView3, imageView4, imageView5;
    private TextView hapus1, hapus2, hapus3, hapus4, hapus5;

    private ImageView pilihKategori, pilihDetail, pilihKondisiBarang;

    private TextView textKategori, detailBarang, contohGambar, textKondisiBarang;
    private EditText inputNamaBarang, inputDeskripsiBarang, inputPanjang, inputLebar, inputTinggi, inputBahan, inputMerk, inputBerat, inputStok, inputHarga;

    private String foto1 = "", foto2 = "", foto3 = "", foto4 = "", foto5 = "", urlfoto1 = "", urlfoto2 = "", urlfoto3 = "", urlfoto4 = "", urlfoto5 = "", namaBarang = "", deskripsiBarang = "", panjangBarang = "", lebarBarang = "", tinggiBarang = "", bahanBarang = "", merk = "", berat = "", stok = "", harga = "", volumeBarang = "", deskripsiEdit = "";

    private String idBarang = "";
    //private int targetW, targetH;

    private BaseUtility baseUtility;
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private PrefManager prefManager;

    private RelativeLayout cardPilihKategori;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
        prefManager = new PrefManager(this);

        contohGambar = (TextView)findViewById(R.id.contohGambar);

        textKondisiBarang = (TextView)findViewById(R.id.kondisiBarang);
        pilihKondisiBarang = (TextView)findViewById(R.id.kondisiBarangPilih);
        textKategori = (TextView)findViewById(R.id.kategori_barang);
        pilihKategori = (ImageView) findViewById(R.id.pilihKategori);
        detailBarang = (TextView)findViewById(R.id.detail_barang);
        pilihDetail = (ImageView) findViewById(R.id.pilihDetail);

        inputNamaBarang = (EditText) findViewById(R.id.nama_barang);
        inputDeskripsiBarang = (EditText) findViewById(R.id.deskripsi_barang);

        //TAMBAHAN TAMPILAN BARU
        inputPanjang = (EditText)findViewById(R.id.panjangBarang);
        inputLebar = (EditText)findViewById(R.id.lebarBarang);
        inputTinggi = (EditText)findViewById(R.id.tinggiBarang);
        inputBahan = (EditText)findViewById(R.id.bahan_barang);
        inputMerk = (EditText)findViewById(R.id.merk_barang);
        inputBerat = (EditText)findViewById(R.id.berat_barang);
        inputHarga = (EditText)findViewById(R.id.harga_barang);
        inputHarga.addTextChangedListener(new NumberTextWatcher(inputHarga, "#,###"));
        inputStok = (EditText)findViewById(R.id.stok_barang);


        cardPilihKategori = (RelativeLayout) findViewById(R.id.cardPilihKategori);
        //cardPilihDetail = (RelativeLayout) findViewById(R.id.cardPilihDetail);

        imageView1 = (ImageView)findViewById(R.id.gambar1);
        imageView2 = (ImageView)findViewById(R.id.gambar2);
        imageView3 = (ImageView)findViewById(R.id.gambar3);
        imageView4 = (ImageView)findViewById(R.id.gambar4);
        imageView5 = (ImageView)findViewById(R.id.gambar5);

        imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        imageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        imageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        imageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
            }
        });

        contohGambar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lihatGambar();
            }
        });

        hapus1 = (TextView) findViewById(R.id.hapus1);
        hapus2 = (TextView) findViewById(R.id.hapus2);
        hapus3 = (TextView) findViewById(R.id.hapus3);
        hapus4 = (TextView) findViewById(R.id.hapus4);
        hapus5 = (TextView) findViewById(R.id.hapus5);

        hapusFoto();
        checkBarang();

        if(getIntent().hasExtra("idBarang")){
            idBarang = getIntent().getExtras().getString("idBarang");
            sessionManager.idBarang(idBarang);
            String namaBarangNya = getIntent().getExtras().getString("namaBarang");
            String namaBarangSplit[] = namaBarangNya.split("sk-");
            inputNamaBarang.setText(namaBarangSplit[0].trim());
            inputDeskripsiBarang.setText(getIntent().getExtras().getString("deskripsi").trim());


            inputBahan.setText(getIntent().getExtras().getString("bahanBarang"));
            inputMerk.setText(getIntent().getExtras().getString("merkBarang"));
            inputBerat.setText(String.valueOf(getIntent().getExtras().getInt("beratBarang")));
            inputHarga.setText(String.valueOf(getIntent().getExtras().getInt("hargaBarang")));
            inputHarga.addTextChangedListener(new NumberTextWatcher(inputHarga, "#,###"));
            inputStok.setText(String.valueOf(getIntent().getExtras().getInt("stokBarang")));

            textKategori.setText(getIntent().getExtras().getString("subkategori"));
            textKategori.setTextColor(Color.DKGRAY);
            sessionManager.subKategori(String.valueOf(getIntent().getExtras().getInt("idSubkategori")));
            sessionManager.textSubKategori(getIntent().getExtras().getString("subkategori"));

            foto1 = getIntent().getExtras().getString("fotoBarang");
            urlfoto1 = getIntent().getExtras().getString("fotoBarang");
            sessionManager.foto1(foto1);
            sessionManager.urlfoto1(urlfoto1);
            hapus1.setVisibility(View.VISIBLE);
            String temp1 = foto1.replaceAll(" ", "%20");
            Picasso.with(this).load(temp1+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView1);

            String volumeBarangNya = getIntent().getExtras().getString("volumeBarang");
            String[] plt = volumeBarangNya.split("x");
            inputPanjang.setText(plt[0]);
            inputLebar.setText(plt[1]);
            inputTinggi.setText(plt[2]);

            String fotoTambahanBarang = getIntent().getExtras().getString("fotoTambahan");
            try {
                JSONArray jsonArray = new JSONArray(fotoTambahanBarang);
                String temp2 = "" , temp3 = "", temp4 = "", temp5 = "", temp6 = "";
                for(int i = 0; i<jsonArray.length(); i++){
                    if(i == 0){
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        foto2 = jsonObject1.getString("foto");
                        urlfoto2 = jsonObject1.getString("foto");
                        sessionManager.foto2(foto2);
                        sessionManager.urlfoto2(urlfoto2);
                        hapus2.setVisibility(View.VISIBLE);
                        temp2 = foto2.replaceAll(" ", "%20");
                    }else if(i ==  1){
                        JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                        foto3 = jsonObject2.getString("foto");
                        urlfoto3 = jsonObject2.getString("foto");
                        sessionManager.foto3(foto3);
                        sessionManager.urlfoto3(urlfoto3);
                        hapus3.setVisibility(View.VISIBLE);
                        temp3 = foto3.replaceAll(" ", "%20");
                    }else if(i == 2){
                        JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                        foto4 = jsonObject3.getString("foto");
                        urlfoto4 = jsonObject3.getString("foto");
                        sessionManager.foto4(foto4);
                        sessionManager.urlfoto4(urlfoto4);
                        hapus4.setVisibility(View.VISIBLE);
                        temp4 = foto4.replaceAll(" ", "%20");
                    }else if(i == 3){
                        JSONObject jsonObject4 = jsonArray.getJSONObject(i);
                        foto5 = jsonObject4.getString("foto");
                        urlfoto5 = jsonObject4.getString("foto");
                        sessionManager.foto5(foto5);
                        sessionManager.urlfoto5(urlfoto5);
                        hapus5.setVisibility(View.VISIBLE);
                        temp5 = foto5.replaceAll(" ", "%20");
                    }
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(!Objects.equals(temp5, "")){
                        Picasso.with(this).load(temp5+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView5);
                    }
                    if(!Objects.equals(temp4, "")){
                        Picasso.with(this).load(temp4+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView4);
                    }
                    if(!Objects.equals(temp3, "")){
                        Picasso.with(this).load(temp3+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView3);
                    }
                    if(!Objects.equals(temp2, "")){
                        Picasso.with(this).load(temp2+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView2);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else{
            if(getIntent().hasExtra("namaBarang")){
                String namaBarangNya = getIntent().getExtras().getString("namaBarang");
                String namaBarangSplit[] = namaBarangNya.split("sk-");
                inputNamaBarang.setText(namaBarangSplit[0].trim());
                inputDeskripsiBarang.setText(getIntent().getExtras().getString("deskripsi").trim());


                inputBahan.setText(getIntent().getExtras().getString("bahanBarang"));
                inputMerk.setText(getIntent().getExtras().getString("merkBarang"));
                inputBerat.setText(String.valueOf(getIntent().getExtras().getInt("beratBarang")));
                inputHarga.setText(String.valueOf(getIntent().getExtras().getInt("hargaBarang")));
                inputHarga.addTextChangedListener(new NumberTextWatcher(inputHarga, "#,###"));
                inputStok.setText(String.valueOf(getIntent().getExtras().getInt("stokBarang")));

                textKategori.setText(getIntent().getExtras().getString("subkategori"));
                textKategori.setTextColor(Color.DKGRAY);
                sessionManager.subKategori(String.valueOf(getIntent().getExtras().getInt("idSubkategori")));
                sessionManager.textSubKategori(getIntent().getExtras().getString("subkategori"));
                Log.d("TAG", sessionManager.getSubKategori());
                Log.d("TAG", String.valueOf(getIntent().getExtras().getInt("idSubkategori")));

                foto1 = getIntent().getExtras().getString("fotoBarang");
                urlfoto1 = getIntent().getExtras().getString("fotoBarang");
                sessionManager.foto1(foto1);
                sessionManager.urlfoto1(urlfoto1);
                hapus1.setVisibility(View.VISIBLE);
                String temp1 = foto1.replaceAll(" ", "%20");
                Picasso.with(this).load(temp1+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView1);

                String volumeBarangNya = getIntent().getExtras().getString("volumeBarang");
                String[] plt = volumeBarangNya.split("x");
                inputPanjang.setText(plt[0]);
                inputLebar.setText(plt[1]);
                inputTinggi.setText(plt[2]);

                String fotoTambahanBarang = getIntent().getExtras().getString("fotoTambahan");
                try {
                    JSONArray jsonArray = new JSONArray(fotoTambahanBarang);
                    String temp2 = "" , temp3 = "", temp4 = "", temp5 = "", temp6 = "";
                    for(int i = 0; i<jsonArray.length(); i++){
                        if(i == 0){
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            foto2 = jsonObject1.getString("foto");
                            urlfoto2 = jsonObject1.getString("foto");
                            sessionManager.foto2(foto2);
                            sessionManager.urlfoto2(urlfoto2);
                            hapus2.setVisibility(View.VISIBLE);
                            temp2 = foto2.replaceAll(" ", "%20");
                        }else if(i ==  1){
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                            foto3 = jsonObject2.getString("foto");
                            urlfoto3 = jsonObject2.getString("foto");
                            sessionManager.foto3(foto3);
                            sessionManager.urlfoto3(urlfoto3);
                            hapus3.setVisibility(View.VISIBLE);
                            temp3 = foto3.replaceAll(" ", "%20");
                        }else if(i == 2){
                            JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                            foto4 = jsonObject3.getString("foto");
                            urlfoto4 = jsonObject3.getString("foto");
                            sessionManager.foto4(foto4);
                            sessionManager.urlfoto4(urlfoto4);
                            hapus4.setVisibility(View.VISIBLE);
                            temp4 = foto4.replaceAll(" ", "%20");
                        }else if(i == 3){
                            JSONObject jsonObject4 = jsonArray.getJSONObject(i);
                            foto5 = jsonObject4.getString("foto");
                            urlfoto5 = jsonObject4.getString("foto");
                            sessionManager.foto5(foto5);
                            sessionManager.urlfoto5(urlfoto5);
                            hapus5.setVisibility(View.VISIBLE);
                            temp5 = foto5.replaceAll(" ", "%20");
                        }
                    }

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(temp5, "")){
                            Picasso.with(this).load(temp5+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView5);
                        }
                        if(!Objects.equals(temp4, "")){
                            Picasso.with(this).load(temp4+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView4);
                        }
                        if(!Objects.equals(temp3, "")){
                            Picasso.with(this).load(temp3+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView3);
                        }
                        if(!Objects.equals(temp2, "")){
                            Picasso.with(this).load(temp2+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView2);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

        simpanBarang = (Button) findViewById(R.id.simpan_barang);
        simpanBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                volumeBarang = "";
                //tambahBarangFunction();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(inputPanjang.getText().length() == 0){
                        volumeBarang = volumeBarang + "0";
                    }else{
                        volumeBarang = volumeBarang + inputPanjang.getText().toString();
                    }

                    if(inputLebar.getText().length() == 0){
                        volumeBarang = volumeBarang + "x0";
                    }else{
                        volumeBarang = volumeBarang + "x" + inputLebar.getText().toString();
                    }

                    if(inputTinggi.getText().length() == 0){
                        volumeBarang = volumeBarang + "x0";
                    }else{
                        volumeBarang = volumeBarang + "x" + inputTinggi.getText().toString();
                    }

                    konfirmasiSimpan();
                }
            }
        });

        cardPilihKategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                namaBarang = inputNamaBarang.getText().toString();
                deskripsiBarang = inputDeskripsiBarang.getText().toString();

                merk = inputMerk.getText().toString();
                berat = inputBerat.getText().toString();
                stok = inputStok.getText().toString();
                harga = inputHarga.getText().toString();
                bahanBarang = inputBahan.getText().toString();

                panjangBarang = inputPanjang.getText().toString();
                lebarBarang = inputLebar.getText().toString();
                tinggiBarang = inputTinggi.getText().toString();

                sessionManager.namaBarang(namaBarang);
                sessionManager.deskripsiBarang(deskripsiBarang);

                sessionManager.merkBarang(merk);
                sessionManager.beratBarang(berat);
                sessionManager.stokBarang(stok);
                sessionManager.hargaBarang(harga);
                sessionManager.bahanBarang(bahanBarang);
                sessionManager.panjangBarang(panjangBarang);
                sessionManager.lebarBarang(lebarBarang);
                sessionManager.tinggiBarang(tinggiBarang);
                Intent intent = new Intent(BarangActivity.this, KategoriActivity.class);
                startActivity(intent);
            }
        });

//        cardPilihDetail.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                namaBarang = inputNamaBarang.getText().toString();
//                deskripsiBarang = inputDeskripsiBarang.getText().toString();
//                sessionManager.namaBarang(namaBarang);
//                sessionManager.deskripsiBarang(deskripsiBarang);
//                Intent intent = new Intent(BarangActivity.this, DetailBarangActivity.class);
//                startActivity(intent);
//            }
//        });

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();

        // Change base URL to your upload server URL.
        barangService = new Retrofit.Builder().baseUrl("http://103.195.31.220:1147/").client(client).build().create(BarangService.class);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            sessionManager.clearBarang();
            Intent intent = new Intent(BarangActivity.this, NewHomeActivity.class);
            intent.putExtra("FRAGMENT_ID", 1);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onBackPressed() {
        sessionManager.clearBarang();
        Intent intent = new Intent(BarangActivity.this, NewHomeActivity.class);
        intent.putExtra("FRAGMENT_ID", 1);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        //super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //sessionManager.clearBarang();
    }



    private void selectImage() {

        namaBarang = inputNamaBarang.getText().toString();
        deskripsiBarang = inputDeskripsiBarang.getText().toString();

        merk = inputMerk.getText().toString();
        berat = inputBerat.getText().toString();
        stok = inputStok.getText().toString();
        harga = inputHarga.getText().toString();
        bahanBarang = inputBahan.getText().toString();

        panjangBarang = inputPanjang.getText().toString();
        lebarBarang = inputLebar.getText().toString();
        tinggiBarang = inputTinggi.getText().toString();

        sessionManager.namaBarang(namaBarang);
        sessionManager.deskripsiBarang(deskripsiBarang);

        sessionManager.merkBarang(merk);
        sessionManager.beratBarang(berat);
        sessionManager.stokBarang(stok);
        sessionManager.hargaBarang(harga);
        sessionManager.bahanBarang(bahanBarang);
        sessionManager.panjangBarang(panjangBarang);
        sessionManager.lebarBarang(lebarBarang);
        sessionManager.tinggiBarang(tinggiBarang);

        final CharSequence[] options = {"Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(BarangActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    int targetW = imageView1.getWidth();
                    int targetH = imageView1.getHeight();
                    //BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

//                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
//                            bitmapOptions);

                    bitmap = decodeFile(f, targetW,targetH);

                    //uploadImage(bitmap);

                    //profilePicture.setImageBitmap(bitmap);\\

                    String path = android.os.Environment
                            .getExternalStorageDirectory().getPath();
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        saveImage(file, bitmap);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Log.e("tes",filePath.toString());
                Log.e("tes",selectedImage.toString());

                String picturePath = "";
                if(String.valueOf(selectedImage).contains("file:")){
                    picturePath = selectedImage.getPath();
                }else{
                    Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    picturePath = c.getString(columnIndex);
                    c.close();
                }

                int targetW = imageView1.getWidth();
                int targetH = imageView1.getHeight();

                File newFile = new File(picturePath);
                thumbnail = decodeFile(newFile,targetW,targetH);
                //profilePicture.setImageBitmap(thumbnail);
                saveImage(newFile, thumbnail);
            }
        }
    }

    private void saveImage(final File newFile, final Bitmap myBitmap){


        Log.e("mananih", "mananih");
        progressDialog = ProgressDialog.show(this, "Menyimpan Foto", "Please wait ...", true );

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), newFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("foto_barang", newFile.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "foto_barang");

//            Log.d("THIS", data.getData().getPath());

        retrofit2.Call<okhttp3.ResponseBody> req = barangService.postImage(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.e("tes","hai");
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body().string()));
                        baseUtility.myToast("Foto berhasil disimpan sementara");
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if(Objects.equals(foto1, "")){
                                foto1 = newFile.getAbsolutePath();
                                urlfoto1 = jsonObject.getString("data");
                                imageView1.setImageBitmap(myBitmap);
                                //setPic(foto1, imageView1);
                                sessionManager.foto1(foto1);
                                sessionManager.urlfoto1(urlfoto1);
                                hapus1.setVisibility(View.VISIBLE);
                            }else if (Objects.equals(foto2, "")){
                                foto2 = newFile.getAbsolutePath();
                                urlfoto2 = jsonObject.getString("data");
                                imageView2.setImageBitmap(myBitmap);
                                //setPic(foto2, imageView2);
                                sessionManager.foto2(foto2);
                                sessionManager.urlfoto2(urlfoto2);
                                hapus2.setVisibility(View.VISIBLE);
                            }else if(Objects.equals(foto3, "")){
                                foto3 = newFile.getAbsolutePath();
                                urlfoto3 = jsonObject.getString("data");
                                imageView3.setImageBitmap(myBitmap);
                                //setPic(foto3, imageView3);
                                sessionManager.foto3(foto3);
                                sessionManager.urlfoto3(urlfoto3);
                                hapus3.setVisibility(View.VISIBLE);
                            }else if(Objects.equals(foto4, "")){
                                foto4 = newFile.getAbsolutePath();
                                urlfoto4 = jsonObject.getString("data");
                                imageView4.setImageBitmap(myBitmap);
                                //setPic(foto4, imageView4);
                                sessionManager.foto4(foto4);
                                sessionManager.urlfoto4(urlfoto4);
                                hapus4.setVisibility(View.VISIBLE);
                            }else if(Objects.equals(foto5, "")){
                                foto5 = newFile.getAbsolutePath();
                                urlfoto5 = jsonObject.getString("data");
                                imageView5.setImageBitmap(myBitmap);
                                //setPic(foto5, imageView5);
                                sessionManager.foto5(foto5);
                                sessionManager.urlfoto5(urlfoto5);
                                hapus5.setVisibility(View.VISIBLE);
                            }
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("tes","hgagalai");
                progressDialog.dismiss();
                baseUtility.myToast("Upload foto gagal, jaringan sibuk. Silahkan coba dengan jaringan lain");
                t.printStackTrace();
            }
        });
    }

    private void tambahBarangFunction(){
        Log.e("foto",foto1);
        Log.e("foto",sessionManager.getSubKategori());
        // TODO :
        // check username, email, password and rePassword value cant null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(inputNamaBarang.getText().length() == 0 || inputDeskripsiBarang.getText().length() == 0 || Objects.equals(foto1, "") || sessionManager.getSubKategori() == null || inputBerat.getText().length() == 0 || inputHarga.getText().length() == 0 || inputStok.getText().length() == 0){
                baseUtility.myToast("Form tidak boleh ada yang dikosongkan, wajib mengisi 1 gambar");
            }else{
                if(inputDeskripsiBarang.getText().length() < 30){
                    baseUtility.myToast("Deskripsi minimal 30 Karakter");
                }else{
                    // check input password and rePassword
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        //call API login and check response
                        progressDialog = ProgressDialog.show(this, "Tambah Barang", "Please wait ...", true );

                        namaBarang = inputNamaBarang.getText().toString();
                        deskripsiBarang = inputDeskripsiBarang.getText().toString();

                        merk = inputMerk.getText().toString();
                        berat = inputBerat.getText().toString();
                        stok = inputStok.getText().toString();
                        harga = inputHarga.getText().toString();
                        bahanBarang = inputBahan.getText().toString();

                        //Log.e("lihat", apiConfig.userEndpoint);

                        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/tambahBarang",
                                new com.android.volley.Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("TES", response.toString());
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            int code = jsonObject.getInt("code");
                                            String message = jsonObject.getString("message");

                                            if(code == 200){
                                                prefManager.setFirstTimeUpload(false);
                                                //String data = jsonObject.getString("data");
                                                //if success create session user
                                                //JSONObject jsonData = new JSONObject(data);
                                                //                                sessionManager.createUserLoginSession(
                                                //                                        jsonData.getString("id"),
                                                //                                        jsonData.getString("account_id"),
                                                //                                        jsonData.getString("email"),
                                                //                                        jsonData.getString("name"),
                                                //                                        jsonData.getString("password"),
                                                //                                        jsonData.getString("role")
                                                //                                );
                                                //                                //if success navigate to MainActivity
                                                progressDialog.dismiss();
                                                baseUtility.myToast(message);
                                                Intent intent = new Intent(BarangActivity.this, NewHomeActivity.class);
                                                intent.putExtra("FRAGMENT_ID", 1);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);baseUtility.myIntent(NewHomeActivity.class);
                                                sessionManager.clearBarang();
                                            }else{
                                                progressDialog.dismiss();
                                                Toast.makeText(BarangActivity.this, message, Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new com.android.volley.Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        Log.e("erornya", error.toString());
                                        baseUtility.myToast(error.toString());
                                    }
                                }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> map = new HashMap<String,String>();
                                map.put("id",sessionManager.getIdUser());
                                map.put("foto1",urlfoto1);
                                map.put("foto2",urlfoto2);
                                map.put("foto3",urlfoto3);
                                map.put("foto4",urlfoto4);
                                map.put("foto5",urlfoto5);
                                String newNama = namaBarang.trim();
                                map.put("nama",newNama);
                                map.put("subkategori",sessionManager.getSubKategori());
                                map.put("deskripsi",deskripsiBarang.trim());

//                            if(sessionManager.getMerk() != null){
//                                map.put("deskripsi","Merk : "+sessionManager.getMerk() + System.getProperty("line.separator") + deskripsiBarang.trim());
//                            }else {
//                                map.put("deskripsi",deskripsiBarang.trim());
//                            }

                                map.put("berat",berat);
                                map.put("bahan",bahanBarang);
                                map.put("volume",volumeBarang);
                                map.put("stok",stok);
                                map.put("merk",merk);

                                String a = harga;
                                String newString = a.replace(",","");
                                String lastString = newString.replaceAll("\\.","");

                                map.put("harga", lastString);
                                return map;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> headers = new HashMap<String, String>();
                                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                                headers.put("secretkey", "JackTheRipper");
                                return headers;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
                        requestQueue.add(stringRequest);
                    }
                }
            }
        }
    }

    private void editBarangFunction(){
        // TODO :
        // check username, email, password and rePassword value cant null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(inputNamaBarang.getText().length() == 0 || inputDeskripsiBarang.getText().length() == 0 || Objects.equals(foto1, "") || sessionManager.getSubKategori() == null || inputBerat.getText().length() == 0 || inputHarga.getText().length() == 0 || inputStok.getText().length() == 0){
                Log.e("cek",inputNamaBarang.getText().toString());
                Log.e("cek",inputDeskripsiBarang.getText().toString());
                baseUtility.myToast("Form tidak boleh ada yang dikosongkan, wajib mengisi 1 gambar");
            }else{
                if(inputDeskripsiBarang.getText().length() < 30){
                    baseUtility.myToast("Deskripsi minimal 30 Karakter");
                }else{
                    // check input password and rePassword
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        //call API login and check response
                        progressDialog = ProgressDialog.show(this, "Edit Barang", "Please wait ...", true );

                        namaBarang = inputNamaBarang.getText().toString();
                        deskripsiBarang = inputDeskripsiBarang.getText().toString();

                        merk = inputMerk.getText().toString();
                        berat = inputBerat.getText().toString();
                        stok = inputStok.getText().toString();
                        harga = inputHarga.getText().toString();
                        bahanBarang = inputBahan.getText().toString();

                        //Log.e("lihat", apiConfig.userEndpoint);

                        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/barang/editBarang/"+sessionManager.getIdBarang(),
                                new com.android.volley.Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.e("TES", response.toString());
                                        try {
                                            JSONObject jsonObject = new JSONObject(response);
                                            int code = jsonObject.getInt("code");
                                            String message = jsonObject.getString("message");

                                            if(code == 200){
                                                //String data = jsonObject.getString("data");
                                                //if success create session user
                                                //JSONObject jsonData = new JSONObject(data);
                                                //                                sessionManager.createUserLoginSession(
                                                //                                        jsonData.getString("id"),
                                                //                                        jsonData.getString("account_id"),
                                                //                                        jsonData.getString("email"),
                                                //                                        jsonData.getString("name"),
                                                //                                        jsonData.getString("password"),
                                                //                                        jsonData.getString("role")
                                                //                                );
                                                //                                //if success navigate to MainActivity
                                                progressDialog.dismiss();
                                                baseUtility.myToast(message);
                                                Intent intent = new Intent(BarangActivity.this, NewHomeActivity.class);
                                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(intent);baseUtility.myIntent(NewHomeActivity.class);
                                                sessionManager.clearBarang();
                                            }else{
                                                progressDialog.dismiss();
                                                Toast.makeText(BarangActivity.this, message, Toast.LENGTH_SHORT).show();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                },
                                new com.android.volley.Response.ErrorListener() {
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        progressDialog.dismiss();
                                        Log.e("erornya", error.toString());
                                        baseUtility.myToast(error.toString());
                                    }
                                }){
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {
                                Map<String,String> map = new HashMap<String,String>();
                                map.put("foto1",urlfoto1);
                                map.put("foto2",urlfoto2);
                                map.put("foto3",urlfoto3);
                                map.put("foto4",urlfoto4);
                                map.put("foto5",urlfoto5);
                                String newNama = namaBarang.trim();
                                map.put("nama",newNama);
                                map.put("subkategori",sessionManager.getSubKategori());
                                map.put("deskripsi",deskripsiBarang.trim());

//                            if(sessionManager.getMerk() != null){
//                                map.put("deskripsi","Merk : "+sessionManager.getMerk() + System.getProperty("line.separator") + deskripsiBarang.trim());
//                            }else {
//                                map.put("deskripsi",deskripsiBarang.trim());
//                            }

                                map.put("berat",berat);
                                map.put("bahan",bahanBarang);
                                map.put("volume",volumeBarang);
                                map.put("stok",stok);
                                map.put("merk",merk);

                                String a = harga;
                                String newString = a.replace(",","");
                                String lastString = newString.replaceAll("\\.","");

                                map.put("harga", lastString);
                                map.put("deskripsiEdit", deskripsiEdit);
                                return map;
                            }

                            @Override
                            public Map<String, String> getHeaders() throws AuthFailureError {
                                Map<String, String> headers = new HashMap<String, String>();
                                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                                headers.put("secretkey", "JackTheRipper");
                                return headers;
                            }
                        };

                        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
                        requestQueue.add(stringRequest);
                    }
                }
            }
        }
    }

    private void checkBarang(){
        if(sessionManager.getFoto1() != null){
            foto1 = sessionManager.getFoto1();
            urlfoto1 = sessionManager.geturlFoto1();

            Log.e("FOTO1", foto1);
            if(foto1.contains("http://103")){
                String tempf1 = foto1.replaceAll(" ", "%20");
                Picasso.with(this).load(tempf1+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView1);
            }else {
                int targetW = imageView1.getWidth();
                int targetH = imageView1.getHeight();
                File file = new File(foto1);
                Bitmap bBitmap = decodeFile(file,targetW,targetH);
                imageView1.setImageBitmap(bBitmap);
                //Picasso.with(this).load(foto1).fit().into(imageView1);
            }
            hapus1.setVisibility(View.VISIBLE);
        }
        if(sessionManager.getFoto2() != null){
            foto2 = sessionManager.getFoto2();
            urlfoto2 = sessionManager.geturlFoto2();

            if(foto2.contains("http://103")){
                String tempf2 = foto2.replaceAll(" ", "%20");
                Picasso.with(this).load(tempf2+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView2);
            }else {
                int targetW = imageView2.getWidth();
                int targetH = imageView2.getHeight();
                File file = new File(foto2);
                Bitmap bBitmap = decodeFile(file,targetW,targetH);
                imageView2.setImageBitmap(bBitmap);
                //Picasso.with(this).load(foto2).fit().into(imageView2);
            }
            hapus2.setVisibility(View.VISIBLE);
        }
        if(sessionManager.getFoto3() != null){
            foto3 = sessionManager.getFoto3();
            urlfoto3 = sessionManager.geturlFoto3();

            if(foto3.contains("http://103")){
                String tempf3 = foto3.replaceAll(" ", "%20");
                Picasso.with(this).load(tempf3+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView3);
            }else {
                int targetW = imageView3.getWidth();
                int targetH = imageView3.getHeight();
                File file = new File(foto3);
                Bitmap bBitmap = decodeFile(file,targetW,targetH);
                imageView3.setImageBitmap(bBitmap);
                //Picasso.with(this).load(foto3).fit().into(imageView3);
            }
            hapus3.setVisibility(View.VISIBLE);
        }
        if(sessionManager.getFoto4() != null){
            foto4 = sessionManager.getFoto4();
            urlfoto4 = sessionManager.geturlFoto4();

            if(foto4.contains("http://103")){
                String tempf4 = foto4.replaceAll(" ", "%20");
                Picasso.with(this).load(tempf4+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView4);
            }else {
                int targetW = imageView4.getWidth();
                int targetH = imageView4.getHeight();
                File file = new File(foto4);
                Bitmap bBitmap = decodeFile(file,targetW,targetH);
                imageView4.setImageBitmap(bBitmap);
                //Picasso.with(this).load(foto4).fit().into(imageView4);
            }
            hapus4.setVisibility(View.VISIBLE);
        }
        if(sessionManager.getFoto5() != null){
            foto5 = sessionManager.getFoto5();
            urlfoto5 = sessionManager.geturlFoto5();

            if(foto5.contains("http://103")){
                String tempf5 = foto5.replaceAll(" ", "%20");
                Picasso.with(this).load(tempf5+"?size=medium").placeholder( R.drawable.animation_loading ).into(imageView5);
            }else {
                int targetW = imageView5.getWidth();
                int targetH = imageView5.getHeight();
                File file = new File(foto5);
                Bitmap bBitmap = decodeFile(file,targetW,targetH);
                imageView5.setImageBitmap(bBitmap);
                //Picasso.with(this).load(foto5).fit().into(imageView5);
            }
            hapus5.setVisibility(View.VISIBLE);
        }
        if(sessionManager.getNamaBarang() != null){
             inputNamaBarang.setText(sessionManager.getNamaBarang());
        }
        if(sessionManager.getDeskripsi() != null){
            inputDeskripsiBarang.setText(sessionManager.getDeskripsi());
        }
        ///////////////////////// BARUU /////////////////////////////////
        if(sessionManager.getHarga() != null){
            inputHarga.setText(sessionManager.getHarga());
        }
        if(sessionManager.getStok() != null){
            inputStok.setText(sessionManager.getStok());
        }
        if(sessionManager.getBerat() != null){
            inputBerat.setText(sessionManager.getBerat());
        }
        if(sessionManager.getMerk() != null){
            inputMerk.setText(sessionManager.getMerk());
        }
        if(sessionManager.getPanjangBarang() != null){
            inputPanjang.setText(sessionManager.getPanjangBarang());
        }
        if(sessionManager.getLebarBarang() != null){
            inputLebar.setText(sessionManager.getLebarBarang());
        }
        if(sessionManager.getTinggiBarang() != null){
            inputTinggi.setText(sessionManager.getTinggiBarang());
        }
        if(sessionManager.getBahanBarang() != null){
            inputBahan.setText(sessionManager.getBahanBarang());
        }
        ////////////////////////// BARU ////////////////////////////////

        if(sessionManager.getSubKategori() != null){
            textKategori.setText(sessionManager.getTextSubKategori());
            textKategori.setTextColor(Color.GREEN);
        }else{
            textKategori.setText("Belum diisi");
            textKategori.setTextColor(Color.RED);
        }
//        if(sessionManager.getStok() == null || sessionManager.getBerat() == null || sessionManager.getHarga() == null){
//            detailBarang.setText("Belum diisi");
//        }else {
//            detailBarang.setText("Sudah diisi");
//        }
    }

    private void lihatGambar(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BarangActivity.this);
        alertDialog.setMessage("Contoh Foto");

        LayoutInflater inflater = LayoutInflater.from(BarangActivity.this);
        final View dialogView = inflater.inflate(R.layout.dialog_box_show_image, null);

        alertDialog.setView(dialogView);

        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void cekUpload(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(BarangActivity.this);
        alertDialog.setTitle("Hai seller !");
        alertDialog.setMessage("Harga di marketplace akan di-markup menyesuaikan biaya operasional lebih kurang 5%");
        alertDialog.setPositiveButton("Setuju", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialog, int which) {
               tambahBarangFunction();
               dialog.cancel();
            }
        });
        alertDialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alertDialog.show();
    }

    private void hapusFoto(){
        hapus1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foto1 = foto2;
                foto2 = foto3;
                foto3 = foto4;
                foto4 = foto5;
                foto5 = "";
                urlfoto1 = urlfoto2;
                urlfoto2 = urlfoto3;
                urlfoto3 = urlfoto4;
                urlfoto4 = urlfoto5;
                urlfoto5 = "";
                afterDelete();
            }
        });
        hapus2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foto2 = foto3;
                foto3 = foto4;
                foto4 = foto5;
                foto5 = "";
                urlfoto2 = urlfoto3;
                urlfoto3 = urlfoto4;
                urlfoto4 = urlfoto5;
                urlfoto5 = "";
                afterDelete();
            }
        });
        hapus3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foto3 = foto4;
                foto4 = foto5;
                foto5 = "";
                urlfoto3 = urlfoto4;
                urlfoto4 = urlfoto5;
                urlfoto5 = "";
                afterDelete();
            }
        });
        hapus4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foto4 = foto5;
                foto5 = "";
                urlfoto4 = urlfoto5;
                urlfoto5 = "";
                afterDelete();
            }
        });
        hapus5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                foto5 = "";
                urlfoto5 = "";
                afterDelete();
            }
        });
    }

    public void afterDelete(){

        namaBarang = inputNamaBarang.getText().toString();
        deskripsiBarang = inputDeskripsiBarang.getText().toString();

        merk = inputMerk.getText().toString();
        berat = inputBerat.getText().toString();
        stok = inputStok.getText().toString();
        harga = inputHarga.getText().toString();
        bahanBarang = inputBahan.getText().toString();

        panjangBarang = inputPanjang.getText().toString();
        lebarBarang = inputLebar.getText().toString();
        tinggiBarang = inputTinggi.getText().toString();

        sessionManager.namaBarang(namaBarang);
        sessionManager.deskripsiBarang(deskripsiBarang);

        sessionManager.merkBarang(merk);
        sessionManager.beratBarang(berat);
        sessionManager.stokBarang(stok);
        sessionManager.hargaBarang(harga);
        sessionManager.bahanBarang(bahanBarang);
        sessionManager.panjangBarang(panjangBarang);
        sessionManager.lebarBarang(lebarBarang);
        sessionManager.tinggiBarang(tinggiBarang);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(!Objects.equals(foto4, "") && !Objects.equals(urlfoto4, "")){
                sessionManager.deleteFoto5();
                sessionManager.foto4(foto4);
                sessionManager.foto3(foto3);
                sessionManager.foto2(foto2);
                sessionManager.foto1(foto1);
                sessionManager.deleteurlFoto5();
                sessionManager.urlfoto4(urlfoto4);
                sessionManager.urlfoto3(urlfoto3);
                sessionManager.urlfoto2(urlfoto2);
                sessionManager.urlfoto1(urlfoto1);
            }else if(!Objects.equals(foto3, "") && !Objects.equals(urlfoto3, "")){
                sessionManager.deleteFoto4();
                sessionManager.foto3(foto3);
                sessionManager.foto2(foto2);
                sessionManager.foto1(foto1);
                sessionManager.deleteurlFoto4();
                sessionManager.urlfoto3(urlfoto3);
                sessionManager.urlfoto2(urlfoto2);
                sessionManager.urlfoto1(urlfoto1);
            }else if(!Objects.equals(foto2, "") && !Objects.equals(urlfoto2, "")){
                sessionManager.deleteFoto3();
                sessionManager.foto2(foto2);
                sessionManager.foto1(foto1);
                sessionManager.deleteurlFoto3();
                sessionManager.urlfoto2(urlfoto2);
                sessionManager.urlfoto1(urlfoto1);
            }else if(!Objects.equals(foto1, "") && !Objects.equals(urlfoto1, "")){
                sessionManager.deleteFoto2();
                sessionManager.foto1(foto1);
                sessionManager.deleteurlFoto2();
                sessionManager.urlfoto1(urlfoto1);
            }else if(Objects.equals(foto1, "") && Objects.equals(urlfoto1, "")){
                sessionManager.deleteFoto1();
                sessionManager.deleteurlFoto1();
            }
            Intent intent = new Intent(BarangActivity.this, BarangActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private void konfirmasiSimpan(){
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(BarangActivity.this);
        alertDialog.setTitle("Info Barang");
        alertDialog.setMessage("Pastikan tidak ada data yang salah sebelum menyimpan");

        LayoutInflater inflater = LayoutInflater.from(BarangActivity.this);
        final View dialogView = inflater.inflate(R.layout.dialog_detail_barang, null);

        TextView namaB = (TextView)dialogView.findViewById(R.id.namaB);
        TextView kategoriB = (TextView)dialogView.findViewById(R.id.kategoriB);
        TextView merkB = (TextView)dialogView.findViewById(R.id.merkB);
        TextView hargaB = (TextView)dialogView.findViewById(R.id.hargaB);
        TextView stokB = (TextView)dialogView.findViewById(R.id.stokB);
        TextView beratB = (TextView)dialogView.findViewById(R.id.beratB);
        TextView deskripsiB = (TextView)dialogView.findViewById(R.id.deskripsiB);
        TextView volumeB = (TextView)dialogView.findViewById(R.id.volumeB);
        TextView bahanB = (TextView)dialogView.findViewById(R.id.bahanB);

        Button oksimpan = (Button) dialogView.findViewById(R.id.oksimpan);
        Button batalsimpan = (Button) dialogView.findViewById(R.id.batalsimpan);

        namaB.setText(inputNamaBarang.getText().toString());
        deskripsiB.setText(inputDeskripsiBarang.getText().toString());
        kategoriB.setText(sessionManager.getTextSubKategori());
        if(inputMerk.getText().length() != 0){
            merkB.setText(inputMerk.getText().toString());
        }else {
            merkB.setText("-");
        }
        hargaB.setText(inputHarga.getText().toString());
        stokB.setText(inputStok.getText().toString());
        beratB.setText(inputBerat.getText().toString());

        volumeB.setText(volumeBarang);

        if(inputBahan.getText().length() != 0){
            bahanB.setText(inputBahan.getText().toString());
        }else {
            bahanB.setText("-");
        }

        alertDialog.setView(dialogView);

        final AlertDialog test = alertDialog.create();

        oksimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(getIntent().hasExtra("namaBarang")){
                    String namaBarangNya = getIntent().getExtras().getString("namaBarang");
                    String namaBarangSplit[] = namaBarangNya.split("sk-");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(inputNamaBarang.getText().toString(), namaBarangSplit[0].trim())){
                            deskripsiEdit = deskripsiEdit + "Nama barang,";
                        }
                        if(!Objects.equals(inputDeskripsiBarang.getText().toString(), getIntent().getExtras().getString("deskripsi").trim())){
                            deskripsiEdit = deskripsiEdit + "Deskripsi,";
                        }
                        if(!Objects.equals(inputBahan.getText().toString(), getIntent().getExtras().getString("bahanBarang"))){
                            deskripsiEdit = deskripsiEdit + "Bahan,";
                        }
                        if(!Objects.equals(inputMerk.getText().toString(), getIntent().getExtras().getString("merkBarang"))){
                            deskripsiEdit = deskripsiEdit + "Merk,";
                        }
                        if(!Objects.equals(inputBerat.getText().toString(), String.valueOf(getIntent().getExtras().getInt("beratBarang")))){
                            deskripsiEdit = deskripsiEdit + "Berat,";
                        }
                    }

                    String a = inputHarga.getText().toString();
                    String newString = a.replace(",","");
                    String lastString = newString.replaceAll("\\.","");

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(lastString, String.valueOf(getIntent().getExtras().getInt("hargaBarang")))){
                            deskripsiEdit = deskripsiEdit + "Harga,";
                        }
                        if(!Objects.equals(inputStok.getText().toString(), String.valueOf(getIntent().getExtras().getInt("stokBarang")))){
                            deskripsiEdit = deskripsiEdit + "Stok,";
                        }
                        if(!Objects.equals(sessionManager.getSubKategori(), String.valueOf(getIntent().getExtras().getInt("idSubkategori")))){
                            deskripsiEdit = deskripsiEdit + "Kategori,";
                        }
                        if(!Objects.equals(sessionManager.geturlFoto1(), getIntent().getExtras().getString("fotoBarang"))){
                            deskripsiEdit = deskripsiEdit + "Foto,";
                        }
                    }

                    String volumeBarangNya = getIntent().getExtras().getString("volumeBarang");
                    String[] plt = volumeBarangNya.split("x");
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(inputPanjang.getText().toString(), plt[0]) || !Objects.equals(inputLebar.getText().toString(), plt[1]) || !Objects.equals(inputTinggi.getText().toString(), plt[2])){
                            deskripsiEdit = deskripsiEdit + "Volume,";
                        }
                    }

                    String fotoTambahanBarang = getIntent().getExtras().getString("fotoTambahan");
                    try {
                        JSONArray jsonArray = new JSONArray(fotoTambahanBarang);
                        String temp2 = sessionManager.geturlFoto2() , temp3 = sessionManager.geturlFoto3(), temp4 = sessionManager.geturlFoto4(), temp5 = sessionManager.geturlFoto5();
                        for(int i = 0; i<jsonArray.length(); i++){
                            if(i == 0){
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                temp2 = jsonObject1.getString("foto");
                            }else if(i ==  1){
                                JSONObject jsonObject2 = jsonArray.getJSONObject(i);
                                temp3 = jsonObject2.getString("foto");
                            }else if(i == 2){
                                JSONObject jsonObject3 = jsonArray.getJSONObject(i);
                                temp4 = jsonObject3.getString("foto");
                            }else if(i == 3){
                                JSONObject jsonObject4 = jsonArray.getJSONObject(i);
                                temp5 = jsonObject4.getString("foto");
                            }
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                            if(!Objects.equals(sessionManager.geturlFoto2(), temp2) || !Objects.equals(sessionManager.geturlFoto3(), temp3) || !Objects.equals(sessionManager.geturlFoto4(), temp4) || !Objects.equals(sessionManager.geturlFoto5(), temp5)){
                                deskripsiEdit = deskripsiEdit + "Foto Tambahan,";
                            }
                            if(Objects.equals(sessionManager.geturlFoto2(), temp2) &&
                                    Objects.equals(sessionManager.geturlFoto3(), temp3) &&
                                    Objects.equals(sessionManager.geturlFoto4(), temp4) &&
                                    Objects.equals(sessionManager.geturlFoto5(), temp5) &&
                                    Objects.equals(inputPanjang.getText().toString(), plt[0]) &&
                                    Objects.equals(inputLebar.getText().toString(), plt[1]) &&
                                    Objects.equals(inputTinggi.getText().toString(), plt[2]) &&
                                    Objects.equals(sessionManager.geturlFoto1(), getIntent().getExtras().getString("fotoBarang")) &&
                                    Objects.equals(sessionManager.getSubKategori(), String.valueOf(getIntent().getExtras().getInt("idSubkategori"))) &&
                                    Objects.equals(inputStok.getText().toString(), String.valueOf(getIntent().getExtras().getInt("stokBarang"))) &&
                                    Objects.equals(lastString, String.valueOf(getIntent().getExtras().getInt("hargaBarang"))) &&
                                    Objects.equals(inputBerat.getText().toString(), String.valueOf(getIntent().getExtras().getInt("beratBarang"))) &&
                                    Objects.equals(inputMerk.getText().toString(), getIntent().getExtras().getString("merkBarang")) &&
                                    Objects.equals(inputBahan.getText().toString(), getIntent().getExtras().getString("bahanBarang")) &&
                                    Objects.equals(inputDeskripsiBarang.getText().toString(), getIntent().getExtras().getString("deskripsi").trim()) &&
                                    Objects.equals(inputNamaBarang.getText().toString(), namaBarangSplit[0].trim())
                                    ){
                                baseUtility.myToast("Tidak ada perubahan sama sekali");
                            }else{
                                if(!Objects.equals(sessionManager.getIdBarang(), null)){
                                    editBarangFunction();
                                }else{
                                    if(prefManager.isFirstTimeUpload()){
                                        cekUpload();
                                    }else {
                                        tambahBarangFunction();
                                    }
                                }
                                test.dismiss();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(sessionManager.getIdBarang(), null)){
                            editBarangFunction();
                        }else{
                            if(prefManager.isFirstTimeUpload()){
                                cekUpload();
                            }else {
                                tambahBarangFunction();
                            }
                        }
                    }
                    test.dismiss();
                }
            }
        });

        batalsimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                test.dismiss();
            }
        });

        test.show();
        //alertDialog.show();
    }

    private void setPic(String mCurrentPhotoPath, ImageView mImageView) {
//        // Get the dimensions of the View
//        int targetW = mImageView.getWidth();
//        int targetH = mImageView.getHeight();
//
//        // Get the dimensions of the bitmap
//        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
//        bmOptions.inJustDecodeBounds = true;
//        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
//        int photoW = bmOptions.outWidth;
//        int photoH = bmOptions.outHeight;
//
//        // Determine how much to scale down the image
//        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
//
//        // Decode the image file into a Bitmap sized to fill the View
//        bmOptions.inJustDecodeBounds = false;
//        bmOptions.inSampleSize = scaleFactor;
//        bmOptions.inPurgeable = true;

        int targetW = imageView1.getWidth();
        int targetH = imageView1.getHeight();
        File file = new File(mCurrentPhotoPath);
        Bitmap bBitmap = decodeFile(file,targetW,targetH);
        mImageView.setImageBitmap(bBitmap);
    }

    public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    public class NumberTextWatcher implements TextWatcher {

        private final DecimalFormat df;
        private final DecimalFormat dfnd;
        private final EditText et;
        private boolean hasFractionalPart;
        private int trailingZeroCount;

        public NumberTextWatcher(EditText editText, String pattern) {
            df = new DecimalFormat(pattern);
            df.setDecimalSeparatorAlwaysShown(true);
            dfnd = new DecimalFormat("#,###");
            this.et = editText;
            hasFractionalPart = false;
        }

        @Override
        public void afterTextChanged(Editable s) {
            et.removeTextChangedListener(this);

            if (s != null && !s.toString().isEmpty()) {
                try {
                    int inilen, endlen;
                    inilen = et.getText().length();
                    String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "").replace("$","");
                    Number n = df.parse(v);
                    int cp = et.getSelectionStart();
                    if (hasFractionalPart) {
                        StringBuilder trailingZeros = new StringBuilder();
                        while (trailingZeroCount-- > 0)
                            trailingZeros.append('0');
                        et.setText(df.format(n) + trailingZeros.toString());
                    } else {
                        et.setText(dfnd.format(n));
                    }
                    et.setText(et.getText().toString());
                    endlen = et.getText().length();
                    int sel = (cp + (endlen - inilen));
                    if (sel > 0 && sel < et.getText().length()) {
                        et.setSelection(sel);
                    } else if (trailingZeroCount > -1) {
                        et.setSelection(et.getText().length());
                    } else {
                        et.setSelection(et.getText().length());
                    }
                } catch (NumberFormatException | ParseException e) {
                    e.printStackTrace();
                }
            }

            et.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int index = s.toString().indexOf(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));
            trailingZeroCount = 0;
            if (index > -1) {
                for (index++; index < s.length(); index++) {
                    if (s.charAt(index) == '0')
                        trailingZeroCount = 0;
                    else {
                        trailingZeroCount = 0;
                    }
                }
                hasFractionalPart = false;
            } else {
                hasFractionalPart = false;
            }
        }
    }

}
