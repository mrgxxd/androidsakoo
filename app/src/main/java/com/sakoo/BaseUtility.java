package com.sakoo;


import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.widget.Toast;

public class BaseUtility {

    private Context myContext;

    public BaseUtility(Context mContext){
        this.myContext = mContext;
    }

    public void myIntent(Class myClass){
        Intent intent = new Intent(myContext, myClass);
        myContext.startActivity(intent);
    }

    public void myToast(String myString){
        Toast.makeText(myContext, myString, Toast.LENGTH_SHORT).show();
    }
}
