package com.sakoo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class BroadcastReceiverOnBootComplete extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)) {
            Log.e("tes", "dapat");
            Intent serviceIntent = new Intent(context, LoginActivity.class);
            context.startService(serviceIntent);
        }else {
            Log.e("tes", "tidak dapat");
        }
    }
}
