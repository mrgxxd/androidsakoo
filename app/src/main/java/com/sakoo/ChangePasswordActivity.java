package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText passwordLama, passwordBaru, KonfirmPass;
    private Button simpan;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private String pLama, pBaru, token;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        String x = getIntent().getData().toString();
        String [] tempx = x.split("=");
        token = tempx[1];



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sessionManager = new SessionManager(this);
        baseUtility = new BaseUtility(this);

        //passwordLama = (EditText)findViewById(R.id.inputPasswordLama);
        passwordBaru = (EditText)findViewById(R.id.inputPasswordBaru);
        KonfirmPass = (EditText)findViewById(R.id.inputKonfirmasiPassword);


        simpan = (Button) findViewById(R.id.simpan_password);

        simpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(passwordBaru.getText().toString(), KonfirmPass.getText().toString())) {
                        gantiPassword();
                    } else {
                        Toast.makeText(ChangePasswordActivity.this, "Password Baru dan Konfirmasi Password Baru Tidak Cocok", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            sessionManager.hapusAlSementara();
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void gantiPassword(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(passwordBaru.getText().length() == 0 ){
                baseUtility.myToast("Form tidak boleh kosong");
            }else{

                //pLama = passwordLama.getText().toString();
                pBaru = passwordBaru.getText().toString();

                progressDialog = ProgressDialog.show(this, "Simpan Password", "Please wait ...", true );

                //Log.e("lihat", apiConfig.userEndpoint);

                StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/user/gantiPassword",
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("TES", response.toString());
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    int code = jsonObject.getInt("code");
                                    String message = jsonObject.getString("message");

                                    progressDialog.dismiss();

                                    if(code == 200){
                                        Toast.makeText(ChangePasswordActivity.this, "Ganti password berhasil, silahkan login ulang untuk verifikasi", Toast.LENGTH_LONG).show();
                                        Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                        finish();
                                    }else{
                                        Toast.makeText(ChangePasswordActivity.this, "Ganti password pagal, silahkan ulangi proses", Toast.LENGTH_LONG).show();
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                progressDialog.dismiss();
                                Log.e("erornya", error.toString());
                                baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                            }
                        }){
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String,String> map = new HashMap<String,String>();
                        map.put("passwordBaru",pBaru);
                        return map;
                    }

                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        Map<String, String> headers = new HashMap<String, String>();
                        //headers.put("Content-Type", "application/x-www-form-urlencoded");
                        headers.put("secretkey", "JackTheRipper");
                        headers.put("token", token);
                        return headers;
                    }
                };

                RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
                requestQueue.add(stringRequest);
            }
        }
    }
}
