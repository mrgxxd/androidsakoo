package com.sakoo;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomGridView extends BaseAdapter {

    private Context mContext;
    private final ArrayList<String> id, gridViewString, gridViewImageId;
    private ImageView imageViewAndroid;

    public CustomGridView(Context context, ArrayList<String> _id, ArrayList<String> _gridViewString, ArrayList<String> _gridViewImageId) {
        mContext = context;
        this.gridViewImageId = _gridViewImageId;
        this.gridViewString = _gridViewString;
        this.id = _id;
    }

    @Override
    public int getCount() {
        return gridViewString.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridViewAndroid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            gridViewAndroid = new View(mContext);
            gridViewAndroid = inflater.inflate(R.layout.grid_view_menu, null);
            TextView textViewAndroid = (TextView) gridViewAndroid.findViewById(R.id.android_gridview_text);
            textViewAndroid.setTextColor(Color.DKGRAY);
            imageViewAndroid = (ImageView) gridViewAndroid.findViewById(R.id.android_gridview_image);
            textViewAndroid.setText(gridViewString.get(i));
            Log.e("new",gridViewString.get(i));
            //imageViewAndroid.setImageResource(gridViewImageId[i]);
            String awal = gridViewImageId.get(i);
            String newUrl = awal.replace("\\","");
            Log.e("new",newUrl);
            Picasso.with(mContext).load(newUrl).into(imageViewAndroid);
        } else {
            gridViewAndroid = (View) convertView;
        }

        return gridViewAndroid;
    }
}
