package com.sakoo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "MyDBName.db";
    public static final String CONTACTS_TABLE_NAME = "notification";
    public static final String CONTACTS_COLUMN_ID = "id";
    public static final String CONTACTS_COLUMN_TITLE = "title";
    public static final String CONTACTS_COLUMN_BODY = "body";
    public static final String CONTACTS_COLUMN_TYPEID = "typeId";
    public static final String CONTACTS_COLUMN_TYPE = "type";
    public static final String CONTACTS_COLUMN_STATUS = "status";
    public static final String CONTACTS_COLUMN_DATE = "date";
    private HashMap notif;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        db.execSQL(
                "create table notification " +
                        "(id integer primary key, title text, body text, typeId integer, type text, status boolean, date text)"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS notification");
        onCreate(db);
    }

    public void deleteData(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ CONTACTS_TABLE_NAME);
    }

    public boolean insertContact (String title, String body, int typeId, String type, String date) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CONTACTS_COLUMN_TITLE, title);
        contentValues.put(CONTACTS_COLUMN_BODY, body);
        contentValues.put(CONTACTS_COLUMN_TYPEID, typeId);
        contentValues.put(CONTACTS_COLUMN_TYPE, type);
        contentValues.put(CONTACTS_COLUMN_STATUS, false);
        contentValues.put(CONTACTS_COLUMN_DATE, date);
        db.insert(CONTACTS_TABLE_NAME, null, contentValues);
        return true;
    }

    public int getAllData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from notification WHERE status=0", null );
        int hasil = res.getCount();
        return hasil;
    }

    public Cursor getNotifData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from notification ORDER BY id DESC", null );
        return res;
    }

    public int getTransaksiData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from notification WHERE status=0 AND type='transaksi'", null );
        int hasil = res.getCount();
        return hasil;
    }

    public Boolean upTransaksiData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "update notification set status = 1 where status = 0 AND type='transaksi'", null );
        return true;
    }

    public int getBarangData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from notification WHERE status=0 AND type='barang'", null );
        int hasil = res.getCount();
        return hasil;
    }

    public Boolean upBarangData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "update notification set status = 1 where status = 0 AND type='barang'", null );
        return true;
    }

    public int getSaldoData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from notification WHERE status=0 AND type='saldo'", null );
        int hasil = res.getCount();
        return hasil;
    }

    public Boolean upSaldoData() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "update notification set status = 1 where status = 0 AND type='saldo'", null );
        return true;
    }

    public int numberOfRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, CONTACTS_TABLE_NAME);
        return numRows;
    }

    public boolean updateNotif(){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("status", true);
        db.update("notification", contentValues, "status = ? ", new String[] { Integer.toString(0) } );
        return true;
    }

//    public boolean updateContact (Integer id, String name, String phone, String email, String street,String place) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        ContentValues contentValues = new ContentValues();
//        contentValues.put("name", name);
//        contentValues.put("phone", phone);
//        contentValues.put("email", email);
//        contentValues.put("street", street);
//        contentValues.put("place", place);
//        db.update("contacts", contentValues, "id = ? ", new String[] { Integer.toString(id) } );
//        return true;
//    }
//
//    public Integer deleteContact (Integer id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        return db.delete("contacts",
//                "id = ? ",
//                new String[] { Integer.toString(id) });
//    }

//    public ArrayList<String> getAllCotacts() {
//        ArrayList<String> array_list = new ArrayList<String>();
//
//        //hp = new HashMap();
//        SQLiteDatabase db = this.getReadableDatabase();
//        Cursor res =  db.rawQuery( "select * from contacts", null );
//        res.moveToFirst();
//
//        while(res.isAfterLast() == false){
//            array_list.add(res.getString(res.getColumnIndex(CONTACTS_COLUMN_NAME)));
//            res.moveToNext();
//        }
//        return array_list;
//    }
}
