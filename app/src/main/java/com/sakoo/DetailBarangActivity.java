package com.sakoo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Objects;

public class DetailBarangActivity extends AppCompatActivity {

    private Button simpan_detail;

    private EditText inputMerk, inputBerat, inputStok, inputHarga;
    private String merk, berat, stok, harga;

    private BaseUtility baseUtility;
    private SessionManager sessionManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_barang);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);

        inputMerk = (EditText)findViewById(R.id.inputMerk);
        inputBerat = (EditText)findViewById(R.id.inputBerat);
        inputHarga = (EditText)findViewById(R.id.inputHarga);
        inputHarga.addTextChangedListener(new NumberTextWatcher(inputHarga, "#,###"));
        inputStok = (EditText)findViewById(R.id.inputStok);

        cekDetail();

        simpan_detail = (Button)findViewById(R.id.simpan_detail);
        simpan_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputBerat.getText().length() == 0 || inputHarga.getText().length() == 0 || inputStok.getText().length() == 0){
                    baseUtility.myToast("Form harus dilengkapi");
                }else{
                    berat = inputBerat.getText().toString();
                    stok = inputStok.getText().toString();
                    harga = inputHarga.getText().toString();
                    merk = inputMerk.getText().toString();

                    sessionManager.beratBarang(berat);
                    sessionManager.stokBarang(stok);
                    sessionManager.hargaBarang(harga);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(merk, "")){
                            sessionManager.merkBarang(merk);
                        }
                    }
                    Intent intent = new Intent(DetailBarangActivity.this, BarangActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void cekDetail(){
        if(sessionManager.getHarga() != null){
            inputHarga.setText(sessionManager.getHarga());
        }
        if(sessionManager.getStok() != null){
            inputStok.setText(sessionManager.getStok());
        }
        if(sessionManager.getBerat() != null){
            inputBerat.setText(sessionManager.getBerat());
        }
        if(sessionManager.getMerk() != null){
            inputBerat.setText(sessionManager.getMerk());
        }
    }

    public class NumberTextWatcher implements TextWatcher {

        private final DecimalFormat df;
        private final DecimalFormat dfnd;
        private final EditText et;
        private boolean hasFractionalPart;
        private int trailingZeroCount;

        public NumberTextWatcher(EditText editText, String pattern) {
            df = new DecimalFormat(pattern);
            df.setDecimalSeparatorAlwaysShown(true);
            dfnd = new DecimalFormat("#,###");
            this.et = editText;
            hasFractionalPart = false;
        }

        @Override
        public void afterTextChanged(Editable s) {
            et.removeTextChangedListener(this);

            if (s != null && !s.toString().isEmpty()) {
                try {
                    int inilen, endlen;
                    inilen = et.getText().length();
                    String v = s.toString().replace(String.valueOf(df.getDecimalFormatSymbols().getGroupingSeparator()), "").replace("$","");
                    Number n = df.parse(v);
                    int cp = et.getSelectionStart();
                    if (hasFractionalPart) {
                        StringBuilder trailingZeros = new StringBuilder();
                        while (trailingZeroCount-- > 0)
                            trailingZeros.append('0');
                        et.setText(df.format(n) + trailingZeros.toString());
                    } else {
                        et.setText(dfnd.format(n));
                    }
                    et.setText(et.getText().toString());
                    endlen = et.getText().length();
                    int sel = (cp + (endlen - inilen));
                    if (sel > 0 && sel < et.getText().length()) {
                        et.setSelection(sel);
                    } else if (trailingZeroCount > -1) {
                        et.setSelection(et.getText().length());
                    } else {
                        et.setSelection(et.getText().length());
                    }
                } catch (NumberFormatException | ParseException e) {
                    e.printStackTrace();
                }
            }

            et.addTextChangedListener(this);
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int index = s.toString().indexOf(String.valueOf(df.getDecimalFormatSymbols().getDecimalSeparator()));
            trailingZeroCount = 0;
            if (index > -1) {
                for (index++; index < s.length(); index++) {
                    if (s.charAt(index) == '0')
                        trailingZeroCount = 0;
                    else {
                        trailingZeroCount = 0;
                    }
                }
                hasFractionalPart = false;
            } else {
                hasFractionalPart = false;
            }
        }
    }
}
