package com.sakoo;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.Objects;

public class DetailBarangInfoActivity extends AppCompatActivity {

    private ImageView detailFotoBarang;
    private TextView detailNamaBarang, detailInformasiBarang, detailInformasiTambahan, detailKategoriBarang;
    private Button editBarang, salinBarang;

    private String merk,volume,bahan;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_info_barang);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        detailNamaBarang = findViewById(R.id.detailNamaBarang);
        detailInformasiBarang = findViewById(R.id.detailInformasiBarang);
        detailInformasiTambahan = findViewById(R.id.detailInformasiTambahan);
        detailKategoriBarang = findViewById(R.id.detailkategoriBarang);
        detailFotoBarang = findViewById(R.id.detailFotoBarang);
        editBarang = findViewById(R.id.editButton);
        salinBarang = findViewById(R.id.salinButton);


        Picasso.with(this).load(getIntent().getExtras().getString("fotoBarang")+"?size=large").placeholder( R.drawable.animation_loading ).into(detailFotoBarang);

        String namaBarangNya = getIntent().getExtras().getString("namaBarang");
        String namaBarangSplit[] = namaBarangNya.split("sk-");
        detailNamaBarang.setText(namaBarangSplit[0].trim());

        detailKategoriBarang.setText(getIntent().getExtras().getString("subkategori"));

        detailInformasiBarang.setText(getIntent().getExtras().getString("deskripsi").trim());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(Objects.equals(getIntent().getExtras().getString("merkBarang"), "")){
                merk = "Merk : -";
            }else{
                merk = "Merk : "+getIntent().getExtras().getString("merkBarang");
            }
            if(Objects.equals(getIntent().getExtras().getString("volumeBarang"), "0x0x0")){
                volume = "Volume : -";
            }else{
                volume = "Volume : "+getIntent().getExtras().getString("volumeBarang")+" cm";
            }

            if(Objects.equals(getIntent().getExtras().getString("bahanBarang"), "")){
                bahan = "Bahan : -";
            }else{
                bahan = "Bahan : "+getIntent().getExtras().getString("bahanBarang");
            }
        }
        String berat = "Berat : "+String.valueOf(getIntent().getExtras().getInt("beratBarang"))+ " gr";

        DecimalFormat kursIndonesia = new DecimalFormat("#,###");
        String harga = "Harga : Rp. "+String.valueOf(kursIndonesia.format(getIntent().getExtras().getInt("hargaBarang"))).replace(",",".");
        String stok = "Stok : "+String.valueOf(getIntent().getExtras().getInt("stokBarang"));

        detailInformasiTambahan.setText(merk+"\n"+volume+"\n"+berat+"\n"+harga+"\n"+stok+"\n"+bahan);

        editBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(DetailBarangInfoActivity.this, BarangActivity.class);
                intent3.putExtra("idBarang",getIntent().getExtras().getString("idBarang"));
                intent3.putExtra("namaBarang", getIntent().getExtras().getString("namaBarang"));
                intent3.putExtra("merkBarang", getIntent().getExtras().getString("merkBarang"));
                intent3.putExtra("idSubkategori", getIntent().getExtras().getInt("idSubkategori"));
                intent3.putExtra("subkategori", getIntent().getExtras().getString("subkategori"));
                intent3.putExtra("deskripsi", getIntent().getExtras().getString("deskripsi"));
                intent3.putExtra("beratBarang", getIntent().getExtras().getInt("beratBarang"));
                intent3.putExtra("stokBarang", getIntent().getExtras().getInt("stokBarang"));
                intent3.putExtra("hargaBarang", getIntent().getExtras().getInt("hargaBarang"));
                intent3.putExtra("bahanBarang", getIntent().getExtras().getString("bahanBarang"));
                intent3.putExtra("volumeBarang", getIntent().getExtras().getString("volumeBarang"));
                intent3.putExtra("fotoBarang", getIntent().getExtras().getString("fotoBarang"));
                intent3.putExtra("fotoTambahan", getIntent().getExtras().getString("fotoTambahan"));
                startActivity(intent3);
            }
        });

        salinBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent3 = new Intent(DetailBarangInfoActivity.this, BarangActivity.class);
                intent3.putExtra("namaBarang", getIntent().getExtras().getString("namaBarang"));
                intent3.putExtra("merkBarang", getIntent().getExtras().getString("merkBarang"));
                intent3.putExtra("idSubkategori", getIntent().getExtras().getInt("idSubkategori"));
                intent3.putExtra("subkategori", getIntent().getExtras().getString("subkategori"));
                intent3.putExtra("deskripsi", getIntent().getExtras().getString("deskripsi"));
                intent3.putExtra("beratBarang", getIntent().getExtras().getInt("beratBarang"));
                intent3.putExtra("stokBarang", getIntent().getExtras().getInt("stokBarang"));
                intent3.putExtra("hargaBarang", getIntent().getExtras().getInt("hargaBarang"));
                intent3.putExtra("bahanBarang", getIntent().getExtras().getString("bahanBarang"));
                intent3.putExtra("volumeBarang", getIntent().getExtras().getString("volumeBarang"));
                intent3.putExtra("fotoBarang", getIntent().getExtras().getString("fotoBarang"));
                intent3.putExtra("fotoTambahan", getIntent().getExtras().getString("fotoTambahan"));
                startActivity(intent3);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
