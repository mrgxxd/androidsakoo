package com.sakoo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class DetailSaldoActivity extends AppCompatActivity {

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private int idTransaksiSeller, count;

    private LinearLayout linearLayout1;
    private List<TransactionModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TransactionAdapter mAdapter;

    private int totalHargaBarang = 0, ongkosKirim, totalBayar;
    private String alamatnya = "", foto = "", kontaknya = "", pengirimannya ="", asuransinya ="", keterangannya = "", keteranganBarang;

    private CardView cardAlamat, cardBiaya;
    private TextView totalHar, totalBay, ongkosKir, alamat, jumlahSaldo, tanggalRequest, tanggalTransfer, kontak, pengiriman, asuransi, keterangan;
    private Button buktiTransfer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_saldo);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sessionManager = new SessionManager(this);
        baseUtility = new BaseUtility(this);

        cardAlamat = (CardView)findViewById(R.id.cardAlamat);
        cardBiaya = (CardView)findViewById(R.id.cardBiaya);
        totalHar = (TextView) findViewById(R.id.totalHarga);
        ongkosKir = (TextView) findViewById(R.id.ongkosKirim);
        totalBay = (TextView) findViewById(R.id.totalBayar);
        alamat = (TextView) findViewById(R.id.alamat);
        kontak = (TextView) findViewById(R.id.kontak);
        pengiriman = (TextView) findViewById(R.id.pengiriman);
        asuransi = (TextView) findViewById(R.id.asuransi);
        keterangan = (TextView) findViewById(R.id.keterangan);

        jumlahSaldo = (TextView) findViewById(R.id.textSaldo);
        tanggalRequest = (TextView) findViewById(R.id.textRequest);
        tanggalTransfer = (TextView) findViewById(R.id.textTransfer);

        buktiTransfer = (Button) findViewById(R.id.lihatTransfer);

        // RecycleView
        recyclerView = (RecyclerView)findViewById(R.id.listTransaction);
        listingModelList = new ArrayList<>();
        mAdapter = new TransactionAdapter(DetailSaldoActivity.this,listingModelList);
        mLayoutManager = new LinearLayoutManager(DetailSaldoActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        idTransaksiSeller = getIntent().getExtras().getInt("idTransaksiSeller");

        getDetailSaldo();

        buktiTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lihatGambar(foto);
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            sessionManager.hapusAlSementara();
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    private void getDetailSaldo(){
        progressDialog = ProgressDialog.show(DetailSaldoActivity.this, "Detail Saldo", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/transaction/getDetailSaldo/"+idTransaksiSeller,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");
                            String jumlahUang = "", tanggalReq = "", tanggalTrans = "";

                            if(code == 200){
                                count = 0;
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int i =0; i < jsonArray.length(); i++){
                                    JSONObject jsonData = jsonArray.getJSONObject(i);
                                    int idTransaksi = jsonData.getInt("id_transaksi");
                                    String namaBarang = jsonData.getString("nama");
                                    int statusTransaksi = jsonData.getInt("status");
                                    String tanggal;
                                    if(statusTransaksi == 0){
                                        tanggal = jsonData.getString("tanggal_pemesanan");
                                    }else{
                                        tanggal = jsonData.getString("tanggal_action");
                                    }
                                    String fotoBarang = jsonData.getString("foto");

                                    String noOrder = jsonData.getString("oreder_no");
                                    String noResi = jsonData.getString("nomor_resi");

                                    foto = jsonData.getString("bukti_transfer");

                                    jumlahUang = jsonData.getString("jumlah_uang");
                                    tanggalReq = jsonData.getString("waktu_request");
                                    tanggalTrans = jsonData.getString("waktu_transfer");


                                    count = count + 1;

                                    if(Objects.equals(jsonData.getString("keterangan"), "")){
                                        keterangannya = "-";
                                    }else{
                                        keterangannya = jsonData.getString("keterangan");
                                    }

                                    if(Objects.equals(jsonData.getString("keterangan_barang"), "")){
                                        keteranganBarang = "-";
                                    }else{
                                        keteranganBarang = jsonData.getString("keterangan_barang");
                                    }

                                    if(jsonData.getInt("asuransi") == 0){
                                        asuransinya = "Tidak";
                                    }else{
                                        asuransinya = "Ya";
                                    }

                                    pengirimannya = jsonData.getString("jasa_pengiriman");
                                    kontaknya = jsonData.getString("no_hp_buyer");

                                    ongkosKirim = jsonData.getInt("biaya_kirim");
                                    alamatnya = jsonData.getString("alamat").replaceAll("( +)"," ").trim();
                                    alamatnya = alamatnya.replaceAll("&nbsp;","").trim();
                                    int barangXharga = jsonData.getInt("harga")*jsonData.getInt("jumlah");
                                    totalHargaBarang = totalHargaBarang + barangXharga;

                                    TransactionModel notificationModel1 = new TransactionModel(idTransaksi, namaBarang, statusTransaksi, tanggal, fotoBarang, noOrder, noResi, String.valueOf(count), keteranganBarang);
                                    listingModelList.add(notificationModel1);
                                }
                                cardBiaya.setVisibility(View.VISIBLE);
                                cardAlamat.setVisibility(View.VISIBLE);
                                totalBayar = ongkosKirim + totalHargaBarang;

                                DecimalFormat kursIndonesia = new DecimalFormat("#,###");

                                totalHar.setText("Total harga  : Rp. "+String.valueOf(kursIndonesia.format(totalHargaBarang)).replace(",","."));
                                ongkosKir.setText("Ongkos kirim : Rp. "+String.valueOf(kursIndonesia.format(ongkosKirim)).replace(",","."));
                                totalBay.setText("Total bayar : Rp. "+String.valueOf(kursIndonesia.format(totalBayar)).replace(",","."));

                                jumlahSaldo.setText("Rp. "+String.valueOf(kursIndonesia.format(Long.valueOf(jumlahUang))).replace(",","."));
                                tanggalRequest.setText(tanggalReq);
                                tanggalTransfer.setText(tanggalTrans);

                                foto = foto.replaceAll(" ", "%20");

                                alamat.setText(alamatnya);
                                keterangan.setText(keterangannya);
                                asuransi.setText(asuransinya);
                                kontak.setText(kontaknya);
                                pengiriman.setText(pengirimannya);

                                recyclerView.setAdapter(mAdapter);


                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(DetailSaldoActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(DetailSaldoActivity.this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void lihatGambar(String sPhoto){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(DetailSaldoActivity.this);

        LayoutInflater inflater = LayoutInflater.from(DetailSaldoActivity.this);
        final View dialogView = inflater.inflate(R.layout.dialog_box_show_transfer, null);

        TextView closeButton = (TextView) dialogView.findViewById(R.id.closeButton);

        ImageView fotoBuktiTransfer = (ImageView)dialogView.findViewById(R.id.fotoBuktiTransfer);
        Picasso.with(DetailSaldoActivity.this).load("http://www.sakoo.id/admin/assets/img/bukti_transfer/"+sPhoto).placeholder( R.drawable.animation_loading ).into(fotoBuktiTransfer);

        alertDialog.setView(dialogView);

        final AlertDialog test = alertDialog.create();

        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                test.cancel();
            }
        });

//        alertDialog.setNegativeButton("Tutup",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });

//        alertDialog.setPositiveButton("Download",
//                new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });

        test.show();
    }
}
