package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Created by bagussetiadi on 9/6/17.
 */

public class DetailTransaction extends AppCompatActivity {

    private CardView cardProses, cardInputResi, cardTitle, cardImage, cardBiaya, cardAlamat, cardInfo;
    private ImageView fotoBarang;
    private Button tolakButton, prosesButton, kirimResi;
    private EditText inputResi;
    private TextView no_order, nama_barang, jumlah_barang, tanggal_pesanan;
    private TextView alamat;
    private TextView totalHarga, ongkosKirim, totalBayar;
    private TextView info_transaksi;

    private String noResi;
    private int idTransaksi;

    private SessionManager sessionManager;
    private BaseUtility baseUtility;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_transaction);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        idTransaksi = getIntent().getExtras().getInt("idTransaksi");
        Log.e("transaksiId", String.valueOf(idTransaksi));

        sessionManager = new SessionManager(this);
        baseUtility = new BaseUtility(this);

        cardProses = (CardView) findViewById(R.id.cardProses);
        cardInputResi = (CardView) findViewById(R.id.cardInputResi);
        cardTitle = (CardView) findViewById(R.id.cardTitle);
        cardImage = (CardView) findViewById(R.id.cardImage);
        cardBiaya = (CardView) findViewById(R.id.cardBiaya);
        cardAlamat = (CardView) findViewById(R.id.cardAlamat);
        cardInfo = (CardView) findViewById(R.id.cardInfo);

        fotoBarang = (ImageView) findViewById(R.id.fotoBarang);

        tolakButton = (Button) findViewById(R.id.tolakButton);
        prosesButton = (Button) findViewById(R.id.prosesButton);
        kirimResi = (Button) findViewById(R.id.kirimResi);

        inputResi = (EditText) findViewById(R.id.inputResi);

        no_order = (TextView) findViewById(R.id.no_order);
        nama_barang = (TextView) findViewById(R.id.nama_barang);
        jumlah_barang = (TextView) findViewById(R.id.jumlah_barang);
        tanggal_pesanan = (TextView) findViewById(R.id.tanggal_pesanan);
        alamat = (TextView) findViewById(R.id.alamat);
        totalHarga = (TextView) findViewById(R.id.totalHarga);
        ongkosKirim = (TextView) findViewById(R.id.ongkosKirim);
        totalBayar = (TextView) findViewById(R.id.totalBayar);
        info_transaksi = (TextView) findViewById(R.id.info_transaksi);

        getTransaction();

        tolakButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tolakFunction();
            }
        });

        prosesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prosesFunction();
            }
        });

        kirimResi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(inputResi.getText().length() == 0){
                    baseUtility.myToast("Nomor resi tidak boleh kosong");
                }else{
                    kirimResiFunction();
                }
            }
        });
    }

    private void tolakFunction(){
        progressDialog = ProgressDialog.show(this, "Tolak transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/tolakTransaksi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(DetailTransaction.this, DetailTransaction.class);
                                intent.putExtra("idTransaksi", idTransaksi);
                                startActivity(intent);
                                finish();
                                Toast.makeText(DetailTransaction.this, "Transaksi sudah ditolak", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(DetailTransaction.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void prosesFunction(){
        progressDialog = ProgressDialog.show(this, "Proses transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/prosesTransaksi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(DetailTransaction.this, DetailTransaction.class);
                                intent.putExtra("idTransaksi", idTransaksi);
                                startActivity(intent);
                                finish();
                                Toast.makeText(DetailTransaction.this, "Transaksi berhasil diproses", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(DetailTransaction.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void kirimResiFunction(){
        progressDialog = ProgressDialog.show(this, "Proses transaksi", "Please wait ...", true );

        noResi = inputResi.getText().toString();
        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/updateResi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(DetailTransaction.this, DetailTransaction.class);
                                intent.putExtra("idTransaksi", idTransaksi);
                                startActivity(intent);
                                finish();
                                Toast.makeText(DetailTransaction.this, "No Resi berhasil diinput", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(DetailTransaction.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("nomor_resi", noResi);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void getTransaction(){
        progressDialog = ProgressDialog.show(this, "DetaiTransaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/transaction/getDetailTransaction",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONObject jsonData = new JSONObject(data);

                                cardTitle.setVisibility(View.VISIBLE);
                                cardImage.setVisibility(View.VISIBLE);
                                cardBiaya.setVisibility(View.VISIBLE);
                                //cardAlamat.setVisibility(View.VISIBLE);
                                cardInfo.setVisibility(View.VISIBLE);

                                no_order.setText("Order no : "+jsonData.getString("oreder_no"));
                                nama_barang.setText("Nama barang : "+jsonData.getString("nama"));
                                jumlah_barang.setText("Jumlah : "+jsonData.getString("jumlah"));
                                int jumlahBarang = jsonData.getInt("jumlah");

                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                String theDate = jsonData.getString("tanggal_pemesanan");
                                Date date = null;
                                try {
                                    date = dateFormat.parse(theDate);
                                    dateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
                                    tanggal_pesanan.setText(dateFormat.format(date));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String temp = jsonData.getString("foto");
                                temp = temp.replaceAll(" ", "%20");
                                Picasso.with(getApplicationContext()).load(temp+"?size=medium").into(fotoBarang);

                                if(jsonData.getInt("status") == 0){
                                    cardProses.setVisibility(View.VISIBLE);
                                    cardInputResi.setVisibility(View.GONE);
                                    info_transaksi.setText("Mohon segera diproses");
                                }else if(jsonData.getInt("status") == 1){
                                    cardProses.setVisibility(View.GONE);
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                        if(Objects.equals(jsonData.getString("nomor_resi"), "")){
                                            info_transaksi.setText("Mohon segera input resi");
                                            cardInputResi.setVisibility(View.VISIBLE);
                                        }else{
                                            info_transaksi.setText("Transaksi selesai, barang sudah dikirim dengan nomor resi : "+jsonData.getString("nomor_resi"));
                                            cardInputResi.setVisibility(View.GONE);
                                        }
                                    }
                                    //info_transaksi.setText("Mohon segera input resi");
                                }else if(jsonData.getInt("status") == 2){
                                    cardProses.setVisibility(View.GONE);
                                    cardInputResi.setVisibility(View.GONE);
                                    info_transaksi.setText("Transaksi ini sudah ditolak");
                                }else if(jsonData.getInt("status") == 3){
                                    cardProses.setVisibility(View.GONE);
                                    cardInputResi.setVisibility(View.GONE);
                                    info_transaksi.setText("Transaksi ini telah selesai");
                                }

                                Log.e("trim", jsonData.getString("alamat").replaceAll("( +)"," ").trim());
                                alamat.setText(jsonData.getString("alamat").replaceAll("( +)"," ").trim());
                                int hargaBarang = jsonData.getInt("harga");
                                int ongkos = jsonData.getInt("biaya_kirim");

                                int hargaTotal = hargaBarang * jumlahBarang;
                                int bayarTotal = hargaTotal + ongkos;

                                totalHarga.setText("Harga produk : Rp. "+hargaBarang);
                                ongkosKirim.setText("Jumlah pembelian : "+jumlahBarang);
                                totalBayar.setText("Total harga : Rp. "+hargaTotal);

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(DetailTransaction.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("idUser", sessionManager.getIdUser());
                map.put("idTransaksi", String.valueOf(idTransaksi));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
