package com.sakoo;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpClientStack;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener{

    File f;
    final int PIC_CROP = 3;
    final int PIC_CROP_FAILED = 4;

    ArrayAdapter<String> adapter, lokasiAdapter;
    List<String> list, listId, listLokasi, listIdLokasi;
    List<Integer> listMarkup;

    private Button updateProfile;
    private CircleImageView profilePicture;
    private ImageView contohGambar;

    private BaseUtility baseUtility;
    private SessionManager sessionManager;
    private HashMap<String, String> user;

    private ProgressDialog progressDialog;

    private EditText inputNama, inputEmail, inputNamaToko, inputAlamatToko, inputKota, inputPhoneNumber, inputRekening, inputNamaRekening;
    private Spinner inputBank, inputLokasi;

    private String id, username="", email="", nama="", alamat="", no_hp="", picture = "", urlFoto = "", bank, lokasi, rekening="", namaRekening="";

    ImageView pilihDetail;

    private RelativeLayout detailLokasi;

    private TextView textLokasi;

    int PICK_IMAGE_REQUEST = 1;
    private Bitmap bitmap, thumbnail;

    ProfileService profileService;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //DEFINITION AND EVENT COMPONENT
        definitionComponent();
        eventComponent();

        list = new ArrayList<String>();
        listId = new ArrayList<String>();
        list.add("Pilih Bank");
        listId.add("0");

        listLokasi = new ArrayList<String>();
        listIdLokasi = new ArrayList<String>();
        listMarkup = new ArrayList<Integer>();
        listLokasi.add("Pilih Lokasi");
        listIdLokasi.add("0");
        listMarkup.add(0);

        //Class
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);

        user = sessionManager.getUserDetails();
        getBank();
        //addInfo();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor).build();

        // Change base URL to your upload server URL.
        profileService = new Retrofit.Builder().baseUrl("http://103.195.31.220:1147/").client(client).build().create(ProfileService.class);
    }

    private void definitionComponent(){
        updateProfile = (Button) findViewById(R.id.updateProfile);
        profilePicture = (CircleImageView) findViewById(R.id.profile_image);
        contohGambar = (ImageView) findViewById(R.id.gambarContoh);

        inputNama = (EditText)findViewById(R.id.inputNama);
        inputEmail = (EditText)findViewById(R.id.inputEmail);
        inputNamaToko = (EditText)findViewById(R.id.inputNamaToko);
        //inputAlamatToko = (EditText)findViewById(R.id.inputAlamatToko);
        //inputKota = (EditText)findViewById(R.id.kota);
        inputPhoneNumber = (EditText)findViewById(R.id.inputPhoneNumber);
        textLokasi = (TextView) findViewById(R.id.detail_lokasi);
        inputRekening = (EditText) findViewById(R.id.inputRekening);
        inputNamaRekening = (EditText) findViewById(R.id.inputNamaRekening);
        detailLokasi = (RelativeLayout) findViewById(R.id.cardlokasi);
        pilihDetail = (ImageView) findViewById(R.id.pilihDetail);

        inputBank = (Spinner) findViewById(R.id.pilihBank);
        inputLokasi = (Spinner) findViewById(R.id.pilihLokasi);
    }

    private void eventComponent(){
        updateProfile.setOnClickListener(this);
        profilePicture.setOnClickListener(this);
        detailLokasi.setOnClickListener(this);
    }

    private void addInfo(){
        id = user.get("id");
        if(!user.get("username").isEmpty()){
            inputNama.setText(user.get("username"));
            //username = user.get("username");
        }

        if(!sessionManager.getUsernameSementara().isEmpty()){
            inputNama.setText(sessionManager.getUsernameSementara());
        }

        if(!user.get("email").isEmpty()){
            inputEmail.setText(user.get("email"));
            //email = user.get("email");
        }

        if(!sessionManager.getEmailSementara().isEmpty()){
            inputEmail.setText(sessionManager.getEmailSementara());
        }

        if(!user.get("nama").isEmpty()){
            inputNamaToko.setText(user.get("nama"));
            //nama = user.get("nama");
        }

        if(!sessionManager.getNamaSementara().isEmpty()){
            inputNamaToko.setText(sessionManager.getNamaSementara());
        }

        if(!user.get("alamat").isEmpty()){
            alamat = user.get("alamat");
            textLokasi.setText(user.get("alamat"));
        }

        if(!sessionManager.getAlamatSementara().isEmpty()){
            alamat = sessionManager.getAlamatSementara();
            textLokasi.setText(sessionManager.getAlamatSementara());
        }

        if(!user.get("no_hp").isEmpty()){
            inputPhoneNumber.setText(user.get("no_hp"));
            //no_hp = user.get("no_hp");
        }

        if(!sessionManager.getKontakSementara().isEmpty()){
            inputPhoneNumber.setText(sessionManager.getKontakSementara());
        }

        if(!user.get("foto").isEmpty()){
            String temp = user.get("foto");
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(this).load(temp).into(profilePicture);
            picture = user.get("foto");
            urlFoto = user.get("foto");
        }

        if(!sessionManager.getGambarSementara().isEmpty()){
            File f = new File(sessionManager.getGambarSementara());
            int targetW = contohGambar.getWidth();
            int targetH = contohGambar.getHeight();
            Bitmap bBitmap = decodeFile(f,targetW,targetH);
            profilePicture.setImageBitmap(bBitmap);
            picture = sessionManager.getGambarSementara();
            urlFoto = sessionManager.getUrlSementara();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(!Objects.equals(user.get("rekening"), "null")){
                inputRekening.setText(user.get("rekening"));
                //rekening = user.get("rekening");
            }
            if(!Objects.equals(user.get("namaRekening"), "null")){
                inputNamaRekening.setText(user.get("namaRekening"));
                //rekening = user.get("rekening");
            }
        }

        if(!sessionManager.getRekSementara().isEmpty()){
            inputRekening.setText(sessionManager.getRekSementara());
        }

        if(!sessionManager.getNamaRekSementara().isEmpty()){
            inputNamaRekening.setText(sessionManager.getNamaRekSementara());
        }

        int a = listId.indexOf(user.get("bank"));
        Log.e("lihat", listId.toString());
        Log.e("tes", String.valueOf(a));
        inputBank.setSelection(a);

        int c = listIdLokasi.indexOf(user.get("lokasiMarkup"));
        Log.e("lihat", listIdLokasi.toString());
        Log.e("tes", String.valueOf(a));
        inputLokasi.setSelection(c);

        if(!sessionManager.getBankSementara().isEmpty()){
            int b = listId.indexOf(sessionManager.getBankSementara());
            Log.e("lihat", listId.toString());
            Log.e("tes", String.valueOf(b));
            inputBank.setSelection(b);
        }

        if(!sessionManager.getLokasiSementara().isEmpty()){
            int d = listIdLokasi.indexOf(sessionManager.getLokasiSementara());
            Log.e("lihat", listIdLokasi.toString());
            Log.e("tes", String.valueOf(d));
            inputLokasi.setSelection(d);
        }
        //bank = user.get("bank");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            sessionManager.hapusAlSementara();
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.updateProfile:
                updateProfile();
                //baseUtility.myIntent(MainActivity.class);
                break;
            case R.id.profile_image:
                selectImage();
                break;
            case R.id.cardlokasi:
                baseUtility.myIntent(MapsActivity.class);

                username = inputNama.getText().toString();
                sessionManager.usernameSementara(username);

                email = inputEmail.getText().toString();
                sessionManager.emailSementara(email);

                nama = inputNamaToko.getText().toString();
                sessionManager.namaSementara(nama);

                no_hp = inputPhoneNumber.getText().toString();
                sessionManager.kontakSementara(no_hp);

                rekening = inputRekening.getText().toString();
                sessionManager.rekSementara(rekening);

                bank = listId.get(inputBank.getSelectedItemPosition());
                sessionManager.bankSementara(bank);

                namaRekening = inputNamaRekening.getText().toString();
                sessionManager.namaRekSementara(namaRekening);

                lokasi = listIdLokasi.get(inputLokasi.getSelectedItemPosition());
                sessionManager.lokasiSementara(lokasi);

                sessionManager.alamatSementara(alamat);


                //selectImage();
                break;
            default:
                break;
        }
    }

    private void selectImage() {

        final CharSequence[] options = {"Take Photo", "Choose from Gallery","Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo"))
                {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    Intent intent = new   Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                File f = new File(Environment.getExternalStorageDirectory().toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    int targetW = contohGambar.getWidth();
                    int targetH = contohGambar.getHeight();
                    //BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

//                    bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
//                            bitmapOptions);

                    bitmap = decodeFile(f, targetW,targetH);

                    //uploadImage(bitmap);

                    //profilePicture.setImageBitmap(bitmap);\\

                    String path = android.os.Environment
                            .getExternalStorageDirectory().getPath();
                    f.delete();
                    OutputStream outFile = null;
                    File file = new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg");

                    try {
                        outFile = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outFile);
                        saveImage(file, bitmap);
                        outFile.flush();
                        outFile.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == 2) {

                Uri selectedImage = data.getData();
                String[] filePath = { MediaStore.Images.Media.DATA };
                Log.e("tes",filePath.toString());
                Log.e("tes",selectedImage.toString());

                String picturePath = "";
                if(String.valueOf(selectedImage).contains("file:")){
                    picturePath = selectedImage.getPath();
                }else{
                    Cursor c = getContentResolver().query(selectedImage,filePath, null, null, null);

                    c.moveToFirst();
                    int columnIndex = c.getColumnIndex(filePath[0]);
                    picturePath = c.getString(columnIndex);
                    c.close();
                }

                int targetW = contohGambar.getWidth();
                int targetH = contohGambar.getHeight();

                File newFile = new File(picturePath);
                thumbnail = decodeFile(newFile,targetW,targetH);
                //profilePicture.setImageBitmap(thumbnail);
                saveImage(newFile, thumbnail);
            }
        }
    }

    private void updateProfile(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(inputNama.getText().length() == 0 || inputEmail.getText().length() == 0 || inputNamaToko.getText().length() == 0 || Objects.equals(alamat, "") || inputPhoneNumber.getText().length() == 0 || inputRekening.getText().length() == 0 || inputNamaRekening.getText().length() == 0 || inputBank.getSelectedItemPosition() == 0 || inputLokasi.getSelectedItemPosition() == 0){
                baseUtility.myToast("Profile harus dilengkapi");
            }else{
                //Check Email Format
                String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
                CharSequence inputStr = inputEmail.getText();
                Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
                Matcher matcher = pattern.matcher(inputStr);
                if (!matcher.matches()) {
                    baseUtility.myToast("Format email salah");
                }else{
                    username = inputNama.getText().toString();
                    email = inputEmail.getText().toString();
                    nama = inputNamaToko.getText().toString();
                    no_hp = inputPhoneNumber.getText().toString();
                    rekening = inputRekening.getText().toString();
                    namaRekening = inputNamaRekening.getText().toString();
                    bank = listId.get(inputBank.getSelectedItemPosition());
                    lokasi = listIdLokasi.get(inputLokasi.getSelectedItemPosition());
                    //alamat = inputAlamatToko.getText().toString();
                    // check input password and rePassword
                    progressDialog = ProgressDialog.show(this, "Simpan Profile", "Please wait ...", true );

                    //Log.e("lihat", apiConfig.userEndpoint);

                    StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/user/updateProfile/"+id,
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.e("TES", response.toString());
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        int code = jsonObject.getInt("code");
                                        String message = jsonObject.getString("message");

                                        if(code == 200){
                                            String data = jsonObject.getString("data");
                                            //if success create session user
                                            JSONObject jsonData = new JSONObject(data);
                                            sessionManager.createUserLoginSession(
                                                    jsonData.getString("id"),
                                                    jsonData.getString("nama"),
                                                    jsonData.getString("email"),
                                                    jsonData.getString("username"),
                                                    jsonData.getString("no_hp"),
                                                    jsonData.getString("alamat"),
                                                    jsonData.getString("foto"),
                                                    jsonData.getString("reg_id"),
                                                    jsonData.getString("token"),
                                                    jsonData.getString("rank"),
                                                    jsonData.getString("jumlah_transaksi"),
                                                    jsonData.getString("jumlah_listing"),
                                                    jsonData.getString("bank"),
                                                    jsonData.getString("rekening"),
                                                    jsonData.getString("nama_rekening"),
                                                    jsonData.getString("lokasi_markup")
                                            );
                                            sessionManager.hapusAlSementara();
    //                                //if success navigate to MainActivity
                                            progressDialog.dismiss();
                                            Toast.makeText(EditProfileActivity.this, "Profil berhasil disimpan", Toast.LENGTH_SHORT).show();
                                            Intent intent = new Intent(EditProfileActivity.this, NewHomeActivity.class);
                                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                            finish();
                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    Log.e("erornya", error.toString());
                                    baseUtility.myToast(error.toString());
                                }
                            }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String,String> map = new HashMap<String,String>();
                            map.put("username",username);
                            map.put("email",email);
                            map.put("nama",nama);
                            map.put("no_hp",no_hp);
                            map.put("alamat",alamat);
                            map.put("foto",urlFoto);
                            map.put("bank", bank);
                            map.put("rekening",rekening);
                            map.put("namarekening",namaRekening);
                            map.put("lokasi_markup", lokasi);
                            return map;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<String, String>();
                            //headers.put("Content-Type", "application/x-www-form-urlencoded");
                            headers.put("secretkey", "JackTheRipper");
                            return headers;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
                    requestQueue.add(stringRequest);
                }
            }
        }
    }

    private void saveImage(final File newFile, final Bitmap myBitmap){

        progressDialog = ProgressDialog.show(this, "Menyimpan Foto", "Please wait ...", true );

        RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), newFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("foto_profil", newFile.getName(), reqFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), "foto_profil");

//            Log.d("THIS", data.getData().getPath());

        retrofit2.Call<okhttp3.ResponseBody> req = profileService.postImage(body, name);
        req.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                try {
                    progressDialog.dismiss();
                    JSONObject jsonObject = new JSONObject(String.valueOf(response.body().string()));
                    baseUtility.myToast("Upload foto berhasil");
                    urlFoto = jsonObject.getString("data");
                    picture = newFile.getAbsolutePath();
                    profilePicture.setImageBitmap(myBitmap);
                    sessionManager.gambarSementara(picture);
                    sessionManager.urlSementara(urlFoto);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                progressDialog.dismiss();
                baseUtility.myToast("Upload foto gagal, mohon periksa koneksi dan permission perangkat anda");
                t.printStackTrace();
            }
        });
    }

    private void getBank(){
        progressDialog = ProgressDialog.show(this, "Mengambil Profile", "Please wait ...", true );
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/user/getBank/", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    String message = jsonObject.getString("message");
                    if(code == 200) {
                        String data = jsonObject.getString("data");
                        JSONArray jsonArray = new JSONArray(data);
                        for(int i = 0; i<jsonArray.length(); i++ ){
                            JSONObject jsonData = jsonArray.getJSONObject(i);
                            list.add(jsonData.getString("nama"));
                            listId.add(jsonData.getString("id"));
                        }
                        String dataMarkup = jsonObject.getString("dataMarkup");
                        Log.d("TAG", dataMarkup.toString());
                        JSONArray jsonArray1 = new JSONArray(dataMarkup);
                        for(int i = 0; i<jsonArray1.length(); i++ ){
                            JSONObject jsonData = jsonArray1.getJSONObject(i);
                            listLokasi.add(jsonData.getString("lokasi"));
                            listIdLokasi.add(jsonData.getString("id"));
                            listMarkup.add(jsonData.getInt("markup"));
                        }

                        adapter = new ArrayAdapter<String>(EditProfileActivity.this,
                                android.R.layout.simple_spinner_item, list);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        inputBank.setAdapter(adapter);

                        lokasiAdapter = new ArrayAdapter<String>(EditProfileActivity.this,
                                android.R.layout.simple_spinner_item, listLokasi);
                        lokasiAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        inputLokasi.setAdapter(lokasiAdapter);

                        addInfo();
                        progressDialog.dismiss();
                    }else{
                        baseUtility.myToast("Koneksi bermasalah, silahkan cek koneksi anda");
                        finish();
                        progressDialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GAGAL",error.toString());
                progressDialog.dismiss();
                baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                finish();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setPic(String mCurrentPhotoPath, ImageView mImageView) {
        // Get the dimensions of the View
        int targetW = mImageView.getWidth();
        int targetH = mImageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap theBitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        mImageView.setImageBitmap(theBitmap);
    }

    public static Bitmap decodeFile(File f,int WIDTH,int HIGHT){
        try {
            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //The new size we want to scale to
            final int REQUIRED_WIDTH=WIDTH;
            final int REQUIRED_HIGHT=HIGHT;
            //Find the correct scale value. It should be the power of 2.
            int scale=1;
            while(o.outWidth/scale/2>=REQUIRED_WIDTH && o.outHeight/scale/2>=REQUIRED_HIGHT)
                scale*=2;

            //Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    private void performCrop(Uri picUri) {
        try {
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            cropIntent.setDataAndType(picUri, "image/*");
            // set crop properties here
            cropIntent.putExtra("crop", true);
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 128);
            cropIntent.putExtra("outputY", 128);
            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            cropIntent.putExtra("uri", picUri);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, PIC_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();

        }
    }
}
