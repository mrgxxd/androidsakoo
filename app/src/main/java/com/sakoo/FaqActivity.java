package com.sakoo;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

public class FaqActivity extends AppCompatActivity {

    private LinearLayout linearLayout1;
    private List<FaqModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FaqAdapter mAdapter;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_faq);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        sessionManager = new SessionManager(this);

        recyclerView = (RecyclerView)findViewById(R.id.listFaq);
        listingModelList = new ArrayList<>();
        mAdapter = new FaqAdapter(this,listingModelList);
        mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        FaqModel notificationModel1 = new FaqModel("1", "1. Apakah produk yang saya jual melalui SaKoO harus yang bisa langsung dibeli (ready stock) atau bisa yang bersifat pemesanan (pre-order)?", "Semua barang yang dijual melalui Sakoo merupakan barang ready stock atau bisa langsung dibeli.");
        listingModelList.add(notificationModel1);

        FaqModel notificationModel2 = new FaqModel("1", "2. Bagaimana cara mengubah Profile (nama toko, foto akun, alamat email, alamat toko, nomor handphone, nomor rekening) saya di aplikasi Sakoo?", "Ikuti langkah-langkah berikut\n" +
                "- Login ke akun Sakoo. \n" +
                "- Klik tab Setting di bagian kanan bawah layar.\n" +
                "- Setelah Profile Anda ditampilkan, silakan klik tombol Edit. \n" +
                "- Jika Anda sudah membuat perubahan yang diinginkan, silakan klik tombol Update.");
        listingModelList.add(notificationModel2);

        FaqModel notificationModel3 = new FaqModel("1", "3. Bagaimana cara mencairkan uang hasil penjualan?", "Seller dapat melakukan pencairan uang melalui link Saldo di halaman Home. \n" +
                "Sakoo akan mentransfer uang hasil penjualan ke nomor rekening seller setelah seller memilih menu pencairan.\n" +
                " Hasil penjualan akan ditransfer dalam waktu 1 (satu) x 24 jam setelah seller mengonfirmasi nominal pencairan.");
        listingModelList.add(notificationModel3);

        FaqModel notificationModel4 = new FaqModel("1", "4. Apa yang mungkin menyebabkan uang hasil penjualan saya tidak bisa dicairkan ?", "- Pembeli mengajukan retur (pengembalian barang)\n" +
                "- Transaksi penjualanmu diduga sebagai transaksi fraud (penipuan)\n" +
                "- Pembeli belum melakukan konfirmasi terima barang.");
        listingModelList.add(notificationModel4);

        FaqModel notificationModel5 = new FaqModel("1", "5. Di mana produk saya dijual ?", "- www.bukalapak.com/satutokoonline\n" +
                "- www.tokopedia.com/satutokoonline\n" +
                "- sakoo.blanja.com");
        listingModelList.add(notificationModel5);

        FaqModel notificationModel6 = new FaqModel("1", "6. Apakah saya harus membayar biaya untuk menggunakan aplikasi Sakoo ?", "Tidak, sampai dengan saat ini aplikasi Sakoo gratis dan bebas komisi penjualan.");
        listingModelList.add(notificationModel6);

        FaqModel notificationModel7 = new FaqModel("1", "7. Bagaimana cara saya mendapatkan bantuan jika mengalami kendala teknis atau memerlukan berkomunikasi dengan Helpdesk ?", "Silakan klik tombol Setting di layar Home, kemudian klik tombol Bantuan untuk berkomunikasi dengan Helpdesk.");
        listingModelList.add(notificationModel7);

        FaqModel notificationModel8 = new FaqModel("1", "8. Apa saja agen logistik yang digunakan oleh Sakoo ?", "Saat ini, sistem Sakoo menggunakan jasa logistik dari JNE, dan terhubung secara langsung untuk membantu penghitungan ongkos kirim dari dan ke seluruh Indonesia. Semua dilakukan secara otomatis dan real time.");
        listingModelList.add(notificationModel8);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            sessionManager.hapusAlSementara();
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
