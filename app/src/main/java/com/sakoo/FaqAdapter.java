package com.sakoo;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class FaqAdapter extends RecyclerView.Adapter<FaqAdapter.MyViewHolder> {

    private List<FaqModel> listingModelList;
    private Context context;

    public FaqAdapter(Context context, List<FaqModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView pertanyaan;
        public ImageView arrow;
        public TextView jawaban;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            pertanyaan = (TextView) view.findViewById(R.id.pertanyaan);
            arrow = (ImageView) view.findViewById(R.id.arrorw);
            jawaban = (TextView) view.findViewById(R.id.jawabanFaq);
            cardView = (CardView) view.findViewById(R.id.card_view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_faq, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final FaqModel documentModel = listingModelList.get(position);
        holder.pertanyaan.setText(documentModel.getPertanyaan());

        holder.jawaban.setText(documentModel.getJawaban());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(holder.jawaban.getVisibility() == View.GONE){
                    holder.jawaban.setVisibility(View.VISIBLE);
                    holder.arrow.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
                }else{
                    holder.jawaban.setVisibility(View.GONE);
                    holder.arrow.setImageResource(R.drawable.ic_keyboard_arrow_right_black_24dp);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
