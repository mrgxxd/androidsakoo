package com.sakoo;

public class FaqModel {

    private String idFaq, pertanyaan, jawaban;

    public FaqModel(String s1, String s2, String s3){
        this.idFaq = s1;
        this.pertanyaan = s2;
        this.jawaban = s3;
    }

    public String getIdFaq() {
        return idFaq;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public String getJawaban() {
        return jawaban;
    }
}
