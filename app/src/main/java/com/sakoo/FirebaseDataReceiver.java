package com.sakoo;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.sakoo.databases.NotifHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;
import java.util.Random;

public class FirebaseDataReceiver extends BroadcastReceiver {

    private final String TAG = "FirebaseDataReceiver";

    private int typeId;
    private  String title, body, type;

    private MyFirebaseMessagingService myFirebase;

    public void onReceive(Context context, Intent intent) {
        NotifHelper notifHelper = new NotifHelper(context);

        if (intent.getExtras() != null) {
            for (String key : intent.getExtras().keySet()) {
                Object value = intent.getExtras().get(key);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(key, "key")){
                        typeId = Integer.parseInt(value.toString());
                    }
                    if(Objects.equals(key, "type")){
                        type = value.toString();
                    }

                    if(Objects.equals(key, "title")){
                        title = value.toString();
                    }

                    if(Objects.equals(key, "body")){
                        body = value.toString();
                    }
                }
                Log.e("FirebaseDataReceiver", "Key: " + key + " Value: " + value);
            }
            @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

            NotificationModel notif = new NotificationModel();
            notif.setMyTitle(title);
            notif.setMyText(body);
            notif.setTypeId(typeId);
            notif.setType(type);
            notif.setMyDate(sdf.format(new Date()));
            notif.setStatus(0);

            notifHelper.open();
            notifHelper.insertTransaction(notif);
            notifHelper.close();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(type, "transaksi")){
                    intent = new Intent(context, TransactionActivity.class);
                    intent.putExtra("idTransaksi", typeId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.iconv2)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.iconv2))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(getRequestCode(), notificationBuilder.build());
                }else if(Objects.equals(type, "barang")){
                    intent = new Intent(context, NewHomeActivity.class);
                    intent.putExtra("FRAGMENT_ID", 1);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.iconv2)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.iconv2))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(getRequestCode(), notificationBuilder.build());
                }else if(Objects.equals(type, "saldo")){
                    intent = new Intent(context, NewHomeActivity.class);
                    intent.putExtra("FRAGMENT_ID", 1);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.iconv2)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.iconv2))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(getRequestCode(), notificationBuilder.build());
                }else if(Objects.equals(type, "detail_transaksi")){
                    intent = new Intent(context, TransactionActivity.class);
                    intent.putExtra("idTransaksi", typeId);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    PendingIntent pendingIntent = PendingIntent.getActivity(context, 0 /* Request code */, intent,
                            PendingIntent.FLAG_ONE_SHOT);
                    Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                            .setSmallIcon(R.drawable.iconv2)
                            .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                                    R.drawable.iconv2))
                            .setContentTitle(title)
                            .setContentText(body)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setPriority(NotificationCompat.PRIORITY_HIGH)
                            .setContentIntent(pendingIntent);
                    NotificationManager notificationManager =
                            (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

                    notificationManager.notify(getRequestCode(), notificationBuilder.build());
                }
            }
        }
    }

    private static int getRequestCode() {
        Random rnd = new Random();
        return 100 + rnd.nextInt(900000);
    }
}
