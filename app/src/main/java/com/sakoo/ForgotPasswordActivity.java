package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener{

    private Button sendButton;
    private EditText inputEmail;
    private BaseUtility baseUtility;
    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    private String email;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //DEFINITION AND EVENT COMPONENT
        definitionComponent();
        eventComponent();

        //Class
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
    }

    private void definitionComponent(){
        sendButton = (Button) findViewById(R.id.sendEmail);
        inputEmail = (EditText) findViewById(R.id.inputEmail);
    }

    private void eventComponent(){
        sendButton.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendEmail :
                lupaPasswordFunction();
                break;
            default:
                break;
        }
    }

    private void lupaPasswordFunction(){
        // TODO :

        //check username and password value can't null
        if(inputEmail.getText().length() == 0){
            baseUtility.myToast("No handphone tidak boleh kosong");
        }else{
            email = inputEmail.getText().toString();
            //call API login and check response
            progressDialog = ProgressDialog.show(this, "Permintaan Ubah Password", "Please wait ...", true );

            //Log.e("lihat", apiConfig.userEndpoint);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/newLupapassword",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("TES", response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int code = jsonObject.getInt("code");

                                if(code == 200){
//                                //if success navigate to MainActivity
                                    progressDialog.dismiss();
                                    Toast.makeText(ForgotPasswordActivity.this, "Permintaan berhasil, silahkan cek sms yang masuk ke dalam handphone anda", Toast.LENGTH_LONG).show();
                                    sessionManager.logoutUser();
                                    finish();
//                                    baseUtility.myIntent(LoginActivity.class);
//                                    finish();
                                }else{
                                    progressDialog.dismiss();
                                    Toast.makeText(ForgotPasswordActivity.this, "Gagal merubah password, silahkan hubungi admin", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Log.e("erornya", error.toString());
                            baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<String,String>();
                    map.put("no_hp",email);
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("secretkey", "JackTheRipper");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
            requestQueue.add(stringRequest);
        }
    }
}
