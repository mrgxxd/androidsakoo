package com.sakoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.qiscus.sdk.Qiscus;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class HomeFragment extends Fragment {

    private List<HomeNotifModel> notificationModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private HomeNotifAdapter mAdapter;

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout, transaksiSeller, listingSeller;;
    private TextView[] dots;
    private int[] layouts;

    GridView androidGridView;

    private HashMap<String, String> user;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private TextView rankText, transText, listText, textGreeting;

    private ImageView notivicationIcon;

    private Button prosesSesuai, bagikanButton, uploadBarang;

    private CircleImageView cvProfilePic;

    private ProgressDialog progressDialog;
    private ProgressBar loadingBar;


    String[] gridViewString = {
            "Upload Barang", "Status Barang", "Transaksi", "Saldo"
    } ;

    int[] gridViewImageId = {
            R.drawable.logo_upload2, R.drawable.logo_update2, R.drawable.logo_transaksi2, R.drawable.logo_pencairan2
    };

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    private Timer timer;
    int page = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        baseUtility = new BaseUtility(getActivity());
        user = sessionManager.getUserDetails();
        Qiscus.clearUser();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_2, container, false);

        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();

        // RecycleView
        recyclerView = (RecyclerView)view.findViewById(R.id.listHome);
        notificationModelList = new ArrayList<>();
        mAdapter = new HomeNotifAdapter(getActivity(),notificationModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        textGreeting = (TextView) view.findViewById(R.id.textGreeting);
        loadingBar = (ProgressBar) view.findViewById(R.id.loadingBar);
        cvProfilePic = view.findViewById(R.id.profile_image);
        notivicationIcon = view.findViewById(R.id.notification_icon);

        if(sessionManager.isUserLoggedIn()){
            if(!user.get("foto").isEmpty()) {
                Picasso.with(getActivity().getApplicationContext())
                        .load(user.get("foto")+"?size=small")
                        .into(cvProfilePic);
            }
        }

        cvProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sessionManager.isUserLoggedIn()){
                    Intent editProfIntent = new Intent(getActivity(), EditProfileActivity.class);
                    startActivity(editProfIntent);
                }else{
                    baseUtility.myIntent(LoginActivity.class);
                }
            }
        });

        notivicationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent editProfIntent = new Intent(getActivity(), NotificationActivity.class);
                startActivity(editProfIntent);
            }
        });


        prosesSesuai = (Button)view.findViewById(R.id.prosesSesuai);
        bagikanButton = (Button)view.findViewById(R.id.bagikanAplikasi);

        uploadBarang = (Button)view.findViewById(R.id.uploadFloating);

        prosesSesuai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sessionManager.isUserLoggedIn()){
                    tombolProses();
                }else{
                   baseUtility.myIntent(LoginActivity.class);
                }
            }
        });

        bagikanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bagikanFunction();
            }
        });

        uploadBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sessionManager.isUserLoggedIn()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                            Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(getActivity(), EditProfileActivity.class);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }else{
                            Intent intent = new Intent(getActivity(), BarangActivity.class);
                            startActivity(intent);
                        }
                    }
                }else{
                    baseUtility.myIntent(LoginActivity.class);
                }
            }
        });

        if(sessionManager.isUserLoggedIn()){
            getNotifikasi();
        }else {
            loadingBar.setVisibility(View.INVISIBLE);
            textGreeting.setText("Hai ! Ayo mulai berjualan, upload barang pertamamu di Sakoo !");
            prosesSesuai.setText("Upload Barang");
            bagikanButton.setVisibility(View.INVISIBLE);
        }





//        rankText = (TextView) view.findViewById(R.id.rankSeller);
//        transText = (TextView) view.findViewById(R.id.transaksiSeller);
//        listText = (TextView) view.findViewById(R.id.listingSeller);
//
//        transaksiSeller = (LinearLayout) view.findViewById(R.id.cTransaksi);
//        listingSeller = (LinearLayout) view.findViewById(R.id.cListing);
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            if(Objects.equals(user.get("rank"), "0")){
//                rankText.setText("-");
//            }else{
//                rankText.setText(user.get("rank"));
//            }
//
//            if(Objects.equals(user.get("transaction"), "0")){
//                transText.setText("-");
//            }else{
//                transText.setText(user.get("transaction"));
//            }
//
//            if(Objects.equals(user.get("listing"), "0")){
//                listText.setText("-");
//            }else{
//                listText.setText(user.get("listing"));
//            }
//        }
//
//        transaksiSeller.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                baseUtility.myIntent(MyTransactionActivity.class);
//            }
//        });
//
//        listingSeller.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                baseUtility.myIntent(StockActivity.class);
//            }
//        });
//
//
//        viewPager = (ViewPager) view.findViewById(R.id.view_pager);
//        dotsLayout = (LinearLayout) view.findViewById(R.id.dots);
//
//        // layouts of all welcome sliders
//        // add few more layouts if you want
//        layouts = new int[]{
//                R.layout.news_ticker_1,
//                R.layout.news_ticker_2,
//                R.layout.news_ticker_3};
//
//        // adding bottom dots
//        addBottomDots(0);
//
//        // making notification bar transparent
//        changeStatusBarColor();
//
//        myViewPagerAdapter = new MyViewPagerAdapter();
//        viewPager.setAdapter(myViewPagerAdapter);
//        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
//        pageSwitcher(5);
//
//        CustomGridViewAnother adapterViewAndroid = new CustomGridViewAnother(getActivity(), gridViewString, gridViewImageId);
//        androidGridView=(GridView)view.findViewById(R.id.grid_view_image_text);
//        androidGridView.setAdapter(adapterViewAndroid);
//        androidGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//
//
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view,
//                                    int i, long id) {
//                //Toast.makeText(getActivity(), String.valueOf(i), Toast.LENGTH_LONG).show();
//                if(i == 0){
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
//                            Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                        }else{
//                            Intent intent = new Intent(getActivity(), BarangActivity.class);
//                            startActivity(intent);
//                        }
//                    }
//                }else if(i == 1){
//                    Intent intent = new Intent(getActivity(), StockActivity.class);
//                    startActivity(intent);
//                }else if(i == 2){
//                    Intent intent = new Intent(getActivity(), MyTransactionActivity.class);
//                    startActivity(intent);
//                }else if(i == 3){
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//                        if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
//                            Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
//                            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(intent);
//                        }else{
//                            Intent intent = new Intent(getActivity(), SaldoActivity.class);
//                            startActivity(intent);
//                        }
//                    }
//                }
//            }
//        });

        return view;
    }

    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getActivity());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }
    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {

            View view = (View) object;
            container.removeView(view);
        }
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

    // this is an inner class...
    private class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            getActivity().runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 2) { // In my case the number of pages are 5
                        timer.cancel();
                        // Showing a toast for just testing purpose
//                        Toast.makeText(getApplicationContext(), "Timer stoped",
//                                Toast.LENGTH_LONG).show();
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    @Override
    public void onDestroy() {
        //timer.cancel();
        super.onDestroy();
    }

    private void setTextGreeting(){
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(sessionManager.getKeyJumlahSaldo());
            loadingBar.setVisibility(View.INVISIBLE);
            if(jsonArray.length() != 0){
                int totalSaldo = 0;
                for(int i = 0; i <jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    totalSaldo = totalSaldo + jsonObject.getInt("jumlah_uang");
                }

                DecimalFormat kursIndonesia = new DecimalFormat("#,###");

                if(totalSaldo == 0){
                    textGreeting.setText("Saat ini saldonya kosong, upload barangmu untuk tingkatkan transaksi");
                }else{
                    textGreeting.setText("Wow, "+user.get("nama")+" punya saldo Rp. "+String.valueOf(kursIndonesia.format(totalSaldo)).replace(",",".")+", klik cairkan agar masuk ke rekeningmu");
                }
                prosesSesuai.setText("Cek Saldo");
            }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(sessionManager.getKeyJumlahTransaksi(), "0")){
                    textGreeting.setText("Hai "+user.get("nama")+ "! Wow ada transaksi, ayo kirim barangnya dan input resinya");
                    prosesSesuai.setText("Cek Transaksi");
                }else if(!Objects.equals(sessionManager.getKeyJumlahBarang(), "0")){
                    textGreeting.setText("Hai "+user.get("nama")+ "! Makin banyak barang diupload, makin besar terjadi transaksi. Ayo upload yang banyak !");
                    prosesSesuai.setText("Upload Barang");
                }else{
                    textGreeting.setText("Hai "+user.get("nama")+ "! Ayo mulai berjualan, upload barang pertamamu di Sakoo !");
                    prosesSesuai.setText("Upload Barang");
                    bagikanButton.setVisibility(View.INVISIBLE);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void bagikanFunction(){

        String pesanBagikan = "";

        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(sessionManager.getKeyJumlahSaldo());
            if(jsonArray.length() != 0){
                int totalSaldo = 0;
                for(int i = 0; i <jsonArray.length(); i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    totalSaldo = totalSaldo + jsonObject.getInt("jumlah_uang");
                }
                pesanBagikan = "Mau jualan online mudah?\n" +
                        "Yuk cobain pakai SaKoO (Satu Toko Online). \n" +
                        "Saya sudah mencobanya dan berhasil mendapatkan transaksi \n\n";
            }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(sessionManager.getKeyJumlahTransaksi(), "0")){
                    pesanBagikan = "Saya sudah memiliki transaksi via Sakoo. Ayo gunakan Sakoo untuk meningkatkan penjualan barang kamu ! \n\n";
                }else if(!Objects.equals(sessionManager.getKeyJumlahBarang(), "0")){
                    pesanBagikan = "Barang dagangan saya sudah terpajang di banyak MarketPlace berkat Sakoo. Ayo upload juga barang daganganmu melalui Sakoo ! \n\n";
                }else{
                    pesanBagikan = "Saya sudah bergabung dengan Sakoo. Ayo bergabung juga ! \n\n";
                }
            }

            pesanBagikan = pesanBagikan + "https://play.google.com/store/apps/details?id=" + getActivity().getPackageName();

            Intent sharingIntent = new Intent(Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Barang");
            sharingIntent.putExtra(Intent.EXTRA_TEXT, pesanBagikan);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void tombolProses(){
        JSONArray jsonArray = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else{
                try {
                    jsonArray = new JSONArray(sessionManager.getKeyJumlahSaldo());
                    if(jsonArray.length() != 0){
                        Intent intent = new Intent(getActivity(), NewHomeActivity.class);
                        intent.putExtra("FRAGMENT_ID", 3);
                        startActivity(intent);
                    }else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(!Objects.equals(sessionManager.getKeyJumlahTransaksi(), "0")){
                            Intent intent = new Intent(getActivity(), NewHomeActivity.class);
                            intent.putExtra("FRAGMENT_ID", 2);
                            startActivity(intent);
                        }else if(!Objects.equals(sessionManager.getKeyJumlahBarang(), "0")){
                            Intent intent = new Intent(getActivity(), NewHomeActivity.class);
                            intent.putExtra("FRAGMENT_ID", 1);
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(getActivity(), NewHomeActivity.class);
                            intent.putExtra("FRAGMENT_ID", 1);
                            startActivity(intent);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getNotifikasi(){
        //progressDialog = ProgressDialog.show(getActivity(), "Home", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/fcm/getHomeNotif/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                String dataTambahan = jsonObject.getString("dataTambahan");
                                //if success create session user
                                JSONArray jsonArray = new JSONArray(data);
                                    for(int z = 0; z <= jsonArray.length(); z++){
                                        if(z != jsonArray.length()){
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(z);
                                            HomeNotifModel notificationModel1 = new HomeNotifModel(
                                                    jsonObject1.getString("title"),
                                                    jsonObject1.getString("body"),
                                                    jsonObject1.getString("type_id"),
                                                    jsonObject1.getString("status_proses"),
                                                    jsonObject1.getString("type")
                                            );
                                            notificationModelList.add(notificationModel1);
                                        }else{
                                            HomeNotifModel notificationModel1 = new HomeNotifModel(
                                                    "empty",
                                                    "empty",
                                                    "empty",
                                                    "empty",
                                                    "empty"
                                            );
                                            notificationModelList.add(notificationModel1);
                                        }
                                    }
                                    recyclerView.setAdapter(mAdapter);
//                                    recyclerView.setVisibility(View.VISIBLE);
//                                    textInfo.setVisibility(View.INVISIBLE);
//                                    gambarInfo.setVisibility(View.INVISIBLE);
//                                //if success navigate to MainActivity
                                //progressDialog.dismiss();
                                JSONObject jsonObject1 = new JSONObject(dataTambahan);
                                String jumlahBarang = jsonObject1.getString("jumlahBarang");
                                String jumlahTransaksi = jsonObject1.getString("jumlahTransaksi");
                                String jumlahSaldo = jsonObject1.getString("jumlahSaldo");

                                sessionManager.createHomeStatus(jumlahBarang, jumlahTransaksi, jumlahSaldo);
                                setTextGreeting();
                            }else{
                                //progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
