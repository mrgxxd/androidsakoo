package com.sakoo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class HomeNotifAdapter extends RecyclerView.Adapter<HomeNotifAdapter.MyViewHolder> {

    private List<HomeNotifModel> notificationModelList;
    private Context context;

    public HomeNotifAdapter(Context context, List<HomeNotifModel> _notificationModelList) {
        this.notificationModelList = _notificationModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView myTitle, myText;
        public ImageView status;
        public View cardView;
        public RelativeLayout batas;

        public MyViewHolder(View view) {
            super(view);
            cardView = (View) view.findViewById(R.id.card_view);
            myTitle = (TextView) view.findViewById(R.id.titleNotif);
            myText = (TextView) view.findViewById(R.id.isiNotif);
            status = (ImageView) view.findViewById(R.id.statusNotif);
            batas = (RelativeLayout) view.findViewById(R.id.garisBatasKiri);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_home_notif, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final HomeNotifModel documentModel = notificationModelList.get(position);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(!Objects.equals(documentModel.getTitle(), "empty")){
                holder.myText.setText(documentModel.getBody());
                holder.myTitle.setText(documentModel.getTitle());

                if(Objects.equals(documentModel.getType(), "transaksi")){
                    if(Objects.equals(documentModel.getStatus(), "Perlu Proses")){
                        holder.batas.setBackgroundResource(R.color.orange);
//                    holder.status.setBackgroundResource(R.color.orange);
//                    holder.status.setText("Perlu Proses");
                        holder.status.setImageResource(R.drawable.perlu_proses);
                    }else if(Objects.equals(documentModel.getStatus(), "Input Resi")){
                        holder.batas.setBackgroundResource(R.color.orange);
//                    holder.status.setBackgroundResource(R.color.orange);
//                    holder.status.setText("Input Resi");
                        holder.status.setImageResource(R.drawable.input_resi);
                    }else if(Objects.equals(documentModel.getStatus(), "Pengiriman")){
                        holder.batas.setBackgroundColor(Color.BLUE);
//                    holder.status.setBackgroundColor(Color.BLUE);
//                    holder.status.setText("Pengiriman");
                        holder.status.setImageResource(R.drawable.pengiriman);
                    }else if(Objects.equals(documentModel.getStatus(), "Selesai")){
                        holder.batas.setBackgroundColor(Color.GREEN);
//                    holder.status.setBackgroundColor(Color.GREEN);
//                    holder.status.setText("Selesai");
                        holder.status.setImageResource(R.drawable.selesai);
                    }
                }else if(Objects.equals(documentModel.getType(), "barang")){
                    if(Objects.equals(documentModel.getStatus(), "Tolak")){
                        holder.batas.setBackgroundColor(Color.RED);
//                    holder.status.setBackgroundColor(Color.RED);
//                    holder.status.setText("Tolak");
                        holder.status.setImageResource(R.drawable.tolak);
                    }else if(Objects.equals(documentModel.getStatus(), "Proses")){
                        holder.batas.setBackgroundColor(Color.BLUE);
//                    holder.status.setBackgroundColor(Color.BLUE);
//                    holder.status.setText("Proses");
                        holder.status.setImageResource(R.drawable.proses);
                    }else if(Objects.equals(documentModel.getStatus(), "Sukses")){
                        holder.batas.setBackgroundColor(Color.GREEN);
//                    holder.status.setBackgroundColor(Color.GREEN);
//                    holder.status.setText("Sukses");
                        holder.status.setImageResource(R.drawable.selesai);
                    }else if(Objects.equals(documentModel.getStatus(), "Perlu Proses")){
                        holder.batas.setBackgroundResource(R.color.orange);
//                    holder.status.setBackgroundResource(R.color.orange);
//                    holder.status.setText("Perlu Proses");
                        holder.status.setImageResource(R.drawable.perlu_proses);
                    }
                }else if(Objects.equals(documentModel.getType(), "saldo")){
                    if(Objects.equals(documentModel.getStatus(), "Proses")){
                        holder.batas.setBackgroundColor(Color.BLUE);
//                    holder.status.setBackgroundColor(Color.BLUE);
//                    holder.status.setText("Proses");
                        holder.status.setImageResource(R.drawable.proses);
                    }else if(Objects.equals(documentModel.getStatus(), "Selesai")){
                        holder.batas.setBackgroundColor(Color.GREEN);
//                    holder.status.setBackgroundColor(Color.GREEN);
//                    holder.status.setText("Selesai");
                        holder.status.setImageResource(R.drawable.selesai);
                    }
                }else if(Objects.equals(documentModel.getType(), "detail_transaksi")){
                    if(Objects.equals(documentModel.getStatus(), "Perlu Proses")){
                        holder.batas.setBackgroundResource(R.color.orange);
//                    holder.status.setBackgroundResource(R.color.orange);
//                    holder.status.setText("Perlu Proses");
                        holder.status.setImageResource(R.drawable.perlu_proses);
                    }else if(Objects.equals(documentModel.getStatus(), "Input Resi")){
                        holder.batas.setBackgroundResource(R.color.orange);
//                    holder.status.setBackgroundResource(R.color.orange);
//                    holder.status.setText("Input Resi");
                        holder.status.setImageResource(R.drawable.input_resi);
                    }else if(Objects.equals(documentModel.getStatus(), "Pengiriman")){
                        holder.batas.setBackgroundColor(Color.BLUE);
//                    holder.status.setBackgroundColor(Color.BLUE);
//                    holder.status.setText("Pengiriman");
                        holder.status.setImageResource(R.drawable.pengiriman);
                    }else if(Objects.equals(documentModel.getStatus(), "Selesai")){
                        holder.batas.setBackgroundColor(Color.GREEN);
//                    holder.status.setBackgroundColor(Color.GREEN);
//                    holder.status.setText("Selesai");
                        holder.status.setImageResource(R.drawable.perlu_proses);
                    }
                }
            }else{
                holder.cardView.setVisibility(View.INVISIBLE);
            }
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(documentModel.getType(), "transaksi")){
                        Intent intent = new Intent(context, TransactionActivity.class);
                        intent.putExtra("idTransaksi", Integer.valueOf(documentModel.getId()));
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "barang")){
                        Intent intent = new Intent(context, NewHomeActivity.class);
                        intent.putExtra("FRAGMENT_ID", 1);
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "saldo")){
                        Intent intent = new Intent(context, NewHomeActivity.class);
                        intent.putExtra("FRAGMENT_ID", 3);
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "detail_transaksi")){
                        Intent intent = new Intent(context, TransactionActivity.class);
                        intent.putExtra("idTransaksi", Integer.valueOf(documentModel.getId()));
                        context.startActivity(intent);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }
}
