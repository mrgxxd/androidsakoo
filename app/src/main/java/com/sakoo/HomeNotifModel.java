package com.sakoo;

public class HomeNotifModel {

    private String title, body, id, status, type, warna;

    public HomeNotifModel(String _title, String _body, String _id, String _status, String _type){
        this.title = _title;
        this.body = _body;
        this.id = _id;
        this.status = _status;
        this.type = _type;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }

    public String getId() {
        return id;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

}
