package com.sakoo;

public class KategoriModel {

    private String id, nama;

    public KategoriModel (String s1, String s2){
        this.id = s1;
        this.nama = s2;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }
}
