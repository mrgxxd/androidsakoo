package com.sakoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.R.attr.fragment;
import static android.R.attr.password;

public class ListingAdapter extends RecyclerView.Adapter {

    private List<ListingModel> listingModelList;
    private Context context;

    private ProgressDialog progressDialog;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public ListingAdapter(Context context, List<ListingModel> _listingModelList, RecyclerView recyclerView) {
        this.listingModelList = _listingModelList;
        this.context = context;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView myFoto;
        public TextView myBarang, myStok, tokopedia, bukalapak, blanja, countNumber;
        public Button updateStok, detailTransaksi;

        public MyViewHolder(View view) {
            super(view);
            myFoto = (ImageView) view.findViewById(R.id.fotoBarang);
            myBarang = (TextView) view.findViewById(R.id.namaBarang);
            myStok = (TextView) view.findViewById(R.id.stokBarang);
            tokopedia = (TextView) view.findViewById(R.id.tokopedia);
            blanja = (TextView) view.findViewById(R.id.blanja);
            bukalapak = (TextView) view.findViewById(R.id.bukalapak);
            countNumber = (TextView) view.findViewById(R.id.countNumber);
            updateStok = (Button) view.findViewById(R.id.updateStok);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==VIEW_TYPE_ITEM){
            return new MyViewHolder(inflater.inflate(R.layout.card_listing,parent,false));
        }else{
            return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, int position) {
        if (holder instanceof MyViewHolder) {
            final ListingModel documentModel = listingModelList.get(position);
            ((MyViewHolder)holder).countNumber.setText(documentModel.getCount());
            String namaBarang = documentModel.getMyBarang();
            if(namaBarang.length() > 23){
                String newNama = namaBarang.substring(0,20);
                ((MyViewHolder)holder).myBarang.setText(newNama.trim()+" ...");
            }else{
                ((MyViewHolder)holder).myBarang.setText(namaBarang.trim());
            }
            ((MyViewHolder)holder).myStok.setText("Stok("+documentModel.getMyStok()+")");
            String temp = documentModel.getFoto();
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(context).load(temp).into( ((MyViewHolder)holder).myFoto);

            ((MyViewHolder)holder).updateStok.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateFunction(documentModel.getMyId(), documentModel.getMyStok());
                }
            });

            final String urlEccomerce = documentModel.getUrl();
            String namaEccomerce = documentModel.getEcommerce();

            try {
                JSONArray arrayNama = new JSONArray(namaEccomerce);
                final JSONArray arrayUrl = new JSONArray(urlEccomerce);
                for(int i = 0; i < arrayNama.length(); i++){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(Objects.equals(arrayNama.getString(i), "Tokopedia")) {
                            ((MyViewHolder)holder).tokopedia.setTextColor(Color.GREEN);
                            final String theUrl = arrayUrl.getString(i);
                            ((MyViewHolder)holder).tokopedia.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Uri uri = Uri.parse(theUrl); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    context.startActivity(intent);
                                }
                            });
                        }

                        if(Objects.equals(arrayNama.getString(i), "Bukalapak")) {
                            ((MyViewHolder)holder).bukalapak.setTextColor(Color.GREEN);
                            final String theUrl = arrayUrl.getString(i);
                            ((MyViewHolder)holder).bukalapak.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Uri uri = Uri.parse(theUrl); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    context.startActivity(intent);
                                }
                            });
                        }

                        if(Objects.equals(arrayNama.getString(i), "Blanja")) {
                            ((MyViewHolder)holder).blanja.setTextColor(Color.GREEN);
                            final String theUrl = arrayUrl.getString(i);
                            ((MyViewHolder)holder).blanja.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Uri uri = Uri.parse(theUrl); // missing 'http://' will cause crashed
                                    Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                                    context.startActivity(intent);
                                }
                            });
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return listingModelList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listingModelList == null ? 0 : listingModelList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void updateList(List<ListingModel> list){
        listingModelList = list;
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    private void updateFunction(final int id, final int stok){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Edit Stok");
        alertDialog.setMessage("Mohon dapat update stok secara berkala setiap 1 minggu sesuai stok offline");

        final EditText input = new EditText(context);
        input.setText(String.valueOf(stok));
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(context, "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            String textStok = input.getText().toString();
                            int newStok = Integer.parseInt(textStok);
                                updateStokToServer(id, newStok);
                                dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void updateStokToServer(int id, final int theStok){
        progressDialog = ProgressDialog.show(context, "Update Stok", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/barang/updateStok/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, StockActivity.class);
                                context.startActivity(intent);
                                Toast.makeText(context, "Update stok sukses", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("stok", String.valueOf(theStok));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }
}
