package com.sakoo;

import android.app.ProgressDialog;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListingFragment extends Fragment {

    private LinearLayout linearLayout1;
    private List<ListingModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ListingAdapter mAdapter;

    private ProgressDialog progressDialog;
    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private int indexListing = 0, count = 0;

    private TextView infojumlahlisting;
    private CardView cardInfoListing;
    private EditText cariByText;

    public ListingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseUtility = new BaseUtility(getActivity());
        sessionManager = new SessionManager(getActivity());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_listing, container, false);
        // RecycleView
        cariByText = (EditText) view.findViewById(R.id.cariByText);
        cardInfoListing = (CardView) view.findViewById(R.id.cardInfoListing);
        cardInfoListing = (CardView) view.findViewById(R.id.cardInfoListing);
        infojumlahlisting = (TextView) view.findViewById(R.id.infojumlahlisting);
        recyclerView = (RecyclerView)view.findViewById(R.id.listListing);
        listingModelList = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new ListingAdapter(getActivity(),listingModelList, recyclerView);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                listingModelList.add(null);
                mAdapter.notifyItemInserted(listingModelList.size() - 1);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        //listingModelList.remove(listingModelList.size() - 1);
                        //mAdapter.notifyItemRemoved(listingModelList.size());

                        // Call you API, then update the result into dataModels, then call adapter.notifyDataSetChanged().
                        //Update the new data into list object]

                        loadData(listingModelList.size() - 1);
                        //mAdapter.setLoaded();
                    }
                });
            }
        });

        listingFunction();


        cariByText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });

        return view;
    }

    private void listingFunction(){
        // TODO :

        progressDialog = ProgressDialog.show(getActivity(), "List Barang", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/barang/getListing/"+sessionManager.getIdUser()+"?index="+indexListing,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");
                            String total = jsonObject.getString("total");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);
                                    int id_barang = jsonData.getInt("id_barang");
                                    int stok = jsonData.getInt("stok");
                                    String nama_barang = jsonData.getString("nama_barang");
                                    String foto = jsonData.getString("foto");
                                    String nama_ecommerce = jsonData.getString("nama_ecommerce");
                                    String url_ecommerce = jsonData.getString("url");
                                    count = count + 1;

                                    ListingModel notificationModel1 = new ListingModel(nama_barang,stok,id_barang,url_ecommerce,nama_ecommerce ,foto, String.valueOf(count));
                                    listingModelList.add(notificationModel1);
                                }

                                infojumlahlisting.setText("Jumlah barang : "+total);
                                cardInfoListing.setVisibility(View.VISIBLE);
//                                //if success navigate to MainActivity
                                recyclerView.setAdapter(mAdapter);
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void loadData(int index){

        Log.e("index", String.valueOf(index));
        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/barang/getListing/"+sessionManager.getIdUser()+"?index="+index,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                listingModelList.remove(listingModelList.size() - 1);
                                mAdapter.setLoaded();

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);
                                    int id_barang = jsonData.getInt("id_barang");
                                    int stok = jsonData.getInt("stok");
                                    String nama_barang = jsonData.getString("nama_barang");
                                    String foto = jsonData.getString("foto");
                                    String nama_ecommerce = jsonData.getString("nama_ecommerce");
                                    String url_ecommerce = jsonData.getString("url");
                                    count = count + 1;

                                    ListingModel notificationModel1 = new ListingModel(nama_barang,stok,id_barang,url_ecommerce,nama_ecommerce ,foto, String.valueOf(count));
                                    listingModelList.add(notificationModel1);
                                }
                                mAdapter.notifyDataSetChanged();
                                //recyclerView.setAdapter(mAdapter);
//                                //if success navigate to MainActivity
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void filter(String text){
        List<ListingModel> temp = new ArrayList();
        for(ListingModel d: listingModelList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getMyBarang().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        mAdapter.updateList(temp);
    }
}
