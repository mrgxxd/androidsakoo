package com.sakoo;

public class ListingModel {

    private String myBarang, url, ecommerce, foto, count;
    private int myStok, myId;

    public ListingModel(String s1, int i1, int i2, String s2, String s3, String s5, String s6){
        this.myBarang = s1;
        this.myStok = i1;
        this.myId = i2;
        this.url = s2;
        this.ecommerce = s3;
        this.foto = s5;
        this.count = s6;
    }

    public String getMyBarang() {
        return myBarang;
    }

    public String getUrl() {
        return url;
    }

    public String getEcommerce() {
        return ecommerce;
    }

    public String getFoto() {
        return foto;
    }

    public int getMyStok() {
        return myStok;
    }

    public int getMyId() {
        return myId;
    }

    public String getCount() {
        return count;
    }
}
