package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    PackageInfo packageInfo;
    String versionName, appPackageName;
    int versionCode;

    private Button loginButton;
    private TextView touchForgotPassword, touchRegister;
    private ImageView showPassword;

    private BaseUtility baseUtility;

    private ProgressDialog progressDialog;
    private EditText inputUsername, inputPassword, inputNumber;
    private String username, password, mode, phoneNumber;

    private SessionManager sessionManager;
    private PrefManager prefManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //DEFINITION AND EVENT COMPONENT
        definitionComponent();
        eventComponent();

        if (getIntent().getBooleanExtra("EXIT", false)) {
            finish();
        }

        //Class
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
        prefManager = new PrefManager(this);

//        if(!Objects.equals(sessionManager.getUserLogin(), "null")){
//            inputUsername.setText(sessionManager.getUserLogin());
//        }

    }

    private void definitionComponent(){
        loginButton = (Button) findViewById(R.id.loginButton);
        touchForgotPassword = (TextView) findViewById(R.id.forgotPasswordTouchable);
        touchRegister = (TextView) findViewById(R.id.registerTouchable);
//
//        inputUsername = (EditText)findViewById(R.id.inputUsername);
        inputPassword = (EditText)findViewById(R.id.inputPassword);

        inputNumber = (EditText)findViewById(R.id.inputNumber);

        //showPassword = (ImageView) findViewById(R.id.showPassword);
    }

    private void eventComponent(){
        loginButton.setOnClickListener(this);
        touchForgotPassword.setOnClickListener(this);
        touchRegister.setOnClickListener(this);

//        showPassword.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch ( event.getAction() ) {
//
//                    case MotionEvent.ACTION_UP:
//                        inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                        break;
//
//                    case MotionEvent.ACTION_DOWN:
//                        inputPassword.setInputType(InputType.TYPE_CLASS_TEXT);
//                        break;
//
//                }
//                return true;
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.loginButton :
                loginFunction();
                //finish();
                break;
            case R.id.forgotPasswordTouchable :
                baseUtility.myIntent(ForgotPasswordActivity.class);
                break;
            case R.id.registerTouchable :
                baseUtility.myIntent(RegisterActivity.class);
                break;
            default:
                break;
        }
    }

    private void loginFunction(){
        loginButton.setEnabled(false);
        // TODO :

        //check username and password value can't null
        if(inputNumber.getText().length() == 0 || inputPassword.getText().length() == 0){
            baseUtility.myToast("Form tidak boleh kosong");
            loginButton.setEnabled(true);
        }else{
//            username = inputUsername.getText().toString();
            password = inputPassword.getText().toString();
            phoneNumber = inputNumber.getText().toString();

            String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
            CharSequence inputStr = inputNumber.getText().toString();
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(inputStr);
            if (!matcher.matches()) {
                mode = "no_hp";
            }else{
                mode = "email";
            }
            //call API login and check response
            progressDialog = ProgressDialog.show(this, "Login", "Please wait ...", true );

            //Log.e("lihat", apiConfig.userEndpoint);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/login",
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("TES", response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int code = jsonObject.getInt("code");
                                String message = jsonObject.getString("message");

                                if(code == 200){
                                    loginButton.setEnabled(true);
                                    String data = jsonObject.getString("data");
                                    prefManager.setFirstTimeLaunch(false);
//                                    progressDialog.dismiss();
//                                    sessionManager.kontakUserSementara(phoneNumber);
//                                    baseUtility.myToast("Silahkan verifikasi akun mu");
//                                    Intent myIntent = new Intent(LoginActivity.this, NewHomeActivity.class);
//                                    myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                                    startActivity(myIntent);
                                    JSONObject jsonData = new JSONObject(data);

                                    sessionManager.createUserLoginSession(
                                            jsonData.getString("id"),
                                            jsonData.getString("nama"),
                                            jsonData.getString("email"),
                                            jsonData.getString("username"),
                                            jsonData.getString("no_hp"),
                                            jsonData.getString("alamat"),
                                            jsonData.getString("foto"),
                                            jsonData.getString("reg_id"),
                                            jsonData.getString("token"),
                                            jsonData.getString("rank"),
                                            jsonData.getString("jumlah_transaksi"),
                                            jsonData.getString("jumlah_listing"),
                                            jsonData.getString("bank"),
                                            jsonData.getString("rekening"),
                                            jsonData.getString("nama_rekening"),
                                            jsonData.getString("lokasi_markup")
                                    );
                                    checkVersion();

                                }else{
                                    loginButton.setEnabled(true);
                                    progressDialog.dismiss();
                                    Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loginButton.setEnabled(true);
                            progressDialog.dismiss();
                            Log.e("erornya", error.toString());
                            baseUtility.myToast(error.toString());
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<String,String>();
                    map.put("username",phoneNumber);
                    map.put("mode",mode);
                    map.put("password",password);
                    map.put("regId",sessionManager.getNewToken());
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("secretkey", "JackTheRipper");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
            requestQueue.add(stringRequest);
        }
    }

    private void checkVersion(){
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
            appPackageName = getPackageName();
            Log.e("Name",appPackageName);
            Log.e("code", String.valueOf(versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/getVersion/"+sessionManager.getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("RESPONSE", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    String message = jsonObject.getString("message");
                    if(code == 200) {
                        String data = jsonObject.getString("data");
                        JSONObject jsonData = new JSONObject(data);
                        String versionData = jsonData.getString("version");
                        String userData = jsonData.getString("user");

                        String jumlahBarang = jsonData.getString("jumlahBarang");
                        String jumlahTransaksi = jsonData.getString("jumlahTransaksi");
                        String jumlahSaldo = jsonData.getString("jumlahSaldo");

                        sessionManager.createHomeStatus(jumlahBarang, jumlahTransaksi, jumlahSaldo);

                        JSONObject versionObject = new JSONObject(versionData);
                        JSONObject userObject = new JSONObject(userData);

                        sessionManager.createUserLoginSession(
                                userObject.getString("id"),
                                userObject.getString("nama"),
                                userObject.getString("email"),
                                userObject.getString("username"),
                                userObject.getString("no_hp"),
                                userObject.getString("alamat"),
                                userObject.getString("foto"),
                                userObject.getString("reg_id"),
                                userObject.getString("token"),
                                userObject.getString("rank"),
                                userObject.getString("jumlah_transaksi"),
                                userObject.getString("jumlah_listing"),
                                userObject.getString("bank"),
                                userObject.getString("rekening"),
                                userObject.getString("nama_rekening"),
                                userObject.getString("lokasi_markup")
                        );

                        progressDialog.dismiss();
                        if(versionCode < versionObject.getInt("version_code")){
                            updateAplikasi();
                        }else{
                            gotomain();
                        }
                    }else{
                        progressDialog.dismiss();
                        gotomain();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GAGAL",error.toString());
                baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                finish();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updateAplikasi(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void gotomain(){
        Intent intent = new Intent(LoginActivity.this, NewHomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
