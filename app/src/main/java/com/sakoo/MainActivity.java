package com.sakoo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private ViewPager viewPager;
    private MyViewPagerAdapter myViewPagerAdapter;
    private LinearLayout dotsLayout, transaksiSeller, listingSeller;
    private TextView[] dots;
    private int[] layouts;

    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private HashMap<String, String> user;

    private ImageView image_profile;
    private TextView nama_barang, nama_email;

    private TextView rankText, transText, listText;

    private CardView upBarang, upStok, upTrans, upSaldo;

    private ProgressDialog progressDialog;

    Timer timer;
    int page = 0;

    private DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //CLASS
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
        dbHelper = new DBHelper(this);

        user = sessionManager.getUserDetails();

        Log.e("jumlah notif", String.valueOf(dbHelper.numberOfRows()));

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });

        upBarang = (CardView) findViewById(R.id.upBarang);
        upStok = (CardView)findViewById(R.id.upStok);
        upTrans = (CardView)findViewById(R.id.upTrans);
        upSaldo = (CardView)findViewById(R.id.upSaldo);

        rankText = (TextView) findViewById(R.id.rankSeller);
        transText = (TextView) findViewById(R.id.transaksiSeller);
        listText = (TextView) findViewById(R.id.listingSeller);

        transaksiSeller = (LinearLayout) findViewById(R.id.cTransaksi);
        listingSeller = (LinearLayout) findViewById(R.id.cListing);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if(Objects.equals(user.get("rank"), "0")){
                rankText.setText("-");
            }else{
                rankText.setText(user.get("rank"));
            }

            if(Objects.equals(user.get("transaction"), "0")){
                transText.setText("-");
            }else{
                transText.setText(user.get("transaction"));
            }

            if(Objects.equals(user.get("listing"), "0")){
                listText.setText("-");
            }else{
                listText.setText(user.get("listing"));
            }
        }

        transaksiSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(MyTransactionActivity.class);
            }
        });

        listingSeller.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(StockActivity.class);
            }
        });

        upBarang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(BarangActivity.class);
            }
        });

        upStok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(StockActivity.class);
            }
        });

        upTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(MyTransactionActivity.class);
            }
        });

        upSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                baseUtility.myIntent(SaldoActivity.class);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View header=navigationView.getHeaderView(0);
        nama_barang = (TextView)header.findViewById(R.id.nama_toko);
        nama_email = (TextView)header.findViewById(R.id.nama_email);
        image_profile = (ImageView)header.findViewById(R.id.image_profile);

        checkProfile();


        //NEWS TICKER
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        dotsLayout = (LinearLayout) findViewById(R.id.dots);

        // layouts of all welcome sliders
        // add few more layouts if you want
        layouts = new int[]{
                R.layout.news_ticker_1,
                R.layout.news_ticker_2,
                R.layout.news_ticker_3};

        // adding bottom dots
        addBottomDots(0);

        // making notification bar transparent
        changeStatusBarColor();

        myViewPagerAdapter = new MyViewPagerAdapter();
        viewPager.setAdapter(myViewPagerAdapter);
        viewPager.addOnPageChangeListener(viewPagerPageChangeListener);
        pageSwitcher(5);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        // Get the notifications MenuItem and LayerDrawable (layer-list)
        MenuItem item = menu.findItem(R.id.notif_right);
        LayerDrawable icon = (LayerDrawable) item.getIcon();

        // Update LayerDrawable's BadgeDrawable
        int notification = dbHelper.getAllData();
        Utils3.setBadgeCount(this, icon, notification);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.notif_right) {
            dbHelper.updateNotif();
            baseUtility.myIntent(NotificationActivity.class);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.beranda) {
            // Handle the camera action
        } else if (id == R.id.my_profile) {
            baseUtility.myIntent(EditProfileActivity.class);
        } else if (id == R.id.my_saldo) {
            dbHelper.upSaldoData();
            baseUtility.myIntent(SaldoActivity.class);
        } else if (id == R.id.update_barang) {
            baseUtility.myIntent(BarangActivity.class);

        } else if (id == R.id.update_stok) {
            dbHelper.upBarangData();
            baseUtility.myIntent(StockActivity.class);

        } else if (id == R.id.transaksi) {
            dbHelper.upTransaksiData();
            baseUtility.myIntent(MyTransactionActivity.class);

        } else if (id == R.id.notifikasi) {
            baseUtility.myIntent(NotificationActivity.class);

        } else if (id == R.id.ranking_seller) {

        } else if (id == R.id.hubungi_kami) {
            lihatKontak();
        } else if (id == R.id.faq) {

        } else if (id == R.id.logout) {
            dbHelper.deleteData();
            sessionManager.logoutUser();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //FUNCTION VIEW PAGER FOR NEWS TICKER
    private void addBottomDots(int currentPage) {
        dots = new TextView[layouts.length];

        int[] colorsActive = getResources().getIntArray(R.array.array_dot_active);
        int[] colorsInactive = getResources().getIntArray(R.array.array_dot_inactive);

        dotsLayout.removeAllViews();
        for (int i = 0; i < dots.length; i++) {
            dots[i] = new TextView(getApplicationContext());
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(35);
            dots[i].setTextColor(colorsInactive[currentPage]);
            dotsLayout.addView(dots[i]);
        }

        if (dots.length > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage]);
    }

    private int getItem(int i) {
        return viewPager.getCurrentItem() + i;
    }

    ViewPager.OnPageChangeListener viewPagerPageChangeListener = new ViewPager.OnPageChangeListener() {

        @Override
        public void onPageSelected(int position) {
            addBottomDots(position);

            // changing the next button text 'NEXT' / 'GOT IT'
//            if (position == layouts.length - 1) {
//                // last page. make button text to GOT IT
//                //btnNext.setText(getString(R.string.start));
//                //btnSkip.setVisibility(View.GONE);
//            } else {
//                // still pages are left
//                //btnNext.setText(getString(R.string.next));
//                //btnSkip.setVisibility(View.VISIBLE);
//            }
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {

        }

        @Override
        public void onPageScrollStateChanged(int arg0) {

        }


    };

    /**
     * Making notification bar transparent
     */
    private void changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public class MyViewPagerAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        public MyViewPagerAdapter() {
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            View view = layoutInflater.inflate(layouts[position], container, false);
            container.addView(view);

            return view;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object obj) {
            return view == obj;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private void checkProfile(){
        if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("token").isEmpty()){
            Intent intent = new Intent(MainActivity.this, EditProfileActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }else{
            nama_barang.setText(user.get("nama"));
            nama_email.setText(user.get("email"));

            if(!user.get("foto").isEmpty()){
                String temp = user.get("foto");
                temp = temp.replaceAll(" ", "%20");
                Picasso.with(this).load(temp).into(image_profile);
            }
        }
//        Log.e("session", user.get("id"));
//        Log.e("session", user.get("nama"));
//        Log.e("session", user.get("email"));
//        Log.e("session", user.get("username"));
//        Log.e("session", user.get("no_hp"));
//        Log.e("session", user.get("alamat"));
//        Log.e("session", user.get("foto"));
//        Log.e("session", user.get("reg_id"));
//        Log.e("session", user.get("token"));
    }

    public void pageSwitcher(int seconds) {
        timer = new Timer(); // At this line a new Thread will be created
        timer.scheduleAtFixedRate(new RemindTask(), 0, seconds * 1000); // delay
        // in
        // milliseconds
    }

    // this is an inner class...
    class RemindTask extends TimerTask {

        @Override
        public void run() {

            // As the TimerTask run on a seprate thread from UI thread we have
            // to call runOnUiThread to do work on UI thread.
            runOnUiThread(new Runnable() {
                public void run() {

                    if (page > 2) { // In my case the number of pages are 5
                        timer.cancel();
                        // Showing a toast for just testing purpose
//                        Toast.makeText(getApplicationContext(), "Timer stoped",
//                                Toast.LENGTH_LONG).show();
                    } else {
                        viewPager.setCurrentItem(page++);
                    }
                }
            });

        }
    }

    private void lihatKontak(){
        progressDialog = ProgressDialog.show(this, "Mencari Kontak", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/getKontak/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONObject jsonData = new JSONObject(data);
                                tampilKontak(jsonData.getString("kontak_admin"));
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void tampilKontak(final String kontak){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
        alertDialog.setMessage("Hubungi Kami");

        LayoutInflater inflater = LayoutInflater.from(MainActivity.this);
        final View dialogView = inflater.inflate(R.layout.dialog_call_me, null);

        CardView cardKontak = (CardView) dialogView.findViewById(R.id.card_view_kontak);
        TextView nomorKontak = (TextView)dialogView.findViewById(R.id.nomorKontak);
        nomorKontak.setText("+"+kontak);

        alertDialog.setView(dialogView);

        alertDialog.setPositiveButton("Tutup",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        cardKontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri mUri = Uri.parse("https://api.whatsapp.com/send?phone=" + kontak);
                Intent mIntent = new Intent("android.intent.action.VIEW", mUri);
                mIntent.setPackage("com.whatsapp");
                startActivity(mIntent);
            }
        });

        alertDialog.show();
    }
}
