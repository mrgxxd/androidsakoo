package com.sakoo;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class MyHistorySaldoAdapter extends RecyclerView.Adapter<MyHistorySaldoAdapter.MyViewHolder> {

    private List<MyHistorySaldoModel> listingModelList;
    private Context context;

    public MyHistorySaldoAdapter(Context context, List<MyHistorySaldoModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView noTrx, jumlahSaldo, tanggalTransfer, countNumber;
        public Button detailSaldo;

        public MyViewHolder(View view) {
            super(view);
            noTrx = (TextView) view.findViewById(R.id.noTrx);
            jumlahSaldo = (TextView) view.findViewById(R.id.jumlahSaldo);
            detailSaldo = (Button) view.findViewById(R.id.detailDana);
            tanggalTransfer = (TextView) view.findViewById(R.id.tanggaTransfer);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_history_saldo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MyHistorySaldoModel documentModel = listingModelList.get(position);
        holder.noTrx.setText("No : "+documentModel.getNoTrx());

        DecimalFormat kursIndonesia = new DecimalFormat("#,###");

        holder.jumlahSaldo.setText("Jumlah dana : Rp. "+String.valueOf(kursIndonesia.format(Long.valueOf(documentModel.getJumlahDana()))).replace(",","."));

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String theDate = documentModel.getTanggalTransfer();
        Date date = null;
        try {
            date = dateFormat.parse(theDate);
            dateFormat.applyPattern("dd/MM/yyyy HH:mm");
            holder.tanggalTransfer.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.detailSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailSaldoActivity.class);
                intent.putExtra("idTransaksiSeller",documentModel.getIdTransaksi());
                context.startActivity(intent);
            }
        });


//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
