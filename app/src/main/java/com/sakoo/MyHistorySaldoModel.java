package com.sakoo;

public class MyHistorySaldoModel {

    private int idTransaksi;
    private String jumlahDana, noTrx, tanggalTransfer;

    public MyHistorySaldoModel(int i2, String s1, String s2, String s3){
        this.idTransaksi = i2;
        this.jumlahDana = s1;
        this.noTrx = s2;
        this.tanggalTransfer = s3;
    }

    public String getJumlahDana() {
        return jumlahDana;
    }

    public String getNoTrx() {
        return noTrx;
    }

    public int getIdTransaksi() {
        return idTransaksi;
    }

    public String getTanggalTransfer() {
        return tanggalTransfer;
    }
}
