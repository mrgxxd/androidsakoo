package com.sakoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class MySaldoAdapter extends RecyclerView.Adapter<MySaldoAdapter.MyViewHolder> {

    private List<MySaldoModel> listingModelList;
    private Context context;
    private HashMap<String,String> user;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;

    public MySaldoAdapter(Context context, List<MySaldoModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
        this.sessionManager = new SessionManager(this.context);
        this.user = sessionManager.getUserDetails();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView noTrx, jumlahSaldo, countNumber;
        public Button cairkanDana, detailTransaksi;

        public MyViewHolder(View view) {
            super(view);
            noTrx = (TextView) view.findViewById(R.id.noTrx);
            jumlahSaldo = (TextView) view.findViewById(R.id.jumlahSaldo);
            //countNumber = (TextView) view.findViewById(R.id.countNumber);
            cairkanDana = (Button) view.findViewById(R.id.cairkanDana);
            detailTransaksi = (Button) view.findViewById(R.id.lihatTransaksi);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_my_saldo, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MySaldoModel documentModel = listingModelList.get(position);
        holder.noTrx.setText("No : "+documentModel.getNoTrx());
        //holder.countNumber.setText(documentModel.getCount());

        DecimalFormat kursIndonesia = new DecimalFormat("#,###");

        holder.jumlahSaldo.setText("Jumlah dana : Rp. "+String.valueOf(kursIndonesia.format(Long.valueOf(documentModel.getJumlahDana()))).replace(",","."));

        holder.cairkanDana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetCode(documentModel.getId());
            }
        });

        holder.detailTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TransactionActivity.class);
                intent.putExtra("idTransaksi", documentModel.getId());
                context.startActivity(intent);
            }
        });


//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }

    private void konfirmSaldo(final int konfId){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage("Masukan Kode yang didapatkan melalui sms");

        final EditText input = new EditText(context);
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(context, "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            String confirmationCode = input.getText().toString();
                            cairkanDanaFunction(confirmationCode, konfId);
                            dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    public void cairkanDanaFunction(final String myCode, int id){
        // TODO :

        progressDialog = ProgressDialog.show(context, "Cairkan Dana", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/cairkanDana/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, NewHomeActivity.class);
                                intent.putExtra("FRAGMENT_ID", 3);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "Pencairan dana sukses", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi bermasalah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("code_verifikasi",myCode);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void resetCode(final int saldoId){
        // TODO :
        //call API login and check response
        progressDialog = ProgressDialog.show(context, "Mendapatkan Code", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/codeSaldo",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");

                            if(code == 200){
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                Intent intent = new Intent(context, VerificationActivity.class);
                                intent.putExtra("saldoId", saldoId);
                                intent.putExtra("mode", "one");
                                context.startActivity(intent);
                                //konfirmSaldo(saldoId);
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, "Code gagal dikirim", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi Bermasalah silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("no_hp",user.get("no_hp"));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }
}
