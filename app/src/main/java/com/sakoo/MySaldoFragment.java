package com.sakoo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MySaldoFragment extends Fragment {

    private LinearLayout linearLayout1;
    private List<MySaldoModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MySaldoAdapter mAdapter;

    private ProgressDialog progressDialog;
    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private HashMap<String,String> user;

    int totalSaldo = 0;
    String totalId = "";
    String confirmationPassword = "";

    private TextView infoSaldo;
    private Button cairkanSemua;
    private CardView cardViewSaldo;

    private int count = 0;

    private ImageView saldoKosong;
    private TextView textSaldoKosong;

    public MySaldoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseUtility = new BaseUtility(getActivity());
        sessionManager = new SessionManager(getActivity());
        user = sessionManager.getUserDetails();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saldo, container, false);

        saldoKosong = (ImageView) view.findViewById(R.id.saldoKosong);
        textSaldoKosong = (TextView) view.findViewById(R.id.textSaldoKosong);

        infoSaldo = (TextView)view.findViewById(R.id.infoSaldo);
        cairkanSemua = (Button) view.findViewById(R.id.cairkanSemua);
        cardViewSaldo = (CardView) view.findViewById(R.id.cardInfoSaldo);

        cairkanSemua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCode();
            }
        });
        // RecycleView
        recyclerView = (RecyclerView)view.findViewById(R.id.listSaldo);
        listingModelList = new ArrayList<>();
        mAdapter = new MySaldoAdapter(getActivity(),listingModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(sessionManager.isUserLoggedIn()){
            saldoFunction();
        }else{
            saldoKosong.setVisibility(View.VISIBLE);
            textSaldoKosong.setVisibility(View.VISIBLE);
        }


        return view;
    }

    private void saldoFunction(){
        // TODO :
        totalSaldo = 0;

        progressDialog = ProgressDialog.show(getActivity(), "Saldo", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/transaction/getSaldo/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);
                                    int id = jsonData.getInt("id_transaksi_seller");
                                    int idTransaksi = jsonData.getInt("id_transaksi");
                                    String noTrx = jsonData.getString("no_trx");
                                    String jumlahUang = jsonData.getString("jumlah_uang");

                                    totalSaldo = totalSaldo + Integer.parseInt(jumlahUang);
                                    if(j == 0){
                                        totalId = String.valueOf(id);
                                    }else{
                                        totalId = totalId + ", "+String.valueOf(id);
                                    }

                                    count = count + 1;

                                    MySaldoModel notificationModel1 = new MySaldoModel(id, idTransaksi,jumlahUang, noTrx, String.valueOf(count));
                                    listingModelList.add(notificationModel1);
                                }

                                if(listingModelList.size() == 0){
                                    saldoKosong.setVisibility(View.VISIBLE);
                                    textSaldoKosong.setVisibility(View.VISIBLE);
                                }else{
                                    saldoKosong.setVisibility(View.INVISIBLE);
                                    textSaldoKosong.setVisibility(View.INVISIBLE);

//                                    DecimalFormat kursIndonesia = (DecimalFormat) DecimalFormat.getCurrencyInstance();
//                                    DecimalFormatSymbols formatRp = new DecimalFormatSymbols("#,###");

                                    DecimalFormat kursIndonesia = new DecimalFormat("#,###");

//                                    formatRp.setCurrencySymbol("Rp. ");
//                                    formatRp.setGroupingSeparator('.');
//
//                                    kursIndonesia.setDecimalFormatSymbols(formatRp);

                                    infoSaldo.setText("Jumlah saldo yang anda miliki adalah "+ String.valueOf(kursIndonesia.format(totalSaldo)).replace(",","."));
                                    cardViewSaldo.setVisibility(View.VISIBLE);
                                }

//                                //if success navigate to MainActivity
                                recyclerView.setAdapter(mAdapter);
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void konfirmSaldo(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Masukan Kode yang didapatkan melalui sms");

        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(getActivity(), "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            confirmationPassword = input.getText().toString();
                            cairkanSemuaFunction();
                            dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    public void cairkanSemuaFunction(){
        progressDialog = ProgressDialog.show(getActivity(), "Saldo", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/transaction/cairkanSemuaDana",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                Intent intent = new Intent(getActivity(), NewHomeActivity.class);
                                intent.putExtra("FRAGMENT_ID", 3);
                                startActivity(intent);
                                getActivity().finish();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan cek koneksi anda");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id", totalId);
                map.put("code_verifikasi", confirmationPassword);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void getCode(){
        // TODO :
        //call API login and check response
        progressDialog = ProgressDialog.show(getActivity(), "Mendapatkan Code", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/codeSaldo",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");

                            if(code == 200){
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                Intent intent = new Intent(getActivity(), VerificationActivity.class);
                                intent.putExtra("saldoId", totalId);
                                intent.putExtra("mode", "all");
                                getActivity().startActivity(intent);
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Code gagal dikirim", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(getActivity(), "Koneksi Bermasalah silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("no_hp",user.get("no_hp"));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
