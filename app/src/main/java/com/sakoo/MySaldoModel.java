package com.sakoo;

public class MySaldoModel {

    private int id, idTransaksi;
    private String jumlahDana, noTrx, count;

    public MySaldoModel(int i1, int i2, String s1, String s2, String s3){
        this.id = i1;
        this.idTransaksi = i2;
        this.jumlahDana = s1;
        this.noTrx = s2;
        this.count = s3;
    }

    public int getId() {
        return id;
    }

    public String getJumlahDana() {
        return jumlahDana;
    }

    public String getNoTrx() {
        return noTrx;
    }

    public int getIdTransaksi() {
        return idTransaksi;
    }

    public String getCount() {
        return count;
    }
}
