package com.sakoo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

public class MyTransactionAdapter extends RecyclerView.Adapter<MyTransactionAdapter.MyViewHolder> {

    private List<MyTransactionModel> listingModelList;
    private Context context;

    public MyTransactionAdapter(Context context, List<MyTransactionModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView myFoto;
        public TextView myBarang, myBuyer, tanggal, textStatus, textResi;
        private Button detailTransaction;

        public MyViewHolder(View view) {
            super(view);
            myBarang = (TextView) view.findViewById(R.id.noorder);
            tanggal = (TextView) view.findViewById(R.id.tanggal);
            myBuyer = (TextView) view.findViewById(R.id.namaPemesan);
            textStatus = (TextView) view.findViewById(R.id.textStatus);
            textResi = (TextView) view.findViewById(R.id.textResi);
            detailTransaction = (Button) view.findViewById(R.id.detailTransaksi);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_my_transaction, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MyTransactionModel documentModel = listingModelList.get(position);
        holder.textStatus.setText(documentModel.getTextStatus());
        holder.myBarang.setText("No : "+documentModel.getNoorder().trim());
        holder.myBuyer.setText("Pembeli : "+documentModel.getBuyer().trim());
        holder.detailTransaction.setText("Proses");

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String theDate = documentModel.getTanggal();
        Date date = null;
        try {
            date = dateFormat.parse(theDate);
            dateFormat.applyPattern("dd/MM/yyyy HH:mm");
            holder.tanggal.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//                                String[] a = jsonData.getString("tanggal_pemesanan").split("T");
//                                String tanggal = a[0];
//                                String[] b = a[1].split("\\.");
//                                String waktu = b[0];



        holder.detailTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TransactionActivity.class);
                intent.putExtra("idTransaksi", documentModel.getIdtransaksi());
                context.startActivity(intent);
            }
        });

//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
