package com.sakoo;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

public class MyTransactionHistoryAdapter extends RecyclerView.Adapter<MyTransactionHistoryAdapter.MyViewHolder> {

    private List<MyTransactionHistoryModel> listingModelList;
    private Context context;

    public MyTransactionHistoryAdapter(Context context, List<MyTransactionHistoryModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView myFoto;
        public TextView myBarang, myBuyer, tanggal, textStatus;
        private Button detailTransaction;

        public MyViewHolder(View view) {
            super(view);
            myBarang = (TextView) view.findViewById(R.id.noorder);
            tanggal = (TextView) view.findViewById(R.id.tanggal);
            myBuyer = (TextView) view.findViewById(R.id.namaPemesan);
            textStatus = (TextView) view.findViewById(R.id.textStatus);
            detailTransaction = (Button) view.findViewById(R.id.detailTransaksi);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_my_transaction, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final MyTransactionHistoryModel documentModel = listingModelList.get(position);
        holder.textStatus.setText(documentModel.getTextStatus());
        holder.myBarang.setText("No : "+documentModel.getNoorder().trim());
        holder.myBuyer.setText("Pembeli : "+documentModel.getBuyer().trim());

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String theDate = documentModel.getTanggal();
        Date date = null;
        try {
            date = dateFormat.parse(theDate);
            dateFormat.applyPattern("dd/MM/yyyy HH:mm");
            holder.tanggal.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.detailTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TransactionActivity.class);
                intent.putExtra("idTransaksi", documentModel.getIdtransaksi());
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
