package com.sakoo;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyTransactionHistoryFragment extends Fragment{

    private LinearLayout linearLayout1;
    private List<MyTransactionHistoryModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyTransactionHistoryAdapter mAdapter;


    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private  int count = 0;

    private TextView infoTransactionHistory;
    private CardView cardInfoinfoTransactionHistory;

    private ImageView transaksiKosong;
    private TextView textTransaksiKosong;

    public MyTransactionHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseUtility = new BaseUtility(getActivity());
        sessionManager = new SessionManager(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_transaction_history, container, false);

        transaksiKosong = (ImageView) view.findViewById(R.id.transaksiKosong);
        textTransaksiKosong = (TextView) view.findViewById(R.id.textTransaksiKosong);

        // RecycleView
        infoTransactionHistory = (TextView) view.findViewById(R.id.infoTransactionHistory);
        cardInfoinfoTransactionHistory = (CardView) view.findViewById(R.id.cardInfoTransactionHistory);
        recyclerView = (RecyclerView)view.findViewById(R.id.listTransactionHistory);
        listingModelList = new ArrayList<>();
        mAdapter = new MyTransactionHistoryAdapter(getActivity(),listingModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(sessionManager.isUserLoggedIn()){
            listTransactionFunction();
        }else{
            transaksiKosong.setVisibility(View.VISIBLE);
            textTransaksiKosong.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void listTransactionFunction(){
        // TODO :

        progressDialog = ProgressDialog.show(getActivity(), "Transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/transaction/getTheTransactionHistory/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                //if success create session user
                                JSONArray jsonArray = new JSONArray(data);
                                for(int i =0; i < jsonArray.length(); i++){
                                    JSONObject jsonData = jsonArray.getJSONObject(i);

                                    count = count + 1;

                                    MyTransactionHistoryModel notificationModel1 = new MyTransactionHistoryModel(jsonData.getInt("id_transaksi"), jsonData.getString("oreder_no"), jsonData.getString("tanggal_pemesanan"), jsonData.getString("buyer"), jsonData.getInt("status"), String.valueOf(count), jsonData.getString("text_status"));
                                    listingModelList.add(notificationModel1);
                                }

                                if(listingModelList.size() == 0){
                                    transaksiKosong.setVisibility(View.VISIBLE);
                                    textTransaksiKosong.setVisibility(View.VISIBLE);
                                }else{
                                    transaksiKosong.setVisibility(View.INVISIBLE);
                                    textTransaksiKosong.setVisibility(View.INVISIBLE);
                                    cardInfoinfoTransactionHistory.setVisibility(View.VISIBLE);
                                    infoTransactionHistory.setText("Jumlah transaksi : "+jsonArray.length());
                                }

                                recyclerView.setAdapter(mAdapter);
                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
