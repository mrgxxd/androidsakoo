package com.sakoo;

public class MyTransactionModel {

    private int idtransaksi, status;
    private String noorder, tanggal, buyer, count, textStatus;

    public MyTransactionModel(int i1, String s1, String s2, String s3, int i2, String s4, String s5){
        this.idtransaksi = i1;
        this.noorder = s1;
        this.tanggal = s2;
        this.buyer = s3;
        this.status = i2;
        this.count = s4;
        this.textStatus = s5;
    }

    public int getIdtransaksi() {
        return idtransaksi;
    }

    public String getNoorder() {
        return noorder;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getBuyer() {
        return buyer;
    }

    public int getStatus() {
        return status;
    }

    public String getCount() {
        return count;
    }

    public String getTextStatus() {
        return textStatus;
    }
}
