package com.sakoo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

public class NewBarangFragment extends Fragment {

    private FragmentActivity myContext;

    private SessionManager sessionManager;
    private BaseUtility baseUtility;
    private ProgressDialog progressDialog;
    private HashMap<String,String> user;

    private LinearLayout linearLayout1;
    private List<StokModel> listingModelList, listingModelListCadangan;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private StokAdapter mAdapter;

    private int indexListing = 0;

    private String searching = "", filtering = "", sorting = "";

    private MenuItem mSearchAction;
    private boolean isSearchOpened = false;
    private EditText edtSeach;

    private TextView textTitle;

    private ImageView barangKosong;
    private TextView textBarangKosong;

    private Timer timer;

    public static NewBarangFragment newInstance() {
        NewBarangFragment fragment = new NewBarangFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        baseUtility = new BaseUtility(getActivity());
        user = sessionManager.getUserDetails();
        setHasOptionsMenu(true);

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_stock, container, false);

        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        barangKosong = (ImageView) view.findViewById(R.id.barangKosong);
        textBarangKosong = (TextView) view.findViewById(R.id.textBarangKosong);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fabTambah);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sessionManager.isUserLoggedIn()){
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                            Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                            //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }else{
                            Intent intent = new Intent(getActivity(), BarangActivity.class);
                            startActivity(intent);
                        }
                    }
                }else{
                    baseUtility.myIntent(LoginActivity.class);
                }
            }
        });

        textTitle = (TextView)toolbar.findViewById(R.id.textTitle);

        recyclerView = (RecyclerView)view.findViewById(R.id.listStock);
        listingModelList = new ArrayList<>();
        listingModelListCadangan = new ArrayList<>();
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new StokAdapter(getActivity(),listingModelList, recyclerView);
        mAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                listingModelList.add(null);
                mAdapter.notifyItemInserted(listingModelList.size() - 1);
                recyclerView.post(new Runnable() {
                    @Override
                    public void run() {
                        //listingModelList.remove(listingModelList.size() - 1);
                        //mAdapter.notifyItemRemoved(listingModelList.size());

                        // Call you API, then update the result into dataModels, then call adapter.notifyDataSetChanged().
                        //Update the new data into list object]

                        loadData(listingModelList.size() - 1);
                        //mAdapter.setLoaded();
                    }
                });
            }
        });

        if(sessionManager.isUserLoggedIn()){
            loadDataAwal(indexListing);
            barangKosong.setVisibility(View.INVISIBLE);
            textBarangKosong.setVisibility(View.INVISIBLE);
        }else{
            barangKosong.setVisibility(View.VISIBLE);
            textBarangKosong.setVisibility(View.VISIBLE);
        }

        return view;
    }

    private void loadDataAwal(final int indexData){

        progressDialog = ProgressDialog.show(getActivity(), "List Barang", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/getListingUnlisting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);

                                    StokModel notificationModel1 = new StokModel(
                                            jsonData.getInt("id_barang"),
                                            jsonData.getString("nama"),
                                            jsonData.getString("deskripsi"),
                                            jsonData.getInt("harga"),
                                            jsonData.getInt("stok"),
                                            jsonData.getString("foto"),
                                            jsonData.getString("tanggal_upload"),
                                            jsonData.getString("tanggal_update"),
                                            jsonData.getString("tanggal_listing"),
                                            jsonData.getInt("id_subkategori"),
                                            jsonData.getString("nama_subkategori"),
                                            jsonData.getString("ecommerce"),
                                            jsonData.getString("status"),
                                            jsonData.getString("foto_tambahan"),
                                            jsonData.getInt("berat"),
                                            jsonData.getString("status_unlisting"),
                                            jsonData.getString("volume"),
                                            jsonData.getString("bahan"),
                                            jsonData.getString("merk")
                                    );
                                    listingModelList.add(notificationModel1);
                                    listingModelListCadangan.add(notificationModel1);
                                }
                                recyclerView.setAdapter(mAdapter);
                                if(listingModelList.size() == 0){
                                    barangKosong.setVisibility(View.VISIBLE);
                                    textBarangKosong.setVisibility(View.VISIBLE);
                                }else {
                                    barangKosong.setVisibility(View.INVISIBLE);
                                    textBarangKosong.setVisibility(View.INVISIBLE);
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",sessionManager.getIdUser());
                map.put("searching",searching);
                map.put("filtering",filtering);
                map.put("sorting",sorting);
                map.put("index", String.valueOf(indexData));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void loadData(final int indexData){

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/getListingUnlisting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                listingModelList.remove(listingModelList.size() - 1);
                                mAdapter.setLoaded();

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);

                                    StokModel notificationModel1 = new StokModel(
                                            jsonData.getInt("id_barang"),
                                            jsonData.getString("nama"),
                                            jsonData.getString("deskripsi"),
                                            jsonData.getInt("harga"),
                                            jsonData.getInt("stok"),
                                            jsonData.getString("foto"),
                                            jsonData.getString("tanggal_upload"),
                                            jsonData.getString("tanggal_update"),
                                            jsonData.getString("tanggal_listing"),
                                            jsonData.getInt("id_subkategori"),
                                            jsonData.getString("nama_subkategori"),
                                            jsonData.getString("ecommerce"),
                                            jsonData.getString("status"),
                                            jsonData.getString("foto_tambahan"),
                                            jsonData.getInt("berat"),
                                            jsonData.getString("status_unlisting"),
                                            jsonData.getString("volume"),
                                            jsonData.getString("bahan"),
                                            jsonData.getString("merk")
                                    );
                                    listingModelList.add(notificationModel1);
                                    listingModelListCadangan.add(notificationModel1);
                                }
                                mAdapter.notifyDataSetChanged();
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",sessionManager.getIdUser());
                map.put("searching",searching);
                map.put("filtering",filtering);
                map.put("sorting",sorting);
                map.put("index", String.valueOf(indexData));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // TODO Add your menu entries here
        inflater.inflate(R.menu.menu_stok, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        mSearchAction = menu.findItem(R.id.action_search);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            handleMenuSearch();
        }else if(menuItem.getItemId() == R.id.action_search){
            if(sessionManager.isUserLoggedIn()){
                handleMenuSearch();
            }
        }else if(menuItem.getItemId() == R.id.action_filter){
            if(sessionManager.isUserLoggedIn()){
                dialogFilter();
            }
        }else if(menuItem.getItemId() == R.id.action_sort){
            if(sessionManager.isUserLoggedIn()){
                dialogSort();
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }


    protected void handleMenuSearch(){
        ActionBar action = ((AppCompatActivity)getActivity()).getSupportActionBar(); //get the actionbar

        if(isSearchOpened){ //test if the search is open

//            textTitle.setVisibility(View.VISIBLE);
//            action.setDisplayShowCustomEnabled(false); //disable a custom view inside the actionbar
//            action.setDisplayHomeAsUpEnabled(true);
//            action.setDisplayShowHomeEnabled(true);
//
//            //hides the keyboard
//            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
//            imm.hideSoftInputFromWindow(edtSeach.getWindowToken(), 0);
//
//            //add the search icon in the action bar
//            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_search_black_24dp));
//
//            isSearchOpened = false;
            Intent intent = new Intent(getActivity(), NewHomeActivity.class);
            intent.putExtra("FRAGMENT_ID", 1);
            startActivity(intent);
            getActivity().finish();

//            Intent i = new Intent(getActivity(), StockActivity.class);
//            startActivity(i);
//            getActivity().finish();
        } else { //open the search entry

            textTitle.setVisibility(View.GONE);
            action.setDisplayShowCustomEnabled(true); //enable it to display a
            // custom view in the action bar.
            action.setCustomView(R.layout.search_bar);//add the custom view
            action.setDisplayShowTitleEnabled(false); //hide the title
            action.setDisplayHomeAsUpEnabled(false);
            action.setDisplayShowHomeEnabled(false);

            edtSeach = (EditText)action.getCustomView().findViewById(R.id.edtSearch); //the text editor

            edtSeach.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (timer != null) {
                        timer.cancel();
                    }

                    // TODO Auto-generated method stub
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    // TODO Auto-generated method stub
                }

                @Override
                public void afterTextChanged(final Editable s) {

                    // filter your list from your input
                    //you can use runnable postDelayed like 500 ms to delay search text

                    final Runnable setFIlter = new Runnable() {
                        public void run() {
                            filter(s.toString());
                        }
                    };

                    TimerTask task = new TimerTask(){
                        public void run() {
                            getActivity().runOnUiThread(setFIlter);
                        }
                    };

                    timer = new Timer();
                    timer.schedule(task, 500);

                }
            });

            //this is a listener to do a search when the user clicks on search button
            edtSeach.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                        doSearch();
                        return true;
                    }
                    return false;
                }
            });


            edtSeach.requestFocus();

            //open the keyboard focused in the edtSearch
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(edtSeach, InputMethodManager.SHOW_IMPLICIT);


            //add the close icon
            mSearchAction.setIcon(getResources().getDrawable(R.drawable.ic_close_black_24dp));

            isSearchOpened = true;
        }
    }

    private void filter(String text){
        List<StokModel> temp = new ArrayList();
        for(StokModel d: listingModelList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches

            if(d != null){
                if(d.getNama().toLowerCase().contains(text.toLowerCase())){
                    temp.add(d);
                }
            }
        }
        //update recyclerview
        mAdapter.updateList(temp);
    }

    public void doSearch(){
        progressDialog = ProgressDialog.show(getActivity(), "Cari Barang", "Please wait ...", true );

        searching = edtSeach.getText().toString();
        filtering = "";
        sorting = "";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/getListingUnlisting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                listingModelList = new ArrayList();

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                if(jsonArray.length() != 0){
                                    for(int j = 0; j < jsonArray.length(); j++){
                                        //if success create session user
                                        JSONObject jsonData = jsonArray.getJSONObject(j);

                                        StokModel notificationModel1 = new StokModel(
                                                jsonData.getInt("id_barang"),
                                                jsonData.getString("nama"),
                                                jsonData.getString("deskripsi"),
                                                jsonData.getInt("harga"),
                                                jsonData.getInt("stok"),
                                                jsonData.getString("foto"),
                                                jsonData.getString("tanggal_upload"),
                                                jsonData.getString("tanggal_update"),
                                                jsonData.getString("tanggal_listing"),
                                                jsonData.getInt("id_subkategori"),
                                                jsonData.getString("nama_subkategori"),
                                                jsonData.getString("ecommerce"),
                                                jsonData.getString("status"),
                                                jsonData.getString("foto_tambahan"),
                                                jsonData.getInt("berat"),
                                                jsonData.getString("status_unlisting"),
                                                jsonData.getString("volume"),
                                                jsonData.getString("bahan"),
                                                jsonData.getString("merk")
                                        );
                                        listingModelList.add(notificationModel1);
                                    }
                                    mAdapter.updateList(listingModelList);
                                }else{
                                    Toast.makeText(getActivity(), "Barang yang anda cari tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",sessionManager.getIdUser());
                map.put("searching",searching);
                map.put("filtering",filtering);
                map.put("sorting",sorting);
                map.put("index", "0");
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void dialogFilter(){
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setTitle("Filter");
        String[] types = {"Listing", "Unlisting"};
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        doFilter("listing");
                        break;
                    case 1:
                        doFilter("unlisting");
                        break;
                }
            }

        });
        b.show();
    }

    private void dialogSort(){
        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
        b.setTitle("Urutkan");
        String[] types = {"Min Harga", "Max Harga", "Terbaru Diupdate", "Terlama Diupdate", "Nama Barang"};
        b.setItems(types, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.dismiss();
                switch(which){
                    case 0:
                        doSorting("minharga");
                        break;
                    case 1:
                        doSorting("maxharga");
                        break;
                    case 2:
                        doSorting("newupdate");
                        break;
                    case 3:
                        doSorting("lastupdate");
                        break;
                    case 4:
                        doSorting("name");
                        break;
                }
            }

        });
        b.show();
    }

    public void doFilter(String filter){
        progressDialog = ProgressDialog.show(getActivity(), "Filter Barang", "Please wait ...", true );

        filtering = filter;
        searching = "";
        sorting = "";

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/getListingUnlisting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                listingModelList = new ArrayList();

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                if(jsonArray.length() != 0){
                                    for(int j = 0; j < jsonArray.length(); j++){
                                        //if success create session user
                                        JSONObject jsonData = jsonArray.getJSONObject(j);

                                        StokModel notificationModel1 = new StokModel(
                                                jsonData.getInt("id_barang"),
                                                jsonData.getString("nama"),
                                                jsonData.getString("deskripsi"),
                                                jsonData.getInt("harga"),
                                                jsonData.getInt("stok"),
                                                jsonData.getString("foto"),
                                                jsonData.getString("tanggal_upload"),
                                                jsonData.getString("tanggal_update"),
                                                jsonData.getString("tanggal_listing"),
                                                jsonData.getInt("id_subkategori"),
                                                jsonData.getString("nama_subkategori"),
                                                jsonData.getString("ecommerce"),
                                                jsonData.getString("status"),
                                                jsonData.getString("foto_tambahan"),
                                                jsonData.getInt("berat"),
                                                jsonData.getString("status_unlisting"),
                                                jsonData.getString("volume"),
                                                jsonData.getString("bahan"),
                                                jsonData.getString("merk")
                                        );
                                        listingModelList.add(notificationModel1);
                                    }
                                    mAdapter.updateList(listingModelList);
                                }else{
                                    Toast.makeText(getActivity(), "Barang yang anda cari tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",sessionManager.getIdUser());
                map.put("searching",searching);
                map.put("filtering",filtering);
                map.put("sorting",sorting);
                map.put("index", "0");
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    public void doSorting(String sortData){
        progressDialog = ProgressDialog.show(getActivity(), "Urutkan Barang", "Please wait ...", true );

        filtering = "";
        searching = "";
        sorting = sortData;

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/barang/getListingUnlisting",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){

                                listingModelList = new ArrayList();

                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                if(jsonArray.length() != 0){
                                    for(int j = 0; j < jsonArray.length(); j++){
                                        //if success create session user
                                        JSONObject jsonData = jsonArray.getJSONObject(j);

                                        StokModel notificationModel1 = new StokModel(
                                                jsonData.getInt("id_barang"),
                                                jsonData.getString("nama"),
                                                jsonData.getString("deskripsi"),
                                                jsonData.getInt("harga"),
                                                jsonData.getInt("stok"),
                                                jsonData.getString("foto"),
                                                jsonData.getString("tanggal_upload"),
                                                jsonData.getString("tanggal_update"),
                                                jsonData.getString("tanggal_listing"),
                                                jsonData.getInt("id_subkategori"),
                                                jsonData.getString("nama_subkategori"),
                                                jsonData.getString("ecommerce"),
                                                jsonData.getString("status"),
                                                jsonData.getString("foto_tambahan"),
                                                jsonData.getInt("berat"),
                                                jsonData.getString("status_unlisting"),
                                                jsonData.getString("volume"),
                                                jsonData.getString("bahan"),
                                                jsonData.getString("merk")
                                        );
                                        listingModelList.add(notificationModel1);
                                    }
                                    mAdapter.updateList(listingModelList);
                                }else{
                                    Toast.makeText(getActivity(), "Barang yang anda cari tidak ditemukan", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                            progressDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("id",sessionManager.getIdUser());
                map.put("searching",searching);
                map.put("filtering",filtering);
                map.put("sorting",sorting);
                map.put("index", "0");
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
