package com.sakoo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import java.lang.reflect.Field;

public class NewHomeActivity extends AppCompatActivity{

    boolean doubleBackToExitPressedOnce = false;
    private DBHelper dbHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_home);

        int fragmentId = getIntent().getIntExtra("FRAGMENT_ID", 0);

        BottomNavigationView bottomNavigationView = (BottomNavigationView)
                findViewById(R.id.navigation);
        BottomNavigationViewHelper.disableShiftMode(bottomNavigationView);

        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.action_item1:
                                selectedFragment = NewTransactionFragment.newInstance();
                                break;
                            case R.id.action_item2:
                                selectedFragment = NewSaldoFragment.newInstance();
                                break;
                            case R.id.action_item3:
                                selectedFragment = HomeFragment.newInstance();
                                break;
                            case R.id.action_item4:
                                selectedFragment = NewBarangFragment.newInstance();
                                break;
                            case R.id.action_item5:
                                selectedFragment = SettingFragment.newInstance();
                                break;
                            default:
                                break;
                        }
                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_layout, selectedFragment);
                        transaction.commit();
                        return true;
                    }
                });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (fragmentId){
            case 0 :
                //Manually displaying the first fragment - one time only
                transaction.replace(R.id.frame_layout, HomeFragment.newInstance());
                transaction.commit();

                //Used to select an item programmatically
                bottomNavigationView.getMenu().getItem(0).setChecked(true);
                break;
            case 1 :
                //Manually displaying the first fragment - one time only
                transaction.replace(R.id.frame_layout, NewBarangFragment.newInstance());
                transaction.commit();

                //Used to select an item programmatically
                bottomNavigationView.getMenu().getItem(1).setChecked(true);
                break;
            case 2 :
                //Manually displaying the first fragment - one time only
                transaction.replace(R.id.frame_layout, NewTransactionFragment.newInstance());
                transaction.commit();

                //Used to select an item programmatically
                bottomNavigationView.getMenu().getItem(2).setChecked(true);
                break;
            case 3 :
                //Manually displaying the first fragment - one time only
                transaction.replace(R.id.frame_layout, NewSaldoFragment.newInstance());
                transaction.commit();

                //Used to select an item programmatically
                bottomNavigationView.getMenu().getItem(3).setChecked(true);
                break;
            case 4 :
                //Manually displaying the first fragment - one time only
                transaction.replace(R.id.frame_layout, SettingFragment.newInstance());
                transaction.commit();

                //Used to select an item programmatically
                bottomNavigationView.getMenu().getItem(4).setChecked(true);
                break;
            default:
                break;
        }

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Tekan 1 kali lagi untuk keluar aplikasi", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }

    public static class BottomNavigationViewHelper {
        @SuppressLint("RestrictedApi")
        public static void disableShiftMode(BottomNavigationView view) {
            BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
            try {
                Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
                shiftingMode.setAccessible(true);
                shiftingMode.setBoolean(menuView, false);
                shiftingMode.setAccessible(false);
                for (int i = 0; i < menuView.getChildCount(); i++) {
                    BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                    //noinspection RestrictedApi
                    item.setShiftingMode(false);
                    // set once again checked value, so view will be updated
                    //noinspection RestrictedApi
                    item.setChecked(item.getItemData().isChecked());

                }
            } catch (NoSuchFieldException e) {
                Log.e("BNVHelper", "Unable to get shift mode field", e);
            } catch (IllegalAccessException e) {
                Log.e("BNVHelper", "Unable to change value of shift mode", e);
            }
        }
    }

}
