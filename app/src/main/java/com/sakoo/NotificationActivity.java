package com.sakoo;

import android.app.ProgressDialog;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sakoo.databases.NotifHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NotificationActivity extends AppCompatActivity {

    private List<NotificationModel> notificationModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private NotificationAdapter mAdapter;

    private NotifHelper notifHelper;

    private DBHelper dbHelper;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private ImageView notifikasiKosong;
    private TextView textNotifikasiKosong;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        dbHelper = new DBHelper(this);
        sessionManager = new SessionManager(this);
        baseUtility = new BaseUtility(this);
        notifHelper = new NotifHelper(this);

        notifikasiKosong = (ImageView) findViewById(R.id.notifikasiKosong);
        textNotifikasiKosong = (TextView) findViewById(R.id.textNotifikasiKosong);

        // RecycleView
        recyclerView = (RecyclerView)findViewById(R.id.listNotivication);
        notificationModelList = new ArrayList<>();
        mAdapter = new NotificationAdapter(NotificationActivity.this,notificationModelList);
        mLayoutManager = new LinearLayoutManager(NotificationActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

//        Cursor dataNotif = dbHelper.getNotifData();
//        dataNotif.moveToFirst();
//        while(dataNotif.isAfterLast() == false){
//            NotificationModel notificationModel1 = new NotificationModel(dataNotif.getString(dataNotif.getColumnIndex("date")), dataNotif.getString(dataNotif.getColumnIndex("body")), dataNotif.getInt(dataNotif.getColumnIndex("id")), dataNotif.getString(dataNotif.getColumnIndex("type")), dataNotif.getInt(dataNotif.getColumnIndex("typeId")), dataNotif.getString(dataNotif.getColumnIndex("title")));
//            notificationModelList.add(notificationModel1);
//            dataNotif.moveToNext();
//        }

        //getNotifikasi();
        getNotifikasiDB();

        //recyclerView.setAdapter(mAdapter);
    }

    private void getNotifikasi(){
        progressDialog = ProgressDialog.show(this, "Notification", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/fcm/getNotification/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                //if success create session user
                                JSONArray jsonArray = new JSONArray(data);
                                for(int z = 0; z < jsonArray.length(); z++){
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(z);
                                    NotificationModel notificationModel1 = new NotificationModel(jsonObject1.getString("date"), jsonObject1.getString("body"), jsonObject1.getInt("id"), jsonObject1.getString("type"), jsonObject1.getInt("type_id"), jsonObject1.getString("title"));
                                    notificationModelList.add(notificationModel1);
                                }
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                recyclerView.setAdapter(mAdapter);
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(NotificationActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void getNotifikasiDB() {
        notifHelper.open();
        notificationModelList = notifHelper.getAll();

        if(notificationModelList.size() == 0){
            notifikasiKosong.setVisibility(View.VISIBLE);
            textNotifikasiKosong.setVisibility(View.VISIBLE);
        }else{
            notifikasiKosong.setVisibility(View.INVISIBLE);
            textNotifikasiKosong.setVisibility(View.INVISIBLE);
        }

        Log.d(NotificationFragment.class.getSimpleName(), "getNotifikasiDB: " + notificationModelList.size());
        mAdapter.setNotificationModelList(notificationModelList);
        recyclerView.setAdapter(mAdapter);
        notifHelper.close();
    }

    public void updateNotifikasiDB() {
        notifHelper.open();
        notificationModelList = notifHelper.getAll();

        if(notificationModelList.size() == 0){
            notifikasiKosong.setVisibility(View.VISIBLE);
            textNotifikasiKosong.setVisibility(View.VISIBLE);
        }else{
            notifikasiKosong.setVisibility(View.INVISIBLE);
            textNotifikasiKosong.setVisibility(View.INVISIBLE);
        }

        Log.d(NotificationFragment.class.getSimpleName(), "getNotifikasiDB: " + notificationModelList.size());
        mAdapter.updateList(notificationModelList);
        notifHelper.close();
    }

    @Override
    protected void onResume() {
        updateNotifikasiDB();
        super.onResume();
    }
}
