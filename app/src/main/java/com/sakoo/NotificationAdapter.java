package com.sakoo;


import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.sakoo.databases.NotifHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Objects;

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.MyViewHolder> {

    private List<NotificationModel> notificationModelList;
    private Context context;

    public NotificationAdapter(Context context, List<NotificationModel> _notificationModelList) {
        this.notificationModelList = _notificationModelList;
        this.context = context;
    }

    public void setNotificationModelList(List<NotificationModel> notificationModelList) {
        this.notificationModelList = notificationModelList;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView myDate, myText, myTitle;
        public ImageView gambar;
        public CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.card_view);
            myDate = (TextView) view.findViewById(R.id.notificationDate);
            myText = (TextView) view.findViewById(R.id.notificationText);
            myTitle = (TextView) view.findViewById(R.id.title);
            gambar = (ImageView) view.findViewById(R.id.image_notification);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final NotificationModel documentModel = notificationModelList.get(position);
        final NotifHelper notifHelper = new NotifHelper(context);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String theDate = documentModel.getMyDate();
        Date date = null;
        try {
            date = dateFormat.parse(theDate);
            dateFormat.applyPattern("dd/MM/yyyy HH:mm");
            holder.myDate.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.myText.setText(documentModel.getMyText());
        holder.myTitle.setText(documentModel.getMyTitle());

        if(documentModel.getStatus() == 0){
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.cardActive));
        } else {
            holder.cardView.setCardBackgroundColor(context.getResources().getColor(R.color.cardview_light_background));
        }

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notifHelper.open();
                notifHelper.changeStatus(documentModel.getMyId(), 1);
                notifHelper.close();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(documentModel.getType(), "transaksi")){
                        Intent intent = new Intent(context, TransactionActivity.class);
                        intent.putExtra("idTransaksi", documentModel.getTypeId());
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "barang")){
                        Intent intent = new Intent(context, NewHomeActivity.class);
                        intent.putExtra("FRAGMENT_ID", 1);
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "saldo")){
                        Intent intent = new Intent(context, NewHomeActivity.class);
                        intent.putExtra("FRAGMENT_ID", 3);
                        context.startActivity(intent);
                    }else if(Objects.equals(documentModel.getType(), "detail_transaksi")){
                        Intent intent = new Intent(context, TransactionActivity.class);
                        intent.putExtra("idTransaksi", documentModel.getTypeId());
                        context.startActivity(intent);
                    }
                }



//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
            }
        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return notificationModelList.size();
    }

    public void updateList(List<NotificationModel> list){
        notificationModelList = list;
        notifyDataSetChanged();
    }
}
