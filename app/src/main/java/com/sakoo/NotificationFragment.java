package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.Image;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sakoo.databases.NotifHelper;
import com.sakoo.models.Notif;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class NotificationFragment extends Fragment {

    private List<NotificationModel> notificationModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private NotificationAdapter mAdapter;

    private NotifHelper notifHelper;

    private ProgressDialog progressDialog;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;

    private CardView card_upload, card_barang, card_transaksi, card_saldo;

    private TextView textInfo;
    private ImageView gambarInfo;

    private Toolbar toolbar;

    private HashMap<String, String> user;

    public static NotificationFragment newInstance() {
        NotificationFragment fragment = new NotificationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        baseUtility = new BaseUtility(getActivity());
        notifHelper = new NotifHelper(getActivity());
        user = sessionManager.getUserDetails();

        //getNotifikasi();
    }

    @Override
    public void onResume() {
        super.onResume();
        getNotifikasiDB();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        //card_upload = (CardView)view.findViewById(R.id.card_upload);
        card_barang = (CardView)view.findViewById(R.id.card_barang);
        card_transaksi = (CardView)view.findViewById(R.id.card_transaksi);
        card_saldo = (CardView)view.findViewById(R.id.card_saldo);


//        card_upload.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                baseUtility.myIntent(BarangActivity.class);
//            }
//        });

        card_barang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                        Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else{
                        baseUtility.myIntent(StockActivity.class);
                    }
                }
            }
        });

        card_transaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                        Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else{
                        baseUtility.myIntent(MyTransactionActivity.class);
                    }
                }
            }
        });

        card_saldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
                        Toast.makeText(getActivity(), "Anda harus melengkapi profile terlebih dahulu", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(getActivity(), EditProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }else{
                        baseUtility.myIntent(SaldoActivity.class);
                    }
                }
            }
        });

        //textInfo = (TextView) view.findViewById(R.id.textInfo);
        //gambarInfo = (ImageView) view.findViewById(R.id.gambarInfo);

        // RecycleView
        recyclerView = (RecyclerView)view.findViewById(R.id.listNotivication);
        notificationModelList = new ArrayList<>();
        mAdapter = new NotificationAdapter(getActivity(),notificationModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getNotifikasiDB();

        return view;
    }

    private void getNotifikasi(){
        progressDialog = ProgressDialog.show(getActivity(), "Notification", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/fcm/getNotification/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                //if success create session user
                                JSONArray jsonArray = new JSONArray(data);
                                    for(int z = 0; z < jsonArray.length(); z++){
                                        JSONObject jsonObject1 = jsonArray.getJSONObject(z);
                                        NotificationModel notificationModel1 = new NotificationModel(
                                                jsonObject1.getString("date"),
                                                jsonObject1.getString("body"),
                                                jsonObject1.getInt("id"),
                                                jsonObject1.getString("type"),
                                                jsonObject1.getInt("type_id"),
                                                jsonObject1.getString("title")
                                        );
                                        notificationModelList.add(notificationModel1);

                                        notifHelper.open();
                                        notificationModel1.setStatus(0);
                                        notifHelper.beginTransaction();
                                        if(!notifHelper.isExist(jsonObject1.getInt("id"))){
                                            notifHelper.insertTransaction(notificationModel1);
                                            notifHelper.setTransactionSuccess();
                                        }
                                        notifHelper.endTransaction();
                                        notifHelper.close();
                                    }
                                    recyclerView.setAdapter(mAdapter);

//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    public void getNotifikasiDB() {
        notifHelper.open();
        notificationModelList = notifHelper.getAll();
        Log.d(NotificationFragment.class.getSimpleName(), "getNotifikasiDB: " + notificationModelList.size());
        mAdapter.setNotificationModelList(notificationModelList);
        recyclerView.setAdapter(mAdapter);
        notifHelper.close();
    }




}
