package com.sakoo;

public class NotificationModel {

    private String myDate;
    private String myText;
    private int myId;
    private String type;
    private int typeId;
    private String myTitle;
    private int status;

    public NotificationModel(){

    }

    public NotificationModel(String s1, String s2, int i1, String s3, int i2, String s4){
        this.myDate = s1;
        this.myText = s2;
        this.myId = i1;
        this.type = s3;
        this.typeId = i2;
        this.myTitle = s4;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMyDate() {
        return myDate;
    }

    public String getMyText() {
        return myText;
    }

    public int getMyId() {
        return myId;
    }

    public String getType() {
        return type;
    }

    public int getTypeId() {
        return typeId;
    }

    public String getMyTitle() {
        return myTitle;
    }

    public void setMyDate(String myDate) {
        this.myDate = myDate;
    }

    public void setMyText(String myText) {
        this.myText = myText;
    }

    public void setMyId(int myId) {
        this.myId = myId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public void setMyTitle(String myTitle) {
        this.myTitle = myTitle;
    }
}
