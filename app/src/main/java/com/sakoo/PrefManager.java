package com.sakoo;

import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editorxx;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "sakoo-welcome";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_FIRST_TIME_UPLOAD = "IsFirstTimeUpload";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editorxx = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editorxx.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editorxx.commit();
    }

    public void setFirstTimeUpload(boolean isFirstTime) {
        editorxx.putBoolean(IS_FIRST_TIME_UPLOAD, isFirstTime);
        editorxx.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
    public boolean isFirstTimeUpload() {
        return pref.getBoolean(IS_FIRST_TIME_UPLOAD, true);
    }
}
