package com.sakoo;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProsesSaldoFragment extends Fragment {

    private LinearLayout linearLayout1;
    private List<ProsesSaldoModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProsesSaldoAdapter mAdapter;

    private ProgressDialog progressDialog;
    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private int count = 0;

    private ImageView saldoKosong;
    private TextView textSaldoKosong;

    public ProsesSaldoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseUtility = new BaseUtility(getActivity());
        sessionManager = new SessionManager(getActivity());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saldo_proses, container, false);

        saldoKosong = (ImageView) view.findViewById(R.id.saldoKosong);
        textSaldoKosong = (TextView) view.findViewById(R.id.textSaldoKosong);

        // RecycleView
        recyclerView = (RecyclerView)view.findViewById(R.id.listProsesSaldo);
        listingModelList = new ArrayList<>();
        mAdapter = new ProsesSaldoAdapter(getActivity(),listingModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(sessionManager.isUserLoggedIn()){
            saldoFunction();
        }else{
            saldoKosong.setVisibility(View.VISIBLE);
            textSaldoKosong.setVisibility(View.VISIBLE);
        }


        return view;
    }

    private void saldoFunction(){
        // TODO :

        progressDialog = ProgressDialog.show(getActivity(), "Saldo", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/transaction/getMyProsesSaldo/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);
                                    int id = jsonData.getInt("id_transaksi_seller");
                                    String noTrx = jsonData.getString("no_trx");
                                    String jumlahUang = jsonData.getString("jumlah_uang");
                                    String tanggalRequest = jsonData.getString("waktu_request");

                                    count = count + 1;

                                    ProsesSaldoModel notificationModel1 = new ProsesSaldoModel(id, jumlahUang, noTrx, tanggalRequest, String.valueOf(count));
                                    listingModelList.add(notificationModel1);
                                }

                                if(listingModelList.size() == 0){
                                    saldoKosong.setVisibility(View.VISIBLE);
                                    textSaldoKosong.setVisibility(View.VISIBLE);
                                }else{
                                    saldoKosong.setVisibility(View.INVISIBLE);
                                    textSaldoKosong.setVisibility(View.INVISIBLE);
                                }

                                recyclerView.setAdapter(mAdapter);

//                                //if success navigate to MainActivity


                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
