package com.sakoo;

public class ProsesSaldoModel {

    private int id;
    private String jumlahDana, noTrx, tanggalRequest, count;

    public int getId() {
        return id;
    }

    public String getJumlahDana() {
        return jumlahDana;
    }

    public String getNoTrx() {
        return noTrx;
    }

    public String getTanggalRequest() {
        return tanggalRequest;
    }

    public String getCount() {
        return count;
    }

    public ProsesSaldoModel(int i1, String s1, String s2, String s3, String s4){
        this.id = i1;
        this.jumlahDana = s1;
        this.noTrx = s2;
        this.tanggalRequest = s3;
        this.count = s4;

    }

}
