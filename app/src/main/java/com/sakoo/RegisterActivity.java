package com.sakoo;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private Button registerButton;
    private TextView loginTouch, termcondition;
    private ImageView showPassword;

    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private EditText inputNama, inputToko, inputKontak, inputEmail, inputPassword, inputKonfirmasi;
    private String nama, namaToko, kontak, email, password;

    private ProgressDialog progressDialog;
    private PrefManager prefManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        //Class
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
        prefManager = new PrefManager(this);

        //DEFINITION AND EVENT COMPONENT
        definitionComponent();
        eventComponent();


    }

    private void definitionComponent(){
        registerButton = (Button) findViewById(R.id.registerButton);
        //loginTouch = (TextView) findViewById(R.id.loginTouchable);
        termcondition = (TextView) findViewById(R.id.termcondition);

        inputNama = (EditText)findViewById(R.id.inputNama);
        inputToko = (EditText)findViewById(R.id.inputToko);
        inputKontak = (EditText)findViewById(R.id.inputKontak);
        inputEmail = (EditText)findViewById(R.id.inputEmail);
        inputPassword = (EditText)findViewById(R.id.inputPassword);
        inputKonfirmasi = (EditText)findViewById(R.id.inputPasswordKonfirmasi);

    }

    private void eventComponent(){
        registerButton.setOnClickListener(this);
        //loginTouch.setOnClickListener(this);
        termcondition.setOnClickListener(this);

//        showPassword.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch ( event.getAction() ) {
//
//                    case MotionEvent.ACTION_UP:
//                        inputPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                        break;
//
//                    case MotionEvent.ACTION_DOWN:
//                        inputPassword.setInputType(InputType.TYPE_CLASS_TEXT);
//                        break;
//
//                }
//                return true;
//            }
//        });

//        showKonfirm.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                switch ( event.getAction() ) {
//
//                    case MotionEvent.ACTION_UP:
//                        inputConfirmationPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
//                        break;
//
//                    case MotionEvent.ACTION_DOWN:
//                        inputConfirmationPassword.setInputType(InputType.TYPE_CLASS_TEXT);
//                        break;
//
//                }
//                return true;
//            }
//        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.registerButton :
                registerFunction();
                //finish();
                break;
            case R.id.termcondition :
                lihatSyarat();
                break;
            default:
                break;
        }
    }

    private void registerFunction(){
        registerButton.setEnabled(false);
        // TODO :
        // check username, email, password and rePassword value cant null
        if(inputNama.getText().length() == 0 || inputToko.getText().length() == 0 || inputKontak.getText().length() == 0 || inputEmail.getText().length() == 0 || inputPassword.getText().length() == 0 || inputKonfirmasi.getText().length() == 0){
            baseUtility.myToast("Form tidak boleh ada yang dikosongkan");
            registerButton.setEnabled(true);
        }else{
            // check input password and rePassword
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(inputKonfirmasi.getText().toString(), inputPassword.getText().toString())){
                    baseUtility.myToast("Password yang anda masukan tidak cocok");
                    registerButton.setEnabled(true);
                }else{
                    nama = inputNama.getText().toString();
                    namaToko = inputToko.getText().toString();
                    kontak = inputKontak.getText().toString();
                    email = inputEmail.getText().toString();
                    password = inputPassword.getText().toString();

                    //call API login and check response
                    progressDialog = ProgressDialog.show(this, "Register", "Please wait ...", true );

                    //Log.e("lihat", apiConfig.userEndpoint);

                    StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/register",
                            new Response.Listener<String>() {
                                @Override
                                public void onResponse(String response) {
                                    Log.e("TES", response.toString());
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);
                                        int code = jsonObject.getInt("code");
                                        String message = jsonObject.getString("message");

                                        if(code == 200){
                                            String data = jsonObject.getString("data");
                                            progressDialog.dismiss();
                                            sessionManager.kontakUserSementara(kontak);
                                            prefManager.setFirstTimeLaunch(false);
                                            Intent myIntent = new Intent(RegisterActivity.this, SukseRegisterActivity.class);
                                            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(myIntent);
                                        }else{
                                            progressDialog.dismiss();
                                            Toast.makeText(RegisterActivity.this, message, Toast.LENGTH_SHORT).show();
                                        }
                                        registerButton.setEnabled(true);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    progressDialog.dismiss();
                                    Log.e("erornya", error.toString());
                                    baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                                    registerButton.setEnabled(true);
                                }
                            }){
                        @Override
                        protected Map<String, String> getParams() throws AuthFailureError {
                            Map<String,String> map = new HashMap<String,String>();
                            map.put("username",nama);
                            map.put("nama_toko",namaToko);
                            map.put("kontak",kontak);
                            map.put("email",email);
                            map.put("password",password);
                            return map;
                        }

                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> headers = new HashMap<String, String>();
                            //headers.put("Content-Type", "application/x-www-form-urlencoded");
                            headers.put("secretkey", "JackTheRipper");
                            return headers;
                        }
                    };

                    RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
                    requestQueue.add(stringRequest);
                }
            }
        }
    }

    private void lihatSyarat(){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(RegisterActivity.this);
        alertDialog.setMessage("Syarat dan Ketentuan");

        LayoutInflater inflater = LayoutInflater.from(RegisterActivity.this);
        final View dialogView = inflater.inflate(R.layout.dialog_syarat_ketentuan, null);

        alertDialog.setView(dialogView);

        alertDialog.setPositiveButton("Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
}
