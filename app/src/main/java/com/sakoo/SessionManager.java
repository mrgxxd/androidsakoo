package com.sakoo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import java.util.HashMap;

public class SessionManager {

    // Shared Preferences reference
    SharedPreferences pref, pref2, pref3, pref4, pref5;

    // Editor reference for Shared preferences
    SharedPreferences.Editor editor;

    SharedPreferences.Editor editor1, editor2, editoralamat, editorEmail;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREFER_NAME = "SaKoO";
    private static final String PREFER_NAME2 = "Barang";
    private static final String PREFER_NAME3 = "fcm_sakoo";
    private static final String PREFER_NAME4 = "lokasi_sakoo";
    private static final String PREFER_NAME5 = "email_sakoo";

    // User name (make variable public to access from outside)
    public static final String KEY_IS_USER_LOGIN = "isUserLogin";
    public static final String KEY_ID = "id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAME = "nama";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_CONTACT = "no_hp";
    public static final String KEY_ADDRESS = "alamat";
    public static final String KEY_KOTA = "kota";
    public static final String KEY_PICTURE = "foto";
    public static final String KEY_REG_ID = "reg_id";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_RANK = "rank";
    public static final String KEY_TRANSACTION = "transaction";
    public static final String KEY_LISTING= "listing";
    public static final String KEY_BANK= "bank";
    public static final String KEY_REKENING= "rekening";
    public static final String KEY_NAMA_REKENING= "namaRekening";
    public static final String KEY_LOKASI_MARKUP= "lokasiMarkup";
    public static final String KEY_PASSWORD= "password";
    public static final String KEY_JUMLAH_BARANG= "jumlahBarang";
    public static final String KEY_JUMLAH_TRANSAKSI= "jumlahTransaksi";
    public static final String KEY_JUMLAH_SALDO= "jumlahSaldo";


    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        pref2 = _context.getSharedPreferences(PREFER_NAME2, PRIVATE_MODE);
        pref3 = _context.getSharedPreferences(PREFER_NAME3, PRIVATE_MODE);
        pref4 = _context.getSharedPreferences(PREFER_NAME4, PRIVATE_MODE);
        pref5 = _context.getSharedPreferences(PREFER_NAME5, PRIVATE_MODE);
        editor = pref.edit();
        editor1 = pref2.edit();
        editor2 = pref3.edit();
        editoralamat = pref4.edit();
        editorEmail = pref5.edit();

        //definisi awal
    }

    //Create new token
    public void createFcmSession(String token){
        editor2.putString("newToken", token);

        editor2.commit();
    }

    public void createUsernameSession(String s1){
        editor2.putString("userLogin", s1);

        editor2.commit();
    }

    public String getNewToken(){
        return pref3.getString("newToken", null);
    }
    public String getUserLogin(){
        return pref3.getString("userLogin", null);
    }

    //Create login session
    public void createUserLoginSession(String id,
                                       String nama,
                                       String email,
                                       String username,
                                       String no_hp,
                                       String alamat,
                                       String foto,
                                       String reg_id,
                                       String token,
                                       String rank,
                                       String transaction,
                                       String listing,
                                       String bank,
                                       String rekening,
                                       String namaRekening,
                                       String lokasiMarkup){
        editor.putBoolean(KEY_IS_USER_LOGIN, true);
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, nama);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_CONTACT, no_hp);
        editor.putString(KEY_ADDRESS, alamat);
        editor.putString(KEY_PICTURE, foto);
        editor.putString(KEY_REG_ID, reg_id);
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_RANK, rank);
        editor.putString(KEY_TRANSACTION, transaction);
        editor.putString(KEY_LISTING, listing);
        editor.putString(KEY_BANK, bank);
        editor.putString(KEY_REKENING, rekening);
        editor.putString(KEY_NAMA_REKENING, namaRekening);
        editor.putString(KEY_LOKASI_MARKUP, lokasiMarkup);

        editor.commit();
    }

    public void createHomeStatus(String jumlahBarang, String jumlahTransaksi, String jumlahSaldo){
        editor.putString(KEY_JUMLAH_BARANG, jumlahBarang);
        editor.putString(KEY_JUMLAH_TRANSAKSI, jumlahTransaksi);
        editor.putString(KEY_JUMLAH_SALDO, jumlahSaldo);

        editor.commit();
    }

    public String getKeyJumlahBarang(){
        return pref.getString(KEY_JUMLAH_BARANG,null);
    }
    public String getKeyJumlahTransaksi(){
        return pref.getString(KEY_JUMLAH_TRANSAKSI,null);
    }
    public String getKeyJumlahSaldo(){
        return pref.getString(KEY_JUMLAH_SALDO,null);
    }


    public void alamatSementara(String alamat){
        editoralamat.putString("alamatsementara", alamat);
        editoralamat.commit();
    }
    public void usernameSementara(String alamat){
        editoralamat.putString("usernameSementara", alamat);
        editoralamat.commit();
    }
    public void emailSementara(String alamat){
        editoralamat.putString("emailSementara", alamat);
        editoralamat.commit();
    }
    public void namaSementara(String alamat){
        editoralamat.putString("namaSementara", alamat);
        editoralamat.commit();
    }
    public void kontakSementara(String alamat){
        editoralamat.putString("kontakSementara", alamat);
        editoralamat.commit();
    }
    public void rekSementara(String alamat){
        editoralamat.putString("rekSementara", alamat);
        editoralamat.commit();
    }
    public void lokasiSementara(String alamat){
        editoralamat.putString("lokasiSementara", alamat);
        editoralamat.commit();
    }
    public void namaRekSementara(String alamat){
        editoralamat.putString("namaRekSementara", alamat);
        editoralamat.commit();
    }
    public void bankSementara(String alamat){
        editoralamat.putString("bankSementara", alamat);
        editoralamat.commit();
    }
    public void gambarSementara(String alamat){
        editoralamat.putString("gambarSementara", alamat);
        editoralamat.commit();
    }
    public void urlSementara(String alamat){
        editoralamat.putString("urlSementara", alamat);
        editoralamat.commit();
    }

    public String getAlamatSementara(){
        return pref4.getString("alamatsementara","");
    }
    public String getUsernameSementara(){
        return pref4.getString("usernameSementara","");
    }
    public String getEmailSementara(){
        return pref4.getString("emailSementara","");
    }
    public String getNamaSementara(){
        return pref4.getString("namaSementara","");
    }
    public String getKontakSementara(){
        return pref4.getString("kontakSementara","");
    }
    public String getRekSementara(){
        return pref4.getString("rekSementara","");
    }
    public String getNamaRekSementara(){
        return pref4.getString("namaRekSementara","");
    }
    public String getBankSementara(){
        return pref4.getString("bankSementara","");
    }
    public String getLokasiSementara(){
        return pref4.getString("lokasiSementara","");
    }
    public String getGambarSementara(){
        return pref4.getString("gambarSementara","");
    }
    public String getUrlSementara(){
        return pref4.getString("urlSementara","");
    }

    public void hapusAlSementara(){
        editoralamat.clear();
        editoralamat.commit();
    }

    public void addBarangAwal(){
        editor1.putString("foto1", "");
        editor1.putString("foto2", "");
        editor1.putString("foto3", "");
        editor1.putString("foto4", "");
        editor1.putString("foto5", "");
        editor1.putString("namaBarang", "");
        editor1.putString("subkategori", "");
        editor1.putString("textsubkategori", "");
        editor1.putString("deskripsi", "");
        editor1.putString("harga", "");
        editor1.putString("berat", "");
        editor1.putString("stok", "");
    }

    public void foto1(String foto1){
        editor1.putString("foto1", foto1);
        editor1.commit();
    }
    public String getFoto1(){
        return pref2.getString("foto1",null);
    }
    public void deleteFoto1(){
        editor1.remove("foto1");
        editor1.commit();
    }
    public void foto2(String foto2){
        editor1.putString("foto2", foto2);
        editor1.commit();
    }
    public String getFoto2(){
        return pref2.getString("foto2",null);
    }
    public void deleteFoto2(){
        editor1.remove("foto2");
        editor1.commit();
    }
    public void foto3(String foto3){
        editor1.putString("foto3", foto3);
        editor1.commit();
    }
    public String getFoto3(){
        return pref2.getString("foto3",null);
    }
    public void deleteFoto3(){
        editor1.remove("foto3");
        editor1.commit();
    }
    public void foto4(String foto4){
        editor1.putString("foto4", foto4);
        editor1.commit();
    }
    public String getFoto4(){
        return pref2.getString("foto4",null);
    }
    public void deleteFoto4(){
        editor1.remove("foto4");
        editor1.commit();
    }
    public void foto5(String foto5){
        editor1.putString("foto5", foto5);
        editor1.commit();
    }
    public String getFoto5(){
        return pref2.getString("foto5",null);
    }
    public void deleteFoto5(){
        editor1.remove("foto5");
        editor1.commit();
    }

    public void urlfoto1(String foto1){
        editor1.putString("urlfoto1", foto1);
        editor1.commit();
    }
    public String geturlFoto1(){
        return pref2.getString("urlfoto1",null);
    }
    public void deleteurlFoto1(){
        editor1.remove("urlfoto1");
        editor1.commit();
    }
    public void urlfoto2(String foto2){
        editor1.putString("urlfoto2", foto2);
        editor1.commit();
    }
    public String geturlFoto2(){
        return pref2.getString("urlfoto2",null);
    }
    public void deleteurlFoto2(){
        editor1.remove("urlfoto2");
        editor1.commit();
    }
    public void urlfoto3(String foto3){
        editor1.putString("urlfoto3", foto3);
        editor1.commit();
    }
    public String geturlFoto3(){
        return pref2.getString("urlfoto3",null);
    }
    public void deleteurlFoto3(){
        editor1.remove("urlfoto3");
        editor1.commit();
    }
    public void urlfoto4(String foto4){
        editor1.putString("urlfoto4", foto4);
        editor1.commit();
    }
    public String geturlFoto4(){
        return pref2.getString("urlfoto4",null);
    }
    public void deleteurlFoto4(){
        editor1.remove("urlfoto4");
        editor1.commit();
    }
    public void urlfoto5(String foto5){
        editor1.putString("urlfoto5", foto5);
        editor1.commit();
    }
    public String geturlFoto5(){
        return pref2.getString("urlfoto5",null);
    }
    public void deleteurlFoto5(){
        editor1.remove("foto5");
        editor1.commit();
    }

    public void namaBarang(String nama){
        editor1.putString("namaBarang", nama);
        editor1.commit();
    }
    public String getNamaBarang(){
        return pref2.getString("namaBarang",null);
    }

    ///////////////// NEW SESSION FOR UPLOAD BARANG ///////////////////////////////

    public void panjangBarang(String panjang){
        editor1.putString("panjangBarang", panjang);
        editor1.commit();
    }
    public String getPanjangBarang(){
        return pref2.getString("panjangBarang",null);
    }

    public void lebarBarang(String lebar){
        editor1.putString("lebarBarang", lebar);
        editor1.commit();
    }
    public String getLebarBarang(){
        return pref2.getString("lebarBarang",null);
    }

    public void tinggiBarang(String tinggi){
        editor1.putString("tinggiBarang", tinggi);
        editor1.commit();
    }
    public String getTinggiBarang(){
        return pref2.getString("tinggiBarang",null);
    }

    public void bahanBarang(String bahan){
        editor1.putString("bahanBarang", bahan);
        editor1.commit();
    }
    public String getBahanBarang(){
        return pref2.getString("bahanBarang",null);
    }

    public void idBarang(String id){
        editor1.putString("idnyaBarang", id);
        editor1.commit();
    }
    public String getIdBarang(){
        return pref2.getString("idnyaBarang",null);
    }

    ///////////////// NEW SESSION FOR UPLOAD BARANG ///////////////////////////////


    public void subKategori(String subKategori){
        editor1.putString("subkategori", subKategori);
        editor1.commit();
    }
    public void textSubKategori(String text){
        editor1.putString("textsubkategori", text);
        editor1.commit();
    }
    public String getSubKategori(){
        return pref2.getString("subkategori",null);
    }
    public String getTextSubKategori(){
        return pref2.getString("textsubkategori",null);
    }
    public void deskripsiBarang(String deskripsi){
        editor1.putString("deskripsi", deskripsi);
        editor1.commit();
    }
    public String getDeskripsi(){
        return pref2.getString("deskripsi",null);
    }

    public void hargaBarang(String harga){
        editor1.putString("harga", harga);
        editor1.commit();
    }
    public String getHarga(){
        return pref2.getString("harga",null);
    }

    public void beratBarang(String berat){
        editor1.putString("berat", berat);
        editor1.commit();
    }
    public String getBerat(){
        return pref2.getString("berat",null);
    }

    public void stokBarang(String stok){
        editor1.putString("stok", stok);
        editor1.commit();
    }
    public String getStok(){
        return pref2.getString("stok",null);
    }

    public void merkBarang(String merk){
        editor1.putString("merk", merk);
        editor1.commit();
    }
    public String getMerk(){
        return pref2.getString("merk",null);
    }

    public String getIdUser(){
        return pref.getString("id",null);
    }

    public HashMap<String, String> getUserDetails(){

        //Use hashmap to store user credentials
        HashMap<String, String> user = new HashMap<String, String>();

        user.put(KEY_ID, pref.getString(KEY_ID, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_CONTACT, pref.getString(KEY_CONTACT, null));
        user.put(KEY_ADDRESS, pref.getString(KEY_ADDRESS, null));
        user.put(KEY_PICTURE, pref.getString(KEY_PICTURE, null));
        user.put(KEY_REG_ID, pref.getString(KEY_REG_ID, null));
        user.put(KEY_TOKEN, pref.getString(KEY_TOKEN, null));
        user.put(KEY_RANK, pref.getString(KEY_RANK, null));
        user.put(KEY_TRANSACTION, pref.getString(KEY_TRANSACTION, null));
        user.put(KEY_LISTING, pref.getString(KEY_LISTING, null));
        user.put(KEY_BANK, pref.getString(KEY_BANK, null));
        user.put(KEY_REKENING, pref.getString(KEY_REKENING, null));
        user.put(KEY_NAMA_REKENING, pref.getString(KEY_NAMA_REKENING, null));
        user.put(KEY_LOKASI_MARKUP, pref.getString(KEY_LOKASI_MARKUP, null));
        // return user
        return user;
    }

    public void logoutUser(){

        // Clearing all user data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);

        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        i.putExtra("EXIT", true);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Clear session details
     * */
    public void clearBarang(){

        // Clearing all user data from Shared Preferences
        editor1.clear();
        editor1.commit();
    }

    public void kontakUserSementara(String kontak){
        editorEmail.putString("kontakUserSementara", kontak);
        editorEmail.commit();
    }

    public String getKontakUserSementara(){
        return pref5.getString("kontakUserSementara",null);
    }

    public void clearKontakSementara(){

        // Clearing all user data from Shared Preferences
        editorEmail.clear();
        editorEmail.commit();
    }


    // Check for login
    public boolean isUserLoggedIn(){
        return pref.getBoolean(KEY_IS_USER_LOGIN, false);
    }

}
