package com.sakoo;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.qiscus.sdk.Qiscus;
import com.qiscus.sdk.data.model.QiscusAccount;
import com.sakoo.databases.NotifHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.HttpException;

public class SettingFragment extends Fragment {
    private Context context;

    private static final String DEFAULT_QISCUS_AVATAR ="https://d1edrlpyc25xu0.cloudfront.net/kiwari-prod/image/upload/75r6s_jOHa/1507541871-avatar-mine.png" ;
    private SessionManager sessionManager;
    private BaseUtility baseUtility;
    private ProgressDialog progressDialog;
    private NotifHelper notifHelper;

    private CardView cardRating, cardChangePassword, cardFaq;

    private HashMap<String, String> user;

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        baseUtility = new BaseUtility(getActivity());
        notifHelper = new NotifHelper(getActivity());
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        TextView namaUserProfile = (TextView) view.findViewById(R.id.namaUserProfile);
        TextView emailUserProfile = (TextView) view.findViewById(R.id.emailUserProfile);
        TextView noHPUserProfile = (TextView) view.findViewById(R.id.noHPUserProfile);

        cardRating = (CardView) view.findViewById(R.id.cardRating);
        cardChangePassword = (CardView) view.findViewById(R.id.cardChangePassword);
        cardFaq = (CardView) view.findViewById(R.id.cardShowFaq);

        user = sessionManager.getUserDetails();

        TextView editTextView = (TextView) view.findViewById(R.id.editProfile);
        Button tombolLogout = (Button) view.findViewById(R.id.tombolLogout);
        RelativeLayout tombolHubungi = (RelativeLayout) view.findViewById(R.id.tombolHubungi);

        if(sessionManager.isUserLoggedIn()){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (!Objects.equals(user.get("nama"), "null")) {
                    namaUserProfile.setText(user.get("nama"));
                } else {
                    namaUserProfile.setText("Nama belum diisi");
                }
                if (!Objects.equals(user.get("email"), "null")) {
                    emailUserProfile.setText(user.get("email"));
                } else {
                    emailUserProfile.setText("Email belum diisi");
                }
                if (!Objects.equals(user.get("no_hp"), "null")) {
                    noHPUserProfile.setText(user.get("no_hp"));
                } else {
                    noHPUserProfile.setText("Kontak belum diisi");
                }
            }
            //cardChangePassword.setVisibility(View.GONE);
        }else{
            namaUserProfile.setText("Anda belum Login");
            emailUserProfile.setVisibility(View.GONE);
            noHPUserProfile.setVisibility(View.GONE);
            tombolLogout.setVisibility(View.GONE);
            //cardChangePassword.setVisibility(View.GONE);
            tombolHubungi.setVisibility(View.GONE);
            editTextView.setText("MASUK");
        }

        editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sessionManager.isUserLoggedIn()){
                    Intent intent = new Intent(getContext(), EditProfileActivity.class);
                    //intent.putExtra("status", documentModel.getStatus());
                    getContext().startActivity(intent);
                }else{
                    baseUtility.myIntent(LoginActivity.class);
                }
            }
        });

        cardRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateAplikasi();
            }
        });

        cardChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lupaPasswordFunction();
            }
        });

        cardFaq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FaqActivity.class);
                startActivity(intent);
            }
        });

        tombolLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutFunction();
            }
        });

        tombolHubungi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //lihatKontak();
//                Intent intent = new Intent(getActivity(), ChatActivity.class);
//                startActivity(intent);
                Qiscus.setUser(user.get("email"),user.get("nama"))
                        .withUsername(user.get("nama"))
                        .save(new Qiscus.SetUserListener() {
                            @Override
                            public void onSuccess(QiscusAccount qiscusAccount) {
                                Log.d("TES", "onSuccess: ");
                                Log.d("AVATAR",qiscusAccount.getAvatar());
                                if (qiscusAccount.getAvatar().equals(DEFAULT_QISCUS_AVATAR)) {
                                    Qiscus.updateUser(user.get("nama"), "https://robohash.org/" + user.get("email") + "/bgset_bg2/3.14160?set=set4",
                                            new Qiscus.SetUserListener() {
                                                @Override
                                                public void onSuccess(QiscusAccount qiscusAccount) {
                                                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                                                    startActivity(intent);
                                                }

                                                @Override
                                                public void onError(Throwable throwable) {

                                                }
                                            });
                                }
                                else
                                {
                                    Intent intent = new Intent(getActivity(), ChatActivity.class);
                                    startActivity(intent);
                                }

                            }

                            // @Override
                            // public void onError(Throwable throwable) {
                            //     showProgress(false);
                            //     Log.e(TAG, "onError: ",throwable );
                            // }

                            @Override
                            public void onError(Throwable throwable) {
                                //showProgress(false);
                                if (throwable instanceof HttpException) { //Error response from server
                                    HttpException e = (HttpException) throwable;
                                    try {
                                        String errorMessage = e.response().errorBody().string();
                                        JSONObject json = new JSONObject(errorMessage).getJSONObject("error");
                                        String finalError = json.getString("message");
                                        if (json.has("detailed_messages") ) {
                                            JSONArray detailedMessages = json.getJSONArray("detailed_messages");
                                            finalError = (String) detailedMessages.get(0);
                                        }

                                        Log.e("tess", errorMessage);
                                        //showError(finalError,"Login Error");
                                        Toast.makeText(getActivity(),"Login Error", Toast.LENGTH_SHORT).show();
                                    } catch (IOException e1) {
                                        e1.printStackTrace();
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                } else if (throwable instanceof IOException) { //Error from network
                                    //showError("Can not connect to qiscus server!","Network Error");
                                    Toast.makeText(getActivity(),"Can not connect to qiscus server!", Toast.LENGTH_SHORT).show();
                                } else { //Unknown error
                                    Toast.makeText(getActivity(),"Unexpected error!", Toast.LENGTH_SHORT).show();
                                    //showError("Unexpected error!","Unknown Error");
                                }
                            }
                        });
            }
        });

        return view;
    }

    private void logoutFunction() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle("Log Out");
        alertDialog.setMessage("Apa anda yakin untuk keluar aplikasi ?");


        alertDialog.setPositiveButton("Ya",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        notifHelper.open();
                        notifHelper.logout();
                        sessionManager.logoutUser();
                        dialog.cancel();
                    }
                });

        alertDialog.setNegativeButton("Tidak",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void updateAplikasi() {
        final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void lihatKontak(){
        progressDialog = ProgressDialog.show(getActivity(), "Mencari Kontak", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/getKontak/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONObject jsonData = new JSONObject(data);
                                tampilKontak(jsonData.getString("kontak_admin"));
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void tampilKontak(final String kontak){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setMessage("Hubungi Kami");

        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View dialogView = inflater.inflate(R.layout.dialog_call_me, null);

        CardView cardKontak = (CardView) dialogView.findViewById(R.id.card_view_kontak);
        TextView nomorKontak = (TextView)dialogView.findViewById(R.id.nomorKontak);
        nomorKontak.setText("+"+kontak);

        alertDialog.setView(dialogView);

        alertDialog.setPositiveButton("Tutup",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        cardKontak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri mUri = Uri.parse("https://api.whatsapp.com/send?phone=" + kontak);
                Intent mIntent = new Intent("android.intent.action.VIEW", mUri);
                mIntent.setPackage("com.whatsapp");
                startActivity(mIntent);
            }
        });

        alertDialog.show();
    }

    private void dummyApi(){
        progressDialog = ProgressDialog.show(getActivity(), "Setting", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/dummyApi",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        progressDialog.dismiss();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void lupaPasswordFunction(){
        // TODO :
        //email = inputEmail.getText().toString();
        //call API login and check response
        progressDialog = ProgressDialog.show(getActivity(), "Permintaan Ubah Password", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/newLupapassword",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");

                            if(code == 200){
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Permintaan berhasil, silahkan cek sms yang masuk ke dalam handphone anda", Toast.LENGTH_LONG).show();
                                sessionManager.logoutUser();
                                getActivity().finish();
//                                    baseUtility.myToast("Permintaan berhasil, silahkan cek sms yang masuk ke dalam handphone anda");
//                                    baseUtility.myIntent(LoginActivity.class);
//                                    finish();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), "Gagal merubah password, silahkan hubungi admin", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("no_hp",user.get("no_hp"));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }
}
