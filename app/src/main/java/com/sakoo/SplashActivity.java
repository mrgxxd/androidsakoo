package com.sakoo;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sakoo.databases.NotifHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;


public class SplashActivity extends AppCompatActivity {

    PackageInfo packageInfo;
    String versionName, appPackageName;
    int versionCode;


    protected int splashTime = 1500;
    Timer timer;

    private SessionManager sessionManager;
    private PrefManager prefManager;
    private BaseUtility baseUtility;

    private HashMap<String, String> user;

    private int typeId;
    private  String title, body, type;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);
        prefManager = new PrefManager(this);
        user = sessionManager.getUserDetails();
        sessionManager.clearBarang();
        NotifHelper notifHelper = new NotifHelper(this);

        timer = new Timer();

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                checkUserLogin();
            }
        }, splashTime);
    }

    private void checkUserLogin() {
        if (sessionManager.isUserLoggedIn()) {
            checkProfile();
        }else {
            if(prefManager.isFirstTimeLaunch()){
                Intent intent = new Intent(SplashActivity.this, WelcomeActivity.class);
                startActivity(intent);
                finish();
            }else{
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }

    private void checkProfile(){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            if(user.get("id").isEmpty() || user.get("nama").isEmpty() || user.get("email").isEmpty() || user.get("username").isEmpty() || user.get("no_hp").isEmpty() || user.get("alamat").isEmpty() || user.get("reg_id").isEmpty() || user.get("rekening").isEmpty() || Objects.equals(user.get("bank"), "0") || user.get("token").isEmpty()){
//                Intent intent = new Intent(SplashActivity.this, EditProfileActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//                finish();
//                startActivity(intent);
//            }else{
//                checkVersion();
//            }
//        }
        checkVersion();
    }

    private void checkVersion(){
        try {
            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
            appPackageName = getPackageName();
            Log.e("Name",appPackageName);
            Log.e("code", String.valueOf(versionCode));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/getVersion/"+sessionManager.getIdUser(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("RESPONSE", response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int code = jsonObject.getInt("code");
                    String message = jsonObject.getString("message");
                    if(code == 200) {
                        String data = jsonObject.getString("data");
                        JSONObject jsonData = new JSONObject(data);
                        String versionData = jsonData.getString("version");
                        String userData = jsonData.getString("user");

                        String jumlahBarang = jsonData.getString("jumlahBarang");
                        String jumlahTransaksi = jsonData.getString("jumlahTransaksi");
                        String jumlahSaldo = jsonData.getString("jumlahSaldo");

                        sessionManager.createHomeStatus(jumlahBarang, jumlahTransaksi, jumlahSaldo);

                        JSONObject versionObject = new JSONObject(versionData);
                        JSONObject userObject = new JSONObject(userData);

                        sessionManager.createUserLoginSession(
                                userObject.getString("id"),
                                userObject.getString("nama"),
                                userObject.getString("email"),
                                userObject.getString("username"),
                                userObject.getString("no_hp"),
                                userObject.getString("alamat"),
                                userObject.getString("foto"),
                                userObject.getString("reg_id"),
                                userObject.getString("token"),
                                userObject.getString("rank"),
                                userObject.getString("jumlah_transaksi"),
                                userObject.getString("jumlah_listing"),
                                userObject.getString("bank"),
                                userObject.getString("rekening"),
                                userObject.getString("nama_rekening"),
                                userObject.getString("lokasi_markup")
                        );

                        if(versionCode < versionObject.getInt("version_code")){
                            updateAplikasi();
                        }else{
                            gotomain();
                        }
                    }else{
                        gotomain();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("GAGAL",error.toString());
                baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                finish();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void updateAplikasi(){
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }

    private void gotomain(){
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.e("FirebaseDataReceiver", "Key: " + key + " Value: " + value);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(key, "key") ){
                        typeId = Integer.parseInt(value.toString());
                    }
                    if(Objects.equals(key, "type")){
                        type = value.toString();
                    }

                    if(Objects.equals(key, "title")){
                        title = value.toString();
                    }

                    if(Objects.equals(key, "body")){
                        body = value.toString();
                    }
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(type, "transaksi")){
                    Intent i = new Intent(SplashActivity.this, TransactionActivity.class);
                    i.putExtra("idTransaksi", typeId);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(i);
                }else if(Objects.equals(type, "barang")){
                    Intent i = new Intent(SplashActivity.this, NewHomeActivity.class);
                    i.putExtra("FRAGMENT_ID", 1);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(i);
                }else if(Objects.equals(type, "saldo")){
                    Intent i = new Intent(SplashActivity.this, NewHomeActivity.class);
                    i.putExtra("FRAGMENT_ID", 3);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(i);

                }else if(Objects.equals(type, "detail_transaksi")){
                    Intent i = new Intent(SplashActivity.this, TransactionActivity.class);
                    i.putExtra("idTransaksi", typeId);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(i);
                }else{
                    Intent i = new Intent(SplashActivity.this, NewHomeActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    finish();
                    startActivity(i);
                }
            }
        }else{
            Intent i = new Intent(SplashActivity.this, NewHomeActivity.class);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            finish();
            startActivity(i);
        }
    }

}
