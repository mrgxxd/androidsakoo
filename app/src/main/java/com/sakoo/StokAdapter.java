package com.sakoo;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.R.attr.fragment;
import static android.R.attr.password;

public class StokAdapter extends RecyclerView.Adapter {

    private List<StokModel> listingModelList;
    private Context context;

    private ProgressDialog progressDialog;

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    private OnLoadMoreListener onLoadMoreListener;
    private boolean isLoading;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;

    public StokAdapter(Context context, List<StokModel> _listingModelList, RecyclerView recyclerView) {
        this.listingModelList = _listingModelList;
        this.context = context;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    Log.e("onload", onLoadMoreListener.toString());
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public ImageView myFoto, settingButton;
        public TextView myBarang, myStok, myHarga, myStatus, myUpdate, myBagikan, myStatUnlisting;

        public MyViewHolder(View view) {
            super(view);
            cardView = (CardView) view.findViewById(R.id.card_view);
            myFoto = (ImageView) view.findViewById(R.id.fotoBarang);
            settingButton = (ImageView) view.findViewById(R.id.settingButton);
            myBarang = (TextView) view.findViewById(R.id.namaBarang);
            myStok = (TextView) view.findViewById(R.id.stokBarang);
            myHarga = (TextView) view.findViewById(R.id.hargaBarang);
            myStatus = (TextView) view.findViewById(R.id.statusBarang);
            myUpdate = (TextView) view.findViewById(R.id.terakhirUpdate);
            myBagikan = (TextView) view.findViewById(R.id.bagikan);
            myStatUnlisting = (TextView) view.findViewById(R.id.statusUnlisting);
        }
    }

    static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        public LoadingViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        if(viewType==VIEW_TYPE_ITEM){
            return new MyViewHolder(inflater.inflate(R.layout.card_stok,parent,false));
        }else{
            return new LoadingViewHolder(inflater.inflate(R.layout.layout_loading_item,parent,false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder  holder, int position) {
        if (holder instanceof MyViewHolder) {
            final StokModel documentModel = listingModelList.get(position);

            String namaBarang = documentModel.getNama();
            if(namaBarang.length() > 20){
                String newNama = namaBarang.substring(0,20);
                ((MyViewHolder)holder).myBarang.setText(newNama.replaceAll("( +)"," ").trim()+" ...");
            }else{
                ((MyViewHolder)holder).myBarang.setText(namaBarang.trim());
            }
            ((MyViewHolder)holder).myStok.setText("Stok ("+documentModel.getStok()+")");

            String temp = documentModel.getFoto();
            temp = temp.replaceAll(" ", "%20");
            Picasso.with(context).load(temp+"?size=medium").placeholder( R.drawable.animation_loading ).into( ((MyViewHolder)holder).myFoto);

            DecimalFormat kursIndonesia = new DecimalFormat("#,###");


            ((MyViewHolder)holder).myHarga.setText("Rp. "+String.valueOf(kursIndonesia.format(Long.valueOf(documentModel.getHarga()))).replace(",","."));

            ((MyViewHolder)holder).myStatus.setText(documentModel.getStatus());
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(documentModel.getStatus(), "Listing")){
                    ((MyViewHolder)holder).myBagikan.setVisibility(View.VISIBLE);
                    ((MyViewHolder)holder).myStatUnlisting.setVisibility(View.INVISIBLE);
                }else{
                    ((MyViewHolder)holder).myBagikan.setVisibility(View.INVISIBLE);
                    ((MyViewHolder)holder).myStatUnlisting.setVisibility(View.VISIBLE);
                    ((MyViewHolder)holder).myStatUnlisting.setText("("+documentModel.getStatusUnlisting()+")");
                }
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(!Objects.equals(documentModel.getTanggalUpdate(), "null")){
                    String sTanggal = dateFunction(documentModel.getTanggalUpdate());
                    ((MyViewHolder)holder).myUpdate.setText(sTanggal);
                }else{
                    String sTanggal = dateFunction(documentModel.getTanggalUpload());
                    ((MyViewHolder)holder).myUpdate.setText(sTanggal);
                }
            }

            ((MyViewHolder)holder).myBagikan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String barangDagangan = "Barang saya dapat dibeli di : ";
                    String myEccomerce = documentModel.getEcommerce();
                    try {
                        JSONArray jsonEccomerce = new JSONArray(myEccomerce);

                        for(int i = 0; i<jsonEccomerce.length(); i++){
                            JSONObject jsonObject = jsonEccomerce.getJSONObject(i);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                if(!Objects.equals(jsonObject.getString("url"), "")){
                                    barangDagangan = barangDagangan + "\n\n" + jsonObject.getString("nama_ecommerce") + " :\n" + jsonObject.getString("url");
                                }
                            }
                        }

                        barangDagangan = barangDagangan + "\n\n" + "Yuk buruan beli :)";

                        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Barang");
                        sharingIntent.putExtra(Intent.EXTRA_TEXT, barangDagangan);
                        context.startActivity(Intent.createChooser(sharingIntent, "Share via"));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

            ((MyViewHolder)holder).cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, DetailBarangInfoActivity.class);
                    intent.putExtra("idBarang",String.valueOf(documentModel.getId()));
                    intent.putExtra("namaBarang", documentModel.getNama());
                    intent.putExtra("merkBarang", documentModel.getMerk());
                    intent.putExtra("idSubkategori", documentModel.getIdSubkategori());
                    intent.putExtra("subkategori", documentModel.getNamaSubkategori());
                    intent.putExtra("deskripsi", documentModel.getDeskripsi());
                    intent.putExtra("beratBarang", documentModel.getBerat());
                    intent.putExtra("stokBarang", documentModel.getStok());
                    intent.putExtra("hargaBarang", documentModel.getHarga());
                    intent.putExtra("bahanBarang", documentModel.getBahan());
                    intent.putExtra("volumeBarang", documentModel.getVolume());
                    intent.putExtra("fotoBarang", documentModel.getFoto());
                    intent.putExtra("fotoTambahan", documentModel.getFotoTambahan());
                    context.startActivity(intent);
                }
            });

            ((MyViewHolder)holder).settingButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    android.app.AlertDialog.Builder b = new android.app.AlertDialog.Builder(context);
                    b.setTitle("Action");
                    String[] types = {"Lihat Detail", "Tambah Stok", "Edit", "Salin", "Hapus"};
                    b.setItems(types, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            dialog.dismiss();
                            switch(which){
                                case 0:
                                    Intent intent = new Intent(context, DetailBarangInfoActivity.class);
                                    intent.putExtra("idBarang",String.valueOf(documentModel.getId()));
                                    intent.putExtra("namaBarang", documentModel.getNama());
                                    intent.putExtra("merkBarang", documentModel.getMerk());
                                    intent.putExtra("idSubkategori", documentModel.getIdSubkategori());
                                    intent.putExtra("subkategori", documentModel.getNamaSubkategori());
                                    intent.putExtra("deskripsi", documentModel.getDeskripsi());
                                    intent.putExtra("beratBarang", documentModel.getBerat());
                                    intent.putExtra("stokBarang", documentModel.getStok());
                                    intent.putExtra("hargaBarang", documentModel.getHarga());
                                    intent.putExtra("bahanBarang", documentModel.getBahan());
                                    intent.putExtra("volumeBarang", documentModel.getVolume());
                                    intent.putExtra("fotoBarang", documentModel.getFoto());
                                    intent.putExtra("fotoTambahan", documentModel.getFotoTambahan());
                                    context.startActivity(intent);
                                    break;
                                case 1:
                                    updateFunction(documentModel.getId(), documentModel.getStok());
                                    break;
                                case 2:
                                    Intent intent3 = new Intent(context, BarangActivity.class);
                                    intent3.putExtra("idBarang",String.valueOf(documentModel.getId()));
                                    intent3.putExtra("namaBarang", documentModel.getNama());
                                    intent3.putExtra("merkBarang", documentModel.getMerk());
                                    intent3.putExtra("idSubkategori", documentModel.getIdSubkategori());
                                    intent3.putExtra("subkategori", documentModel.getNamaSubkategori());
                                    intent3.putExtra("deskripsi", documentModel.getDeskripsi());
                                    intent3.putExtra("beratBarang", documentModel.getBerat());
                                    intent3.putExtra("stokBarang", documentModel.getStok());
                                    intent3.putExtra("hargaBarang", documentModel.getHarga());
                                    intent3.putExtra("bahanBarang", documentModel.getBahan());
                                    intent3.putExtra("volumeBarang", documentModel.getVolume());
                                    intent3.putExtra("fotoBarang", documentModel.getFoto());
                                    intent3.putExtra("fotoTambahan", documentModel.getFotoTambahan());
                                    context.startActivity(intent3);
                                    break;
                                case 3:
                                    Intent intent2 = new Intent(context, BarangActivity.class);
                                    intent2.putExtra("namaBarang", documentModel.getNama());
                                    intent2.putExtra("merkBarang", documentModel.getMerk());
                                    intent2.putExtra("idSubkategori", documentModel.getIdSubkategori());
                                    intent2.putExtra("subkategori", documentModel.getNamaSubkategori());
                                    intent2.putExtra("deskripsi", documentModel.getDeskripsi());
                                    intent2.putExtra("beratBarang", documentModel.getBerat());
                                    intent2.putExtra("stokBarang", documentModel.getStok());
                                    intent2.putExtra("hargaBarang", documentModel.getHarga());
                                    intent2.putExtra("bahanBarang", documentModel.getBahan());
                                    intent2.putExtra("volumeBarang", documentModel.getVolume());
                                    intent2.putExtra("fotoBarang", documentModel.getFoto());
                                    intent2.putExtra("fotoTambahan", documentModel.getFotoTambahan());
                                    context.startActivity(intent2);
                                    break;
                                case 4:
                                    hapusBarang(documentModel.getId());
                                    break;
                            }
                        }

                    });
                    b.show();
                }
            });

//            ((MyViewHolder)holder).updateStok.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    updateFunction(documentModel.getMyId(), documentModel.getMyStok());
//                }
//            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }

    }

    @Override
    public int getItemViewType(int position) {
        return listingModelList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {
        return listingModelList == null ? 0 : listingModelList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    public void updateList(List<StokModel> list){
        listingModelList = list;
        notifyDataSetChanged();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    private void updateFunction(final int id, final int stok){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Edit Stok");
        alertDialog.setMessage("Mohon dapat update stok secara berkala setiap 1 minggu sesuai stok offline");

        final EditText input = new EditText(context);
        input.setText(String.valueOf(stok));
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(context, "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            String textStok = input.getText().toString();
                            int newStok = Integer.parseInt(textStok);
                            updateStokToServer(id, newStok);
                            dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void updateStokToServer(int id, final int theStok){
        progressDialog = ProgressDialog.show(context, "Update Stok", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/barang/updateStok/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, NewHomeActivity.class);
                                intent.putExtra("FRAGMENT_ID", 1);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "Update stok sukses", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("stok", String.valueOf(theStok));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private String dateFunction(String date) {

        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat("yyyy-M-dd hh:mm:ss");

        try {

            Date date1 = simpleDateFormat.parse(date);
            Date date2 = new Date();

            String theDifferent = printDifference(date1, date2);

            return theDifferent;

        } catch (ParseException e) {
            e.printStackTrace();
            return "failed";
        }
    }

    public String printDifference(Date startDate, Date endDate){

        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        Log.e("asd","startDate : " + startDate);
        Log.e("asd","endDate : "+ endDate);
        Log.e("asd","different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        String lastString;
        if(elapsedDays < 1){
            lastString = elapsedHours + " jam yang lalu";
        }else{
            lastString = elapsedDays + " hari yang lalu";
        }
        return lastString;
    }

    private void hapusBarang(int id){
        progressDialog = ProgressDialog.show(context, "Hapus BArang", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, "http://103.195.31.220:5000/v1/barang/hapusBarang/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, NewHomeActivity.class);
                                intent.putExtra("FRAGMENT_ID", 1);
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "Hapus Barang Sukses", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, error.toString(), Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }
}
