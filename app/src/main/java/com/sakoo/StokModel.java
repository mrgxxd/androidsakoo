package com.sakoo;

public class StokModel {
    private String nama, deskripsi, foto, tanggalUpload, tanggalUpdate, tanggalListing, namaSubkategori, ecommerce, status, fotoTambahan, statusUnlisting, volume, bahan, merk;
    private int id, harga, stok, idSubkategori, berat;

    public StokModel (
            int _id, String _nama, String _deskripsi, int _harga, int _stok, String _foto,
            String _tanggalUpload, String _tanggalUpdate, String _tanggalListing,
            int _idSubkategori, String _namaSubkategori, String _ecommerce, String _status, String _fotoTambahan, int _berat,
            String _statusUnlisting, String _volume, String _bahan, String _merk
    ){
        this.id = _id;
        this.nama = _nama;
        this.deskripsi = _deskripsi;
        this.harga = _harga;
        this.stok = _stok;
        this.foto = _foto;
        this.tanggalUpload = _tanggalUpload;
        this.tanggalUpdate = _tanggalUpdate;
        this.tanggalListing = _tanggalListing;
        this.idSubkategori = _idSubkategori;
        this.namaSubkategori = _namaSubkategori;
        this.ecommerce = _ecommerce;
        this.status = _status;
        this.fotoTambahan = _fotoTambahan;
        this.berat = _berat;
        this.statusUnlisting = _statusUnlisting;
        this.volume = _volume;
        this.bahan = _bahan;
        this.merk = _merk;
    }

    public String getNama() {
        return nama;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public String getFoto() {
        return foto;
    }

    public String getTanggalUpload() {
        return tanggalUpload;
    }

    public String getTanggalUpdate() {
        return tanggalUpdate;
    }

    public String getTanggalListing() {
        return tanggalListing;
    }

    public String getNamaSubkategori() {
        return namaSubkategori;
    }

    public String getEcommerce() {
        return ecommerce;
    }

    public String getStatus() {
        return status;
    }

    public String getFotoTambahan() {
        return fotoTambahan;
    }

    public int getId() {
        return id;
    }

    public int getHarga() {
        return harga;
    }

    public int getStok() {
        return stok;
    }

    public int getIdSubkategori() {
        return idSubkategori;
    }

    public int getBerat() {
        return berat;
    }

    public String getStatusUnlisting() {
        return statusUnlisting;
    }

    public String getVolume() {
        return volume;
    }

    public String getBahan() {
        return bahan;
    }

    public String getMerk() {
        return merk;
    }
}