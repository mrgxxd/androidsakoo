package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SubCategoryActivity extends AppCompatActivity {

    private List<SubKategoriModel> notificationModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SubKategoriAdapter mAdapter;

    private BaseUtility baseUtility;
    private ProgressDialog progressDialog;

    private String idSubkategori;

    private Bundle bundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        baseUtility = new BaseUtility(this);

        bundle = getIntent().getExtras();
        idSubkategori = bundle.getString("idKategori");


        // RecycleView
        recyclerView = (RecyclerView)findViewById(R.id.listCategory);
        notificationModelList = new ArrayList<>();
        mAdapter = new SubKategoriAdapter(SubCategoryActivity.this,notificationModelList);
        mLayoutManager = new LinearLayoutManager(SubCategoryActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        getSubKategori();



//        NotificationModel notificationModel1 = new NotificationModel("26/05/2017","Transaksi anda gagal");
//        notificationModelList.add(notificationModel1);
//
//        NotificationModel notificationModel2 = new NotificationModel("26/09/2017","Transaksi anda berhasil");
//        notificationModelList.add(notificationModel2);


    }

    private void getSubKategori(){
        progressDialog = ProgressDialog.show(this, "Mengambil Sub Kategori", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/subcategory/getSubByCategory/"+idSubkategori,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                //if success create session user
                                JSONArray jsonArray = new JSONArray(data);
                                for(int z = 0; z < jsonArray.length(); z++){
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(z);
                                    Log.e("hai",String.valueOf(jsonObject1.getInt("id")));
                                    SubKategoriModel kategoriModel = new SubKategoriModel(String.valueOf(jsonObject1.getInt("id")),jsonObject1.getString("nama"));
                                    notificationModelList.add(kategoriModel);

                                }
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                recyclerView.setAdapter(mAdapter);
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(SubCategoryActivity.this, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
