package com.sakoo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class SubKategoriAdapter extends RecyclerView.Adapter<SubKategoriAdapter.MyViewHolder> {

    private List<SubKategoriModel> listingModelList;
    private Context context;
    private SessionManager sessionManager;

    public SubKategoriAdapter(Context context, List<SubKategoriModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView myText;
        public ImageView myChoose;
        public CardView myCard;

        public MyViewHolder(View view) {
            super(view);
            myText = (TextView) view.findViewById(R.id.textCategory);
            myChoose = (ImageView) view.findViewById(R.id.choose);
            myCard = (CardView) view.findViewById(R.id.card_view);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_category, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        sessionManager = new SessionManager(context);
        final SubKategoriModel documentModel = listingModelList.get(position);
        holder.myText.setText(documentModel.getNama());
        holder.myChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BarangActivity.class);
                sessionManager.subKategori(documentModel.getId());
                sessionManager.textSubKategori(documentModel.getNama());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.putExtra("idKategori", documentModel.getId());
                //intent.putExtra("namaKategori", documentModel.getNama());
                context.startActivity(intent);
            }
        });

        holder.myCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, BarangActivity.class);
                sessionManager.subKategori(documentModel.getId());
                sessionManager.textSubKategori(documentModel.getNama());
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                //intent.putExtra("idKategori", documentModel.getId());
                //intent.putExtra("namaKategori", documentModel.getNama());
                context.startActivity(intent);
            }
        });

//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}

