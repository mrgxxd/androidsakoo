package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class TransactionActivity extends AppCompatActivity{

    private LinearLayout linearLayout1;
    private List<TransactionModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private TransactionAdapter mAdapter;

    private ProgressDialog progressDialog;
    private BaseUtility baseUtility;
    private SessionManager sessionManager;

    private int idTransasaksi, count = 0;
    private int totalHargaBarang = 0, ongkosKirim, totalBayar;
    private String alamatnya = "", kontaknya = "", pengirimannya ="", asuransinya ="", keterangannya = "", keteranganBarang;

    private CardView cardAlamat, cardBiaya;
    private TextView totalHar, totalBay, ongkosKir, alamat, kontak, pengiriman, asuransi, keterangan;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction);

        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);

        idTransasaksi = getIntent().getExtras().getInt("idTransaksi");
        Log.e("Lihat", String.valueOf(idTransasaksi));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        cardAlamat = (CardView)findViewById(R.id.cardAlamat);
        cardBiaya = (CardView)findViewById(R.id.cardBiaya);
        totalHar = (TextView) findViewById(R.id.totalHarga);
        ongkosKir = (TextView) findViewById(R.id.ongkosKirim);
        totalBay = (TextView) findViewById(R.id.totalBayar);
        alamat = (TextView) findViewById(R.id.alamat);
        kontak = (TextView) findViewById(R.id.kontak);
        pengiriman = (TextView) findViewById(R.id.pengiriman);
        asuransi = (TextView) findViewById(R.id.asuransi);
        keterangan = (TextView) findViewById(R.id.keterangan);

        // RecycleView
        recyclerView = (RecyclerView)findViewById(R.id.listTransaction);
        listingModelList = new ArrayList<>();
        mAdapter = new TransactionAdapter(TransactionActivity.this,listingModelList);
        mLayoutManager = new LinearLayoutManager(TransactionActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        listTransactionFunction();
    }

    private void listTransactionFunction(){
        // TODO :

        progressDialog = ProgressDialog.show(this, "Transaksi", "Please wait ...", true );

            StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/transaction/getMyTransaction/"+idTransasaksi,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("TES", response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int code = jsonObject.getInt("code");
                                String message = jsonObject.getString("message");

                                if(code == 200){
                                    String data = jsonObject.getString("data");
                                    //if success create session user
                                    JSONArray jsonArray = new JSONArray(data);
                                    for(int i =0; i < jsonArray.length(); i++){
                                        JSONObject jsonData = jsonArray.getJSONObject(i);
                                        int idTransaksi = jsonData.getInt("id_transaksi");
                                        String namaBarang = jsonData.getString("nama");
                                        int statusTransaksi = jsonData.getInt("status");
                                        String tanggal;
                                        if(statusTransaksi == 0){
                                            tanggal = jsonData.getString("tanggal_pemesanan");
                                        }else{
                                            tanggal = jsonData.getString("tanggal_action");
                                        }
                                        String fotoBarang = jsonData.getString("foto");

                                        String noOrder = jsonData.getString("oreder_no");
                                        String noResi = jsonData.getString("nomor_resi");

                                        count = count + 1;

                                        if(Objects.equals(jsonData.getString("keterangan"), "")){
                                            keterangannya = "-";
                                        }else{
                                            keterangannya = jsonData.getString("keterangan");
                                        }

                                        if(Objects.equals(jsonData.getString("keterangan_barang"), "")){
                                            keteranganBarang = "-";
                                        }else{
                                            keteranganBarang = jsonData.getString("keterangan_barang");
                                        }

                                        if(jsonData.getInt("asuransi") == 0){
                                            asuransinya = "Tidak";
                                        }else{
                                            asuransinya = "Ya";
                                        }

                                        pengirimannya = jsonData.getString("jasa_pengiriman");
                                        kontaknya = jsonData.getString("no_hp_buyer");

                                        ongkosKirim = jsonData.getInt("biaya_kirim");
                                        alamatnya = jsonData.getString("alamat").replaceAll("( +)"," ").trim();
                                        alamatnya = alamatnya.replaceAll("&nbsp;","").trim();
                                        int barangXharga = jsonData.getInt("harga")*jsonData.getInt("jumlah");
                                        totalHargaBarang = totalHargaBarang + barangXharga;

                                        TransactionModel notificationModel1 = new TransactionModel(idTransaksi, namaBarang, statusTransaksi, tanggal, fotoBarang, noOrder, noResi, String.valueOf(count), keteranganBarang);
                                        listingModelList.add(notificationModel1);
                                    }
                                    cardBiaya.setVisibility(View.VISIBLE);
                                    cardAlamat.setVisibility(View.VISIBLE);
                                    totalBayar = ongkosKirim + totalHargaBarang;

                                    DecimalFormat kursIndonesia = new DecimalFormat("#,###");

                                    totalHar.setText("Total harga  : Rp. "+String.valueOf(kursIndonesia.format(totalHargaBarang)).replace(",","."));
                                    ongkosKir.setText("Ongkos kirim : Rp. "+String.valueOf(kursIndonesia.format(ongkosKirim)).replace(",","."));
                                    totalBay.setText("Total bayar : Rp. "+String.valueOf(kursIndonesia.format(totalBayar)).replace(",","."));
                                    alamat.setText(alamatnya);
                                    keterangan.setText(keterangannya);
                                    asuransi.setText(asuransinya);
                                    kontak.setText(kontaknya);
                                    pengiriman.setText(pengirimannya);

                                    recyclerView.setAdapter(mAdapter);
//                                //if success navigate to MainActivity
                                    progressDialog.dismiss();
                                }else{
                                    progressDialog.dismiss();
                                    Toast.makeText(TransactionActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Log.e("erornya", error.toString());
                            baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<String,String>();
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("secretkey", "JackTheRipper");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
            requestQueue.add(stringRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public int getId(){
        return idTransasaksi;
    }
}
