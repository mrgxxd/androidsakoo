package com.sakoo;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.MyViewHolder> {

    private List<TransactionModel> listingModelList;
    private Context context;

    private ProgressDialog progressDialog;

    public TransactionAdapter(Context context, List<TransactionModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView myFoto;
        public TextView myBarang, myStatus, tanggal, keteranganBarang, noResi;
        private Button tolakTransaksi, prosesTransaksi, inputResi;

        public MyViewHolder(View view) {
            super(view);
            myFoto = (ImageView) view.findViewById(R.id.fotoBarang);
            myBarang = (TextView) view.findViewById(R.id.namaBarang);
            myStatus = (TextView) view.findViewById(R.id.statusBarang);
            tanggal = (TextView) view.findViewById(R.id.tanggalAction);
            keteranganBarang = (TextView) view.findViewById(R.id.keteranganBarang);
            noResi = (TextView) view.findViewById(R.id.noResi);
            tolakTransaksi = (Button) view.findViewById(R.id.tolakTransaksi);
            prosesTransaksi = (Button) view.findViewById(R.id.prosesTransaksi);
            inputResi = (Button) view.findViewById(R.id.inputResi);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_transaction, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final TransactionModel documentModel = listingModelList.get(position);
//        String namaBarang = documentModel.getNamaBarang();
//        if(namaBarang.length() > 23){
//            String newNama = namaBarang.substring(0,20);
//            holder.myBarang.setText(newNama.trim()+" ...");
//        }else{
//            holder.myBarang.setText(namaBarang.trim());
//        }
        holder.myBarang.setText(documentModel.getNamaBarang().trim());

        if(documentModel.getStatusResi() == 0){
            holder.myStatus.setText("Belum diproses");
            holder.inputResi.setVisibility(View.INVISIBLE);
            holder.noResi.setText("No resi : -");
            holder.tolakTransaksi.setVisibility(View.VISIBLE);
            holder.prosesTransaksi.setVisibility(View.VISIBLE);
        }else if(documentModel.getStatusResi() == 1){
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(documentModel.getNoResi(), "")){
                    holder.myStatus.setText("Belum input resi");
                    holder.inputResi.setVisibility(View.VISIBLE);
                    holder.noResi.setText("No resi : -");
                    holder.tolakTransaksi.setVisibility(View.INVISIBLE);
                    holder.prosesTransaksi.setVisibility(View.INVISIBLE);
                }else{
                    holder.myStatus.setText("Sudah selesai");
                    holder.inputResi.setVisibility(View.INVISIBLE);
                    holder.noResi.setText("No resi : "+documentModel.getNoResi());
                    holder.tolakTransaksi.setVisibility(View.INVISIBLE);
                    holder.prosesTransaksi.setVisibility(View.INVISIBLE);
                }
            }

        }else if(documentModel.getStatusResi() == 2){
            holder.myStatus.setText("Sudah ditolak");
            holder.inputResi.setVisibility(View.INVISIBLE);
            holder.tolakTransaksi.setVisibility(View.INVISIBLE);
            holder.prosesTransaksi.setVisibility(View.INVISIBLE);
        }else {
            holder.myStatus.setText("Sudah Selesai");
            holder.inputResi.setVisibility(View.INVISIBLE);
            holder.tolakTransaksi.setVisibility(View.INVISIBLE);
            holder.prosesTransaksi.setVisibility(View.INVISIBLE);
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String theDate = documentModel.getTanggal();
        Date date = null;
        try {
            date = dateFormat.parse(theDate);
            dateFormat.applyPattern("dd/MM/yyyy HH:mm");
            holder.tanggal.setText(dateFormat.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        String temp = documentModel.getFoto();
        temp = temp.replaceAll(" ", "%20");
        Picasso.with(context).load(temp+"?size=medium").into(holder.myFoto);

        holder.keteranganBarang.setText(documentModel.getKeterangan());

        holder.tolakTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, DetailTransaction.class);
//                intent.putExtra("idTransaksi", documentModel.getIdTransaksi());
//                context.startActivity(intent);
                tolakFunction(documentModel.getIdTransaksi());
            }
        });

        holder.prosesTransaksi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, DetailTransaction.class);
//                intent.putExtra("idTransaksi", documentModel.getIdTransaksi());
//                context.startActivity(intent);
                prosesFunction(documentModel.getIdTransaksi());
            }
        });

        holder.inputResi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(context, DetailTransaction.class);
//                intent.putExtra("idTransaksi", documentModel.getIdTransaksi());
//                context.startActivity(intent);
                updateResi(documentModel.getIdTransaksi());
            }
        });


//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    private void tolakFunction(final int idTransaksi){
        progressDialog = ProgressDialog.show(context, "Tolak transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/tolakTransaksi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, TransactionActivity.class);
                                intent.putExtra("idTransaksi", ((TransactionActivity)context).getId());
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "Transaksi sudah ditolak", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi bermasalah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void prosesFunction(final int idTransaksi){
        progressDialog = ProgressDialog.show(context, "Proses transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/prosesTransaksi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, TransactionActivity.class);
                                intent.putExtra("idTransaksi", ((TransactionActivity)context).getId());
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "Transaksi berhasil diproses", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi bermasalah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void kirimResiFunction(final int idTransaksi, final String noResi){
        progressDialog = ProgressDialog.show(context, "Proses transaksi", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/transaction/updateResi/"+idTransaksi,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, TransactionActivity.class);
                                intent.putExtra("idTransaksi", ((TransactionActivity)context).getId());
                                context.startActivity(intent);
                                ((Activity)context).finish();
                                Toast.makeText(context, "No Resi berhasil diinput", Toast.LENGTH_SHORT).show();

                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi bermasalah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("nomor_resi", noResi);
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void updateResi(final int idTransaksi){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Input Resi");
        alertDialog.setMessage("Silahkan masukan No Resi dengan benar");

        final EditText input = new EditText(context);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("Kirim",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(context, "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            String resinya = input.getText().toString();
                            kirimResiFunction(idTransaksi, resinya);
                            dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("Batal",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }
}
