package com.sakoo;

public class TransactionModel {

    private int idTransaksi, statusResi;
    private String namaBarang, tanggal, foto, noOrder, noResi, count, keterangan;

    public TransactionModel(int i1, String s1, int s2,String s3, String s4, String s5, String s6, String s7, String s8){
        this.idTransaksi = i1;
        this.namaBarang = s1;
        this.statusResi = s2;
        this.tanggal = s3;
        this.foto = s4;
        this.noOrder = s5;
        this.noResi = s6;
        this.count = s7;
        this.keterangan = s8;
    }

    public String getNamaBarang() {
        return namaBarang;
    }

    public int getStatusResi() {
        return statusResi;
    }

    public int getIdTransaksi() {
        return idTransaksi;
    }

    public String getFoto() {
        return foto;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getNoOrder() {
        return noOrder;
    }

    public String getNoResi() {
        return noResi;
    }

    public String getCount() {
        return count;
    }

    public String getKeterangan() {
        return keterangan;
    }
}
