package com.sakoo;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnlistingAdapter extends RecyclerView.Adapter<UnlistingAdapter.MyViewHolder> {

    private List<UnlistingModel> listingModelList;
    private Context context;

    private ProgressDialog progressDialog;

    public UnlistingAdapter(Context context, List<UnlistingModel> _listingModelList) {
        this.listingModelList = _listingModelList;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView myBarang, myStok, myKeterangan, countNumber;
        public ImageView myFoto;
        public Button updateStok;

        public MyViewHolder(View view) {
            super(view);
            myFoto = (ImageView) view.findViewById(R.id.fotoBarang);
            myBarang = (TextView) view.findViewById(R.id.namaBarang);
            myStok = (TextView) view.findViewById(R.id.stokBarang);
            countNumber = (TextView) view.findViewById(R.id.countNumber);
            myKeterangan = (TextView) view.findViewById(R.id.statusBarang);
            updateStok = (Button) view.findViewById(R.id.updateStok);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_unlisting, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final UnlistingModel documentModel = listingModelList.get(position);
        String namaBarang = documentModel.getMyBarang();
        holder.countNumber.setText(documentModel.getCountNumber());
        if(namaBarang.length() > 23){
            String newNama = namaBarang.substring(0,20);
            holder.myBarang.setText(newNama+" ...");
        }else{
            holder.myBarang.setText(namaBarang);
        }

        if(documentModel.getStatus() == 1){
            holder.myKeterangan.setText("Status : Barang baru");
            holder.updateStok.setVisibility(View.INVISIBLE);
        }else if(documentModel.getStatus() == 3){
            holder.myKeterangan.setText("Status : Stok perlu diupdate");
            holder.updateStok.setVisibility(View.VISIBLE);
        }
        holder.myStok.setText("Stok("+documentModel.getMyStok()+")");
        String temp = documentModel.getFoto();
        temp = temp.replaceAll(" ", "%20");
        Picasso.with(context).load(temp).into(holder.myFoto);

        holder.updateStok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateFunction(documentModel.getMyId(), documentModel.getMyStok());
            }
        });

//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(context, DetailHotelActivity.class);
//                intent.putExtra(KEY_NAMA_LOKASI,documentModel.getLocaName());
//                intent.putExtra(KEY_ALAMAT, documentModel.getLocation());
//                intent.putExtra(KEY_KATEGORI, "hotel");
//                intent.putExtra(KEY_LATITUDE,documentModel.getLatitude());
//                intent.putExtra(KEY_LONGITUDE,documentModel.getLongitude());
//                intent.putExtra(KEY_DESKRIPSI, documentModel.getInformation());
//                intent.putExtra(KEY_PICTURE, documentModel.getPicture());
//                intent.putExtra(KEY_ANGKOT, documentModel.getAngkot());
//                context.startActivity(intent);
//            }
//        });

//        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
//            @Override
//            public boolean onLongClick(View v) {
//                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        switch (which){
//                            case DialogInterface.BUTTON_POSITIVE:
//                                break;
//
//                            case DialogInterface.BUTTON_NEGATIVE:
//                                break;
//                        }
//                    }
//                };
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(context);
//                builder.setMessage("Are you sure to delete this document ?").setPositiveButton("Yes", dialogClickListener)
//                        .setNegativeButton("No", dialogClickListener).show();
//                return false;
//            }
//        });

    }

    public void updateList(List<UnlistingModel> list){
        listingModelList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return listingModelList.size();
    }

    private void updateFunction(final int id, final int stok){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Edit Stok");
        alertDialog.setMessage("Mohon dapat update stok secara berkala setiap 1 minggu sesuai stok offline");

        final EditText input = new EditText(context);
        input.setText(String.valueOf(stok));
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);

        input.setLayoutParams(lp);
        alertDialog.setView(input);

        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if(input.getText().length() == 0){
                            Toast.makeText(context, "Form tidak boleh dikosongkan", Toast.LENGTH_SHORT).show();
                        }else{
                            String textStok = input.getText().toString();
                            int newStok = Integer.parseInt(textStok);
                            updateStokToServer(id, newStok);
                            dialog.cancel();
                        }
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }

    private void updateStokToServer(int id, final int theStok){
        progressDialog = ProgressDialog.show(context, "Update Stok", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.PUT, "http://103.195.31.220:5000/v1/barang/updateStok/"+id,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                Intent intent = new Intent(context, StockActivity.class);
                                context.startActivity(intent);
                                Toast.makeText(context, "Update stok sukses", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        Toast.makeText(context, "Koneksi bermasalah, silahkan coba lagi", Toast.LENGTH_SHORT).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("stok", String.valueOf(theStok));
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(context, new HurlStack());
        requestQueue.add(stringRequest);
    }
}
