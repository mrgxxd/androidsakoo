package com.sakoo;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UnlistingFragment extends Fragment {

    private LinearLayout linearLayout1;
    private List<UnlistingModel> listingModelList;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private UnlistingAdapter mAdapter;

    private SessionManager sessionManager;
    private ProgressDialog progressDialog;
    private BaseUtility baseUtility;

    private int count = 0;
    private TextView infojumlahunlisting;
    private CardView cardInfoUnlisting;
    private EditText cariByText;

    public UnlistingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getActivity());
        baseUtility = new BaseUtility(getActivity());
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_unlisting, container, false);

        // RecycleView
        cariByText = (EditText) view.findViewById(R.id.cariByText);
        cardInfoUnlisting = (CardView) view.findViewById(R.id.cardInfoUnlisting);
        infojumlahunlisting = (TextView) view.findViewById(R.id.infojumlahunlisting);
        recyclerView = (RecyclerView)view.findViewById(R.id.listListing);
        listingModelList = new ArrayList<>();
        mAdapter = new UnlistingAdapter(getActivity(),listingModelList);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        unListingFunction();

        cariByText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // filter your list from your input
                filter(s.toString());
                //you can use runnable postDelayed like 500 ms to delay search text
            }
        });

        return view;
    }

    private void unListingFunction(){
        // TODO :

        progressDialog = ProgressDialog.show(getActivity(), "List Barang", "Please wait ...", true );

        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/barang/getUnListing/"+sessionManager.getIdUser(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");
                            String message = jsonObject.getString("message");

                            if(code == 200){
                                String data = jsonObject.getString("data");
                                JSONArray jsonArray = new JSONArray(data);
                                for(int j = 0; j < jsonArray.length(); j++){
                                    //if success create session user
                                    JSONObject jsonData = jsonArray.getJSONObject(j);
                                    int id_barang = jsonData.getInt("id_barang");
                                    int stok = jsonData.getInt("stok");
                                    String nama_barang = jsonData.getString("nama_barang");
                                    String foto = jsonData.getString("foto");
                                    int statusBarang = jsonData.getInt("status");
                                    count = count + 1;

                                    UnlistingModel notificationModel1 = new UnlistingModel(nama_barang,stok,id_barang,foto, statusBarang, String.valueOf(count));
                                    listingModelList.add(notificationModel1);
                                }
                                infojumlahunlisting.setText("Jumlah barang : "+jsonArray.length());
                                cardInfoUnlisting.setVisibility(View.VISIBLE);

//                                //if success navigate to MainActivity
                                recyclerView.setAdapter(mAdapter);
                                progressDialog.dismiss();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(getActivity(), new HurlStack());
        requestQueue.add(stringRequest);
    }

    private void filter(String text){
        List<UnlistingModel> temp = new ArrayList();
        for(UnlistingModel d: listingModelList){
            //or use .equal(text) with you want equal match
            //use .toLowerCase() for better matches
            if(d.getMyBarang().contains(text)){
                temp.add(d);
            }
        }
        //update recyclerview
        mAdapter.updateList(temp);
    }
}
