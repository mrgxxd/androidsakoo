package com.sakoo;

public class UnlistingModel {

    private String myBarang, foto, countNumber;
    private int myStok, myId, status;

    public UnlistingModel(String s1, int i1, int i2,  String s5, int i3, String s6){
        this.myBarang = s1;
        this.myStok = i1;
        this.myId = i2;
        this.foto = s5;
        this.status = i3;
        this.countNumber = s6;
    }

    public String getMyBarang() {
        return myBarang;
    }

    public String getFoto() {
        return foto;
    }

    public int getMyStok() {
        return myStok;
    }

    public int getMyId() {
        return myId;
    }

    public int getStatus() {
        return status;
    }

    public String getCountNumber() {
        return countNumber;
    }
}
