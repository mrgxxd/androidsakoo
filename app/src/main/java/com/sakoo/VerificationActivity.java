package com.sakoo;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VerificationActivity extends AppCompatActivity implements View.OnClickListener {

    PackageInfo packageInfo;
    String versionName, appPackageName;
    int versionCode;

    private Button sendButton;
    private EditText inputCode, edDigit1, edDigit2, edDigit3, edDigit4;
    private PinEntryEditText cInput;
    private TextView klikDisini, timerCount;
    private BaseUtility baseUtility;
    private SessionManager sessionManager;
    private ProgressDialog progressDialog;

    private String myCode, myMode, myData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);

        //Class
        baseUtility = new BaseUtility(this);
        sessionManager = new SessionManager(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        myMode = String.valueOf(getIntent().getExtras().get("mode"));
        myData = String.valueOf(getIntent().getExtras().get("saldoId"));

        Log.e("myMode", myMode);
        Log.e("myData", myData);

        //Log.e("TES", sessionManager.getKontakUserSementara());

        //DEFINITION AND EVENT COMPONENT
        definitionComponent();
        eventComponent();


        new CountDownTimer(300000, 1000) {

            public void onTick(long millisUntilFinished) {
                long fullSecond = millisUntilFinished / 1000;
                long tSecond = fullSecond % 60;
                long tMinute = (fullSecond - tSecond)/60;

                timerCount.setText(String.valueOf(tMinute)+" : "+String.valueOf(tSecond));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                timerCount.setText("0 : 0");
                klikDisini.setEnabled(true);
            }

        }.start();

    }

    private void definitionComponent(){
        sendButton = (Button) findViewById(R.id.sendCode);
        //inputCode = (EditText) findViewById(R.id.inputCode);

        edDigit1 = (EditText) findViewById(R.id.edDigit1);
        edDigit2 = (EditText) findViewById(R.id.edDigit2);
        edDigit3 = (EditText) findViewById(R.id.edDigit3);
        edDigit4 = (EditText) findViewById(R.id.edDigit4);

        klikDisini = (TextView) findViewById(R.id.klikDisini);
        klikDisini.setEnabled(false);
        timerCount = (TextView) findViewById(R.id.timerCount);

        //cInput = (PinEntryEditText) findViewById(R.id.codeInput);
    }

    private void eventComponent(){
        sendButton.setOnClickListener(this);
        klikDisini.setOnClickListener(this);

        edDigit1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edDigit1.getText().length() != 0){
                    edDigit2.requestFocus();
                }
            }


        });

        edDigit2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edDigit2.getText().length() != 0){
                    edDigit3.requestFocus();
                }
            }
        });

        edDigit3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(edDigit3.getText().length() != 0){
                    edDigit4.requestFocus();
                }
            }
        });

        edDigit4.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    if(edDigit4.getText().length() == 0){
                        edDigit3.requestFocus();
                    }
                }
                return false;
            }
        });

        edDigit3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    if(edDigit3.getText().length() == 0){
                        edDigit2.requestFocus();
                    }
                }
                return false;
            }
        });

        edDigit2.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if(keyCode == KeyEvent.KEYCODE_DEL) {
                    if(edDigit2.getText().length() == 0){
                        edDigit1.requestFocus();
                    }
                }
                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        if(menuItem.getItemId() == android.R.id.home){
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendCode :
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if(Objects.equals(timerCount.getText().toString(), "0")){
                        baseUtility.myToast("Waktu habis, silahkan untuk meminta kode yang baru");
                    }else {
                        verifikasiUser();
                    }
                }
                break;
            case R.id.klikDisini :
                resetCode();
                break;
            default:
                break;
        }
    }

    private void verifikasiUser(){
        // TODO :

        //check username and password value can't null
        if(edDigit1.getText().length() == 0 || edDigit2.getText().length() == 0 || edDigit3.getText().length() == 0 || edDigit4.getText().length() == 0){
            baseUtility.myToast("Form tidak boleh kosong");
        }else{
            myCode = edDigit1.getText().toString() + edDigit2.getText().toString() + edDigit3.getText().toString() + edDigit4.getText().toString();
            //call API login and check response
            progressDialog = ProgressDialog.show(this, "Verifikasi Code", "Please wait ...", true );

            //Log.e("lihat", apiConfig.userEndpoint);
            String myUrl = "";
            int myMethod = 0;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if(Objects.equals(myMode, "all")){
                    myUrl = "http://103.195.31.220:5000/v1/transaction/cairkanSemuaDana";
                    myMethod = Request.Method.POST;
                }else{
                    myUrl = "http://103.195.31.220:5000/v1/transaction/cairkanDana/"+myData;
                    myMethod = Request.Method.PUT;
                }
            }


            StringRequest stringRequest = new StringRequest(myMethod, myUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.e("TES", response.toString());
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int code = jsonObject.getInt("code");
                                String message = jsonObject.getString("message");

                                if(code == 200){
//                                //if success navigate to MainActivity
                                    //progressDialog.dismiss();
                                    //baseUtility.myToast("Sukses");
                                    //String data = jsonObject.getString("data");
                                    //if success create session user
                                    //JSONObject jsonData = new JSONObject(data);

                                    //checkVersion();
                                    Intent intent = new Intent(VerificationActivity.this, NewHomeActivity.class);
                                    intent.putExtra("FRAGMENT_ID", 3);
                                    startActivity(intent);
                                    finish();
                                }else{
                                    progressDialog.dismiss();
                                    Toast.makeText(VerificationActivity.this, message, Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Log.e("erornya", error.toString());
                            baseUtility.myToast(error.toString());
                        }
                    }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> map = new HashMap<String,String>();
                    map.put("id", myData);
                    map.put("code_verifikasi", myCode);
                    return map;
                }

                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<String, String>();
                    //headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("secretkey", "JackTheRipper");
                    return headers;
                }
            };

            RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
            requestQueue.add(stringRequest);
        }
    }

    private void resetCode(){
        // TODO :
            //call API login and check response
        progressDialog = ProgressDialog.show(this, "Verifikasi Code", "Please wait ...", true );

        //Log.e("lihat", apiConfig.userEndpoint);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, "http://103.195.31.220:5000/v1/user/newLogin",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("TES", response.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int code = jsonObject.getInt("code");

                            if(code == 200){
//                                //if success navigate to MainActivity
                                progressDialog.dismiss();
                                baseUtility.myToast("Code sedang dikirim");
                                Intent myIntent = new Intent(VerificationActivity.this, VerificationActivity.class);
                                myIntent.putExtra("saldoId", myData);
                                myIntent.putExtra("mode", myMode);
                                startActivity(myIntent);
                                finish();
                            }else{
                                progressDialog.dismiss();
                                Toast.makeText(VerificationActivity.this, "Code gagal dikirim", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("erornya", error.toString());
                        baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String> map = new HashMap<String,String>();
                map.put("no_hp",sessionManager.getKontakUserSementara());
                return map;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                //headers.put("Content-Type", "application/x-www-form-urlencoded");
                headers.put("secretkey", "JackTheRipper");
                return headers;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this, new HurlStack());
        requestQueue.add(stringRequest);
    }

//    private void checkVersion(){
//        try {
//            packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
//            versionName = packageInfo.versionName;
//            versionCode = packageInfo.versionCode;
//            appPackageName = getPackageName();
//            Log.e("Name",appPackageName);
//            Log.e("code", String.valueOf(versionCode));
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }
//
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, "http://103.195.31.220:5000/v1/util/getVersion/"+sessionManager.getIdUser(), new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.e("RESPONSE", response);
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//                    int code = jsonObject.getInt("code");
//                    String message = jsonObject.getString("message");
//                    if(code == 200) {
//                        String data = jsonObject.getString("data");
//                        JSONObject jsonData = new JSONObject(data);
//                        String versionData = jsonData.getString("version");
//                        String userData = jsonData.getString("user");
//
//                        String jumlahBarang = jsonData.getString("jumlahBarang");
//                        String jumlahTransaksi = jsonData.getString("jumlahTransaksi");
//                        String jumlahSaldo = jsonData.getString("jumlahSaldo");
//
//                        sessionManager.createHomeStatus(jumlahBarang, jumlahTransaksi, jumlahSaldo);
//
//                        JSONObject versionObject = new JSONObject(versionData);
//                        JSONObject userObject = new JSONObject(userData);
//
//                        sessionManager.createUserLoginSession(
//                                userObject.getString("id"),
//                                userObject.getString("nama"),
//                                userObject.getString("email"),
//                                userObject.getString("username"),
//                                userObject.getString("no_hp"),
//                                userObject.getString("alamat"),
//                                userObject.getString("foto"),
//                                userObject.getString("reg_id"),
//                                userObject.getString("token"),
//                                userObject.getString("rank"),
//                                userObject.getString("jumlah_transaksi"),
//                                userObject.getString("jumlah_listing"),
//                                userObject.getString("bank"),
//                                userObject.getString("rekening"),
//                                userObject.getString("nama_rekening"),
//                                userObject.getString("lokasi_markup")
//                        );
//
//                        progressDialog.dismiss();
//                        if(versionCode < versionObject.getInt("version_code")){
//                            updateAplikasi();
//                        }else{
//                            gotomain();
//                        }
//                    }else{
//                        progressDialog.dismiss();
//                        gotomain();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.e("GAGAL",error.toString());
//                baseUtility.myToast("Koneksi bermasalah, silahkan coba lagi");
//                finish();
//            }
//        }){
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String,String> map = new HashMap<String,String>();
//                return map;
//            }
//
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> headers = new HashMap<String, String>();
//                //headers.put("Content-Type", "application/x-www-form-urlencoded");
//                headers.put("secretkey", "JackTheRipper");
//                return headers;
//            }
//        };
//
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(stringRequest);
//    }
//
//    private void updateAplikasi(){
//        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
//        try {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
//        } catch (android.content.ActivityNotFoundException anfe) {
//            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
//        }
//    }
//
//    private void gotomain(){
//        Intent intent = new Intent(VerificationActivity.this, NewHomeActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent);
//        finish();
//    }
}
