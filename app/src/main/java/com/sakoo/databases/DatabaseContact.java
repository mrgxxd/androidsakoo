package com.sakoo.databases;

import android.provider.BaseColumns;

/**
 * Created by Dell on 12/14/2017.
 */

public class DatabaseContact {
    static String TBL_NOTIF = "tbl_notif";

    static final class NotifColumns implements BaseColumns {
        static String TITLE = "title";
        static String BODY = "body";
        static String TYPEID = "typeId";
        static String TYPE = "type";
        static String STATUS = "status";
        static String DATE = "date";
    }
}
