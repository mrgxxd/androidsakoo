package com.sakoo.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.sakoo.databases.DatabaseContact.TBL_NOTIF;
import static com.sakoo.databases.DatabaseContact.NotifColumns;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "dbSakoo";
    private static final int DATABASE_VERSION = 1;
    private static String CREATE_NOTIF = "create table " + TBL_NOTIF +
            "("+NotifColumns._ID +" integer primary key autoincrement, " +
            NotifColumns.TITLE + " text not null, " +
            NotifColumns.BODY + " text not null, " +
            NotifColumns.TYPEID + " integer not null, " +
            NotifColumns.TYPE + " text not null, " +
            NotifColumns.STATUS + " integer not null, " +
            NotifColumns.DATE + " text not null);";

    DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_NOTIF);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS "+TBL_NOTIF);
    }

    public void deleteData(){
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("delete from "+ TBL_NOTIF);
    }
}
