package com.sakoo.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import com.sakoo.NotificationModel;
import com.sakoo.models.Notif;

import java.util.ArrayList;

import static com.sakoo.databases.DatabaseContact.NotifColumns.BODY;
import static com.sakoo.databases.DatabaseContact.NotifColumns.DATE;
import static com.sakoo.databases.DatabaseContact.NotifColumns.STATUS;
import static com.sakoo.databases.DatabaseContact.NotifColumns.TITLE;
import static com.sakoo.databases.DatabaseContact.NotifColumns.TYPE;
import static com.sakoo.databases.DatabaseContact.NotifColumns.TYPEID;
import static com.sakoo.databases.DatabaseContact.NotifColumns._ID;
import static com.sakoo.databases.DatabaseContact.TBL_NOTIF;

public class NotifHelper {
    private Context context;
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    public NotifHelper(Context context) {
        this.context = context;
    }

    public void open() throws SQLException {
        dbHelper = new DatabaseHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public void beginTransaction(){
        db.beginTransaction();
    }

    public void setTransactionSuccess(){
        db.setTransactionSuccessful();
    }

    public void endTransaction(){
        db.endTransaction();
    }

    public void logout() {dbHelper.deleteData();}

    public void insertTransaction(NotificationModel notif){
        String sql = "INSERT INTO "+TBL_NOTIF+" ("+TITLE+","+BODY+","+TYPEID+","+TYPE+","+STATUS+","+DATE+")"+
                " VALUES (?,?,?,?,?,?)";

        SQLiteStatement st = db.compileStatement(sql);
        st.bindString(1, notif.getMyTitle());
        st.bindString(2, notif.getMyText());
        st.bindLong(3, notif.getTypeId());
        st.bindString(4, notif.getType());
        st.bindLong(5, notif.getStatus());
        st.bindString(6, notif.getMyDate());
        st.execute();
        st.clearBindings();
    }

    public int changeStatus(int id, int status){
        ContentValues cv = new ContentValues();
        cv.put(STATUS, status);

        return db.update(TBL_NOTIF, cv, _ID + " = ?",new String[]{String.valueOf(id)});
    }

    public ArrayList<NotificationModel> getAll(){
        Cursor cursor = db.query(TBL_NOTIF,
                null,
                null,
                null,
                null,
                null,
                _ID + " DESC",
                "20");

        ArrayList<NotificationModel> notificationModels = new ArrayList<>();
        NotificationModel nfMdl;

        if (cursor != null && cursor.getCount() > 0){
            cursor.moveToFirst();
            do {
                nfMdl = new NotificationModel();
                nfMdl.setMyId(cursor.getInt(cursor.getColumnIndexOrThrow(_ID)));
                nfMdl.setMyTitle(cursor.getString(cursor.getColumnIndexOrThrow(TITLE)));
                nfMdl.setMyText(cursor.getString(cursor.getColumnIndexOrThrow(BODY)));
                nfMdl.setTypeId(cursor.getInt(cursor.getColumnIndexOrThrow(TYPEID)));
                nfMdl.setType(cursor.getString(cursor.getColumnIndexOrThrow(TYPE)));
                nfMdl.setMyDate(cursor.getString(cursor.getColumnIndexOrThrow(DATE)));
                nfMdl.setStatus(cursor.getInt(cursor.getColumnIndexOrThrow(STATUS)));

                notificationModels.add(nfMdl);
                cursor.moveToNext();
            } while (!cursor.isAfterLast());
        }

        return notificationModels;
    }

    public boolean isExist(int id){
        Cursor cursor = db.query(TBL_NOTIF,
                null,
                _ID + " = ?",
                new String[]{String.valueOf(id)},
                null,
                null,
                null);

        int curCount = cursor.getCount();
        cursor.close();

        return curCount > 0;
    }
}
