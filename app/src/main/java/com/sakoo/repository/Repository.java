package com.sakoo.repository;


import com.sakoo.models.Person;

import java.util.List;


public interface Repository {
    public void loadAll(RepositoryCallback<List<Person>> callback);
    public void save(List<Person> persons);
    public void save(Person person);
}
