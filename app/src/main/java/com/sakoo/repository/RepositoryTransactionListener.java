package com.sakoo.repository;

import com.sakoo.models.Person;

import java.util.List;

public interface RepositoryTransactionListener {
    public void onLoadAlumnusSucceeded(List<Person> alumnus);
}
