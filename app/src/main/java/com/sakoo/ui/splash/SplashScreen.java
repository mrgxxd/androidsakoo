package com.sakoo.ui.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.sakoo.ui.homepagetab.HomePageTabActivity;
import com.sakoo.ui.login.LoginActivity;
import com.qiscus.sdk.Qiscus;

public class SplashScreen extends AppCompatActivity {
    private static int splashInterval = 10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_splash_screen);


        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // TODO Auto-generated method stub
                if (!Qiscus.hasSetupUser()) {
                    startActivity(new Intent(SplashScreen.this, LoginActivity.class));
                }
                else {
                    //startActivity(new Intent(SplashScreen.this, RecentConversationsActivity.class));
                    startActivity(new Intent(SplashScreen.this, HomePageTabActivity.class));
                    this.finish();
                }
                this.finish();
            }

            private void finish() {
                // TODO Auto-generated method stub

            }
        }, splashInterval);

    };

}
